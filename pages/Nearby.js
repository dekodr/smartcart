import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , Modal
  , TouchableOpacity
  , TextInput } from 'react-native';
import { SearchBar } from 'react-native-elements';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import { 
  styles
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

import BoxAccent from '../components/BoxAccent';
import TextDivider from '../components/TextDivider';
import PromoBox1 from '../components/PromoBox1';
import PromoBox2 from '../components/PromoBox2';
import PromoBox3 from '../components/PromoBox3';

let ScreenHeight = Dimensions.get("window").height;

class Nearby extends React.Component {
  static navigationOptions = {
    header: null
  }
  
  state = {
    search: '',
  };

  updateSearch = search => {
    this.setState({ search });
  };

  render() {
    const { search } = this.state;

    return (
      <ScrollView style={styles.mcGrid}>
        <BoxAccent
          yAxis={-25}
          xAxis={325}
          width={200}
          height={200}
        ></BoxAccent>
        <BoxAccent
          yAxis={-250}
          xAxis={-150}
          width={350}
          height={350}
        ></BoxAccent>

        <View style={[styles.mcRow, styles.firstRow, page.center, page.header]}>
          <View style={styles.mcCol6}>
            <Text style={page.headerTitle}>Nearby</Text>
            <View style={page.headerSubtitle}>
              <Image
                style={{width: 12, height: 16}}
                source={require('../assets/icon-location-mark.png')}
              />
              <Text style={page.subtitle}>Sudirman, Jakarta, ID</Text>
            </View>
          </View>
          <View style={[styles.mcCol3, page.searchButtonWp]}>
            <TouchableOpacity 
              onPress={
                  () => this.props.navigation.navigate('NearbyMap', { Component: { options: { bottomTabs: { visible: false, drawBehind: true, animate: true } } } })
            }>
              <View style={page.searchButton}>
                <Image
                  style={{width: 25, height: 35}}
                  source={require('../assets/icon-phone-map.png')}
                />
                <Text style={page.text}>Search {"\n"}By <Text style={{fontWeight: '700'}}>Map</Text></Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>

        <View style={[styles.mcRow, page.center]}>
          <View style={styles.mcCol6}>
            <View style={page.searchBar}>
              <Image
                style={{width: 18, height: 18}}
                source={require('../assets/icon-search-dollar.png')}
              />
              <TextInput 
                style={page.searchInput}
                placeholder={"Search here and let's go"}
                placeholderTextColor={'#798191'}
                onChangeText={this.updateSearch}
                value={search}
              />
            </View>
          </View>
          <View style={[styles.mcCol1, page.optionButton]}>
            <Image
              style={{width: 20, height: 20}}
              source={require('../assets/icon-options.png')}
            />
          </View>
        </View>

        <View style={[styles.mcRow, styles.fullWidth]}>
          <ScrollView 
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={page.categorySlider}
          >
            <Text style={[page.categoryItem, page.categoryItemActive]}>All</Text>
            <Text style={page.categoryItem}>Food & Bevarage</Text>
            <Text style={page.categoryItem}>Lifestyle</Text>
            <Text style={[page.categoryItem, page.categoryItemLast]}>Healthcare</Text>
          </ScrollView>
        </View>

        <View style={[styles.sliderDefault]}>
          <ScrollView 
            horizontal={true}
            showsHorizontalScrollIndicator={false}
          >
            <TouchableOpacity 
              onPress={
                () => this.props.navigation.navigate('PromoDetail', { Component: { options: { bottomTabs: { visible: false, drawBehind: true, animate: true } } } })
              }>
              <PromoBox2
                image={require('../assets/promo-2-1.png')} 
                title="Buy 1 get 1 FREE!"
                span="Hot Deals!"
              />
            </TouchableOpacity>
            <TouchableOpacity 
              onPress={() => this.props.navigation.navigate('PromoDetail')}>
              <PromoBox2
                image={require('../assets/promo-2-2.png')} 
                title="Buy 2 get 1 FREE!"
                span="Hot Deals!"
              />
            </TouchableOpacity>
            <TouchableOpacity 
              onPress={() => this.props.navigation.navigate('PromoDetail')}>
              <PromoBox2
                image={require('../assets/promo-2-1.png')} 
                title="Buy 3 get 1 FREE!"
                span="Hot Deals!"
                lastSlide
              />
            </TouchableOpacity>
          </ScrollView>
        </View>

        <TextDivider 
          upper="Best Malls"
          under="near you!"
        />

        <View style={[styles.sliderBox]}>
          <ScrollView 
            horizontal={true}
            showsHorizontalScrollIndicator={false}
          >
            <TouchableOpacity 
              onPress={() => this.props.navigation.navigate('StorePage')}>
                <PromoBox1 
                  text="Mall title #1"
                  noDistance
                />
            </TouchableOpacity>
            <PromoBox1 
              text="Mall title #2"
              noDistance
            />
            <PromoBox1 
              text="Mall title #3"
              noDistance
            />
            <PromoBox1 
              text="Mall title #4"
              noDistance
            />
            <PromoBox1 
              text="Mall title #5"
              noDistance
            />
            <PromoBox1 
              text="Mall title #6"
              noDistance
              lastSlide
            />
          </ScrollView>
        </View>

        <TextDivider 
          upper=""
          under="Discover Promo"
        />

        <View style={[styles.sliderBox]}>
            <ScrollView 
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              <PromoBox3 
                title="Unilever Mega sale WEEK!"
                subtitle="Grand Indonesia"
                span="0.1 KM"
              />
              <PromoBox3 
                title="Unilever Mega sale WEEK!"
                subtitle="Grand Indonesia"
                span="0.1 KM"
              />
              <PromoBox3 
                title="Unilever Mega sale WEEK!"
                subtitle="Grand Indonesia"
                span="0.1 KM"
              />
              <PromoBox3 
                title="Unilever Mega sale WEEK!"
                subtitle="Grand Indonesia"
                span="0.1 KM"
              />
              <PromoBox3 
                title="Unilever Mega sale WEEK!"
                subtitle="Grand Indonesia"
                span="0.1 KM"
              />
              <PromoBox3 
                title="Unilever Mega sale WEEK!"
                subtitle="Grand Indonesia"
                span="0.1 KM"
                lastSlide
              />
            </ScrollView>
        </View>
        <View style={[styles.sliderBox]}>
          <ScrollView 
            horizontal={true}
            showsHorizontalScrollIndicator={false}
          >
            <PromoBox3 
              title="Unilever Mega sale WEEK!"
              subtitle="Grand Indonesia"
              span="0.1 KM"
            />
            <PromoBox3 
              title="Unilever Mega sale WEEK!"
              subtitle="Grand Indonesia"
              span="0.1 KM"
            />
            <PromoBox3 
              title="Unilever Mega sale WEEK!"
              subtitle="Grand Indonesia"
              span="0.1 KM"
            />
            <PromoBox3 
              title="Unilever Mega sale WEEK!"
              subtitle="Grand Indonesia"
              span="0.1 KM"
            />
            <PromoBox3 
              title="Unilever Mega sale WEEK!"
              subtitle="Grand Indonesia"
              span="0.1 KM"
            />
            <PromoBox3 
              title="Unilever Mega sale WEEK!"
              subtitle="Grand Indonesia"
              span="0.1 KM"
              lastSlide
            />
          </ScrollView>
        </View>

          <View style={{ width: '100%', height: 100, backgroundColor: 'rgba(0,0,0,0)' }}></View>

          <BoxAccent
            bottom={50}
            xAxis={335}
            width={200}
            height={200}
          ></BoxAccent>
          <BoxAccent
            bottom={-225}
            xAxis={25}
            width={350}
            height={250}
          ></BoxAccent>

      </ScrollView>
    );
  }
};

export default Nearby;

const page = StyleSheet.create({
  center: {
    paddingHorizontal: 30,
    marginHorizontal: 0,
  },
  header: {
    height: 75,
    alignItems: 'center',
  },
  headerTitle: {
    fontSize: 37,
    fontFamily: 'CeraPro-Bold',
    color: black,
  },
  headerSubtitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  subtitle: {
    fontSize: 16,
    fontFamily: 'CeraPro-Regular',
    color: primary,
    marginLeft: 8,
  },
  searchButtonWp: {
    justifyContent: 'flex-end',
  },
  searchButton: {
    flexDirection: 'row',
    height: 65,
    alignItems: 'center',
    backgroundColor: primary,
    paddingHorizontal: 12,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  text: {
    color: '#ffffff',
    fontSize: 14,
    marginLeft: 8,
    fontFamily: 'CeraPro-Regular',
  },
  // - - - -
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 12,
    backgroundColor: lightGrey,
    paddingHorizontal: 15,
    marginVertical: 24,
  },
  searchInput: {
    flex: 1,
    marginLeft: 15,
    paddingVertical: Platform.OS === 'ios' ? 15 : 15, 
  },
  // - - - -
  optionButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  // - - - -
  categorySlider: {
    marginVertical: 15,
  },
  categoryItem: {
    marginLeft: 35,
    fontFamily: 'CeraPro-Bold',
    fontSize: 18,
    color: darkGrey,
  },
  categoryItemLast: {
    marginRight: 35,
  },
  categoryItemActive: {
    color: primary,
  },
});








