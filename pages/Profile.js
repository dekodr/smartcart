import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , Modal
  , TouchableOpacity
  , TextInput 
  , SafeAreaView } from 'react-native';
import { SearchBar } from 'react-native-elements';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import { 
  styles
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey
  , fonts } from '../assets/style';

import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Carousel from 'react-native-snap-carousel';
import BoxAccent from '../components/BoxAccent';
import BoxAccent2 from '../components/BoxAccent2';
import MemberCard from '../components/MemberCard';
import DefaultList from '../components/DefaultList';

let ScreenHeight = Dimensions.get("window").height;

class Profile extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      entries: [1,2],
    }
  }

  static navigationOptions = {
    header: null
  }

  _renderItem () {
    return (
        <View style={page.slide}>
            <View style={page.card}>
              <MemberCard 
                amount="1.820"
                image={require('../assets/silver-member-bg.png')}
              />
            </View>
        </View>
    );
  }

  render() {
    return (
      <View style={page.container}>

        <ScrollView style={[styles.mcGrid, page.mcGrid]}>
          <ImageBackground style={page.gradientBg} source={require('../assets/linear-bg.png')}>
            <BoxAccent2
              yAxis={110}
              xAxis={-175}
              width={300}
              height={300}
            ></BoxAccent2>
            <BoxAccent2
              yAxis={-125}
              xAxis={25}
              width={200}
              height={200}
            ></BoxAccent2>
            <View style={page.radiusUpper}>
            </View>
          </ImageBackground>

          <SafeAreaView style={page.pageWrapper}>
            <View style={page.headerWrapper}>
              <View style={page.profilePictureWp}>
                <View style={page.verifiedIconWp}>
                  <View style={page.verifiedIcon}>
                    <Image
                      style={page.icon}
                      source={require('../assets/icon-check.png')}
                    />
                  </View>
                </View>
                <View style={page.profilePicture}>
                  <Image
                    style={page.picture}
                    source={require('../assets/profile-pic.png')}
                  />
                </View>
              </View>
              <Text style={page.userName}>Glantino putra_</Text>
              <Text style={page.userMemberStatus}>silver member</Text>
            </View>
            <View style={page.carouselWp}>
              <Carousel
                layout={'default'}
                ref={(c) => { this._carousel = c; }}
                data={this.state.entries}
                renderItem={this._renderItem}
                sliderWidth={wp('100%')}
                // loop={true}
                // loopClonesPerSide={2}
                inactiveSlideScale={1}
                inactiveSlideOpacity={1}
                enableMomentum={true}
                activeSlideAlignment={'start'}
                activeAnimationType={'spring'}
                activeAnimationOptions={{
                    friction: 4,
                    tension: 40
                }}
                slideStyle={{ overflow: 'visible' }}
                // autoplay={true}
                // autoplayDelay={500}
                // autoplayInterval={1000}
              />
            </View>
            <View style={page.listWp}>
              <DefaultList 
                name="Payment Method"
              />
              <DefaultList 
                name="Personal Information"
              />
              <DefaultList 
                name="My Rewards"
              />
            </View>
          </SafeAreaView>
        </ScrollView>
        
      </View>
    );
  }
};

export default Profile;

const page = StyleSheet.create({
  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
  },

  mcGrid: {
    height: 500,
  },

  gradientBg: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: hp('58%'),
  },

  radiusUpper: {
    backgroundColor: '#F5FCFF',
    position: 'absolute',
    left: 0,
    top: hp('54%'),
    width: '100%',
    height: 35,
    borderTopRightRadius: 16,
    borderTopLeftRadius: 16,
  },

  center: {
    paddingHorizontal: 30,
    marginHorizontal: 0,
  },

  pageWrapper: {
    position: 'relative',
    zIndex: 1,
  },
  headerWrapper: {
    width: '100%',
    alignItems: 'center',
    paddingTop: hp('8%'),
  },
  profilePictureWp: {
    backgroundColor: 'rgba(255,255,255,.25)',
    height: 90,
    width: 90,
    alignItems: 'center',
    borderRadius: 55,
    justifyContent: 'center',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 8 },
    shadowOpacity: 0.3,
    shadowRadius: 8,
    elevation: 7,
  },
  profilePicture: {
    height: 77,
    width: 77,
    borderRadius: 55,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    zIndex: 1,
  },
  picture: {
    height: 77,
    width: 77,
  },
  verifiedIconWp: {
    position: 'absolute',
    right: -10,
    zIndex: 2,
    height: 27,
    width: 27,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#CCE8FF',
    borderRadius: 30,
  },
  verifiedIcon: {
    height: 21,
    width: 21,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#4FA7F2',
    borderRadius: 30,
  },
  icon: {
    height: 10,
    width: 10,
  },
  userName: {
    fontFamily: 'CeraPro-Bold',
    textTransform: 'capitalize',
    fontSize: 25,
    color: '#fff',
    marginTop: 25,
    marginBottom: 5,
  },
  userMemberStatus: {
    fontFamily: 'CeraPro-Regular',
    fontSize: 12,
    color: '#fff',
    letterSpacing: 3,
    textTransform: 'uppercase',
    marginBottom: 10,
  },

  carouselWp: {
    marginTop: 35,
    paddingBottom: 75,
  },
  slide: {
    width: '100%',
    height: 220,
    // backgroundColor: grey,
    paddingLeft: 15,
    alignItems: 'center',
    justifyContent: 'center',
    overflow: 'visible',
  },

  card: {
    width: '100%',
    height: '100%',
    overflow: 'visible',
  },

  listWp: {
    paddingHorizontal: 25,
    paddingVertical: 55,
    marginTop: -130,
  },
  

});








