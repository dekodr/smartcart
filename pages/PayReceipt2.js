import React from 'react';
import PropTypes from 'prop-types';
import { 
  View
  , Platform
  , Text
  , StyleSheet
  , Dimensions 
  , TouchableHighlight
  , TouchableOpacity
  , Image
  , ImageBackground
  , ScrollView
  , TextInput
  , SafeAreaView 
  , Linking} from 'react-native';

import Modal from "react-native-modal";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import NumberFormat from 'react-number-format';

import PromoBoxNearby from '../components/PromoBoxNearby';
import BoxAccent from '../components/BoxAccent';
import Button from '../components/Button';

import { 
  styles
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

let ScreenHeight = Dimensions.get("window").height;

class PayReceipt2 extends React.Component {
  static navigationOptions = {
    header: null
  }

  state = {
    search     : '',
    receipt_id : '',
    loading    : true,
    orderList  : [],
    swipeablePanelActive : false,
    modalVisible    : false,
    selectedProduct : '',
    selectedURL     : '',
    selectedImage   : '',
  };

  updateSearch = search => {
    this.setState({ search });
  };

  openPanel(product, link, image){
    if(link == '' || link == 'http://'){
      return false;
    }else{
      this.setState({ isModalVisible: !this.state.isModalVisible, selectedProduct: product, selectedURL: link, selectedImage: image });
    }
  }

  closePanel =() =>{
    this.setState({ swipeablePanelActive: false });
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  };

  openURL = () => {
    
    Linking.canOpenURL(this.state.selectedURL).then(supported => {
      if (supported) {
        Linking.openURL(this.state.selectedURL);
      } else {
        console.log("Don't know how to open URI: " + this.state.selectedURL);
      }
    });
  };


  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  // Load Data to retrieve in page
  async componentDidMount(){
    try {
      let response = await fetch(
        'http://103.3.62.188/osmosis/getData_/'+this.props.navigation.state.params.receipt_id,
      );
      // +this.props.navigation.state.params.receipt_id
      let responseJson = await response.json();

      this.setState({orderList: responseJson, loading: false});
      console.log("orderlist:")
      console.log(this.state.orderList);

    }catch (error) {
      console.error(error);
    }
    console.log("component mount");
  }

  
  renderOrderList() {
    const productImg = {
      25272: require('../assets/product/25272.jpg'),
      25273: require('../assets/product/25273.jpg'),
      25274: require('../assets/product/25274.jpg'),
      25275: require('../assets/product/25275.jpg'),
      25276: require('../assets/product/25276.jpg'),
      25277: require('../assets/product/25277.jpg'),
      25278: require('../assets/product/25278.jpg'),
      25279: require('../assets/product/25279.jpg'),
      25280: require('../assets/product/25280.jpg'),
      25281: require('../assets/product/25281.jpg'),
      25282: require('../assets/product/25282.jpg'),
      25283: require('../assets/product/25283.jpg'),
      25284: require('../assets/product/25284.jpg'),
      25285: require('../assets/product/25285.jpg'),
      25286: require('../assets/product/25286.jpg'),
      25287: require('../assets/product/25287.jpg'),
      // 25288: require('../assets/product/25288.jpg'),
      25271: require('../assets/product/25271.jpg'),
      25270: require('../assets/product/25270.jpg'),
      25269: require('../assets/product/25269.jpg'),
      25268: require('../assets/product/25268.jpg'),
      25267: require('../assets/product/25267.jpg'),
      25266: require('../assets/product/25266.jpg'),
      25262: require('../assets/product/25262.jpg'),
      25261: require('../assets/product/25261.jpg'),
      25260: require('../assets/product/25260.jpg'),
      25259: require('../assets/product/25259.jpg'),
      25258: require('../assets/product/25258.jpg'),
      25265: require('../assets/product/25265.jpg'),
      25264: require('../assets/product/25264.jpg'),
      25263: require('../assets/product/25263.jpg'),
    }
    let data = this.state.orderList.detail;
    console.log(data)
    return (<View>
                { data.map((item, key)=>(
                  <View>
                    {item.website !== null ? 
                    //info : Ini untuk yang bisa popup ya
                      <TouchableOpacity
                      onPress={() => { this.openPanel(item.product_name, item.website, item.product_id); }}
                        style={[page.middleLT, page.LthasMod]}>
                        <Text style={page.LtName}>{item.product_name}</Text>
                        <Text style={page.LtAmount}>{item.total_item}</Text>
                        <Text style={page.LtPrice}><NumberFormat value={item.total_price} displayType={'text'} thousandSeparator={true} prefix={''} renderText={value => <Text>{value}</Text>}/></Text>
                        <Text style={page.LtSubtotal}><NumberFormat value={item.total_price} displayType={'text'} thousandSeparator={true} prefix={''} renderText={value => <Text>{value}</Text>}/></Text>
                      </TouchableOpacity>
                      // info - - - END
                    : 
                      //info : Ini untuk yang ga bisa popup ya
                      <View style={[page.middleLT]}>
                        <Text style={page.LtName}>{item.product_name}</Text>
                        <Text style={page.LtAmount}>{item.total_item}</Text>
                        <Text style={page.LtPrice}><NumberFormat value={item.total_price} displayType={'text'} thousandSeparator={true} prefix={''} renderText={value => <Text>{value}</Text>}/></Text>
                        <Text style={page.LtSubtotal}><NumberFormat value={item.total_price} displayType={'text'} thousandSeparator={true} prefix={''} renderText={value => <Text>{value}</Text>}/></Text>
                      </View>
                      // info - - - END
                    }
                  </View>
                  
                  

                  

                  // <TouchableOpacity 
                  // style={styles.ReceiptItem}>
                  //   <View style={styles.ReceiptIcon}>
                  //     <Image
                  //       style={{width: 50, height: 50}}
                  //       source={productImg[item.product_id]}/>
                  //   </View>
                    
                  //   <View style={styles.historyName}>
                  //     {/* BUTTON TOUCHABLE TO OPEN WEBSITE IF ANY */}
                  //       {item.website == null ?  <Text style={styles.ReceiptSubtitle}>{item.product_name}</Text> : <TouchableOpacity onPress={() => { this.openPanel(item.product_name, item.website, item.product_id); }}><Text style={styles.ReceiptSubtitleBold}>{item.product_name}</Text></TouchableOpacity>}
                  //   </View>
                  //   <View style={styles.amountWp}>
                  //     <View style={styles.ReceiptAmount}>
                  //       <Text style={styles.ReceiptAmountText}>x{item.total_item}</Text>
                  //     </View>
                  //     <View style={styles.ReceiptPrice}>
                  //       <Text style={styles.ReceiptPriceText}>
                  //         <NumberFormat value={item.total_price} displayType={'text'} thousandSeparator={true} prefix={''} renderText={value => <Text>{value}</Text>}/>
                  //       </Text>
                  //     </View>
                  //   </View>
                  // </TouchableOpacity>
                ))}

          </View>
          );
  }
  
  redirectToPayment(){
    console.log(this.state.orderList.transaction_total)

    this.props.navigation.navigate('PayEnterPin', { transaction_total:this.state.orderList.transaction_total })
  }


  render() {
    const mapMarker = {
      position: 'absolute',
      height: 55,
      width: 55,
    };

    const { search }      = this.state;
    const { receipt_id }  = this.props.navigation.state.params.receipt_id;
    let id_ = this.state.selectedImage.toString();

   
    
    if(this.state.loading == true){
      return (
        <View></View>
      );
    }else{
      return (
        <SafeAreaView style={{ position: 'relative', zIndex: 99, backgroundColor: '#f8f8f8'}}>
          <View style={[styles.mcGrid, page.top]}>

            <View style={[styles.mcRow, styles.topNavigation]}>
              <TouchableOpacity 
                style={[styles.mcCol1, styles.backButton]}
                onPress={() => this.props.navigation.goBack()}>
                <Image
                  style={{width: 35, height: 35}}
                  source={require('../assets/icon-chevron-left.png')}
                />
              </TouchableOpacity>
              <View style={[styles.mcCol4, styles.topNavTitle]}>
                <Text style={styles.pageTitle}></Text>
              </View>
              <View style={[styles.mcCol3, styles.leftIcons]}>
                
              </View>
            </View>

          </View>
          <ScrollView style={[styles.mcGrid, page.pageWrapper]}>

            <View style={[styles.mcRow, page.center, page.header]}>
              <View style={styles.mcCol6}>
                <Text style={page.headerTitle}>{this.state.orderList.store}</Text>
                <Text style={page.headerDate}>{this.state.orderList.transaction_date}</Text>
              </View>
            </View>

            {/* Search Bar HIDDEN */}
            <View style={[styles.mcRow, page.center, {display: 'none'}]}>
              <View style={[styles.mcCol6, {}]}>
                <View style={page.searchBar}>
                  <Image
                    style={{width: 18, height: 18}}
                    source={require('../assets/icon-search-dollar.png')}
                  />
                  <TextInput 
                    style={page.searchInput}
                    placeholder={"Search Transaction"}
                    placeholderTextColor={'#798191'}
                    onChangeText={this.updateSearch}
                    value={search}
                  />
                </View>
              </View>
              <View style={[styles.mcCol1, page.optionButton]}>
                <Image
                  style={{width: 20, height: 20}}
                  source={require('../assets/icon-options.png')}
                />
              </View>
            </View>
            {/* Search Bar - - -  END*/}

            <View style={[styles.mcRow, page.center, page.historyWrapper]}>
              
              <View style={page.RcWp}>
                {/* info : Ini Header */}
                <View style={page.RcHeader}>
                  <View style={page.RcHIcon}>
                    <Image
                      style={page.RcHIconImg}
                      source={require('../assets/brand-piko-mart.png')}
                    />
                  </View>
                  <Text style={page.RcHText}>{this.state.orderList.store}</Text>
                  <Text style={page.RcHText}>{this.state.orderList.store_address}</Text>
                </View>
                {/* info - - - END */}
                <Text numberOfLines={1} style={[page.dashedLine, {}]}>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</Text>
                <View style={page.RcMiddle}>
                  {this.renderOrderList()}
                  {/* info : ini total */}
                  <View style={page.middleTotal}>
                    <View style={page.LtTotal}>
                      <Text style={page.totalText}>total</Text>
                      <Text style={page.totalDotDivider}>:</Text>
                      <Text style={page.totalNumber}><NumberFormat value={this.state.orderList.transaction_total} displayType={'text'} thousandSeparator={true} prefix={''} renderText={value => <Text>{value}</Text>}/></Text>
                    </View>
                    <View style={page.LtTotal}>
                      <Text style={page.totalText}>tunai</Text>
                      <Text style={page.totalDotDivider}>:</Text>
                      <Text style={page.totalNumber}>1,000,000</Text>
                    </View>
                    <View style={page.LtTotal}>
                      <Text style={page.totalText}>kembali</Text>
                      <Text style={page.totalDotDivider}>:</Text>
                      <Text style={page.totalNumber}>63,2000</Text>
                    </View>
                  </View>
                  {/* info - - - END */}
                  <View style={[page.middlePPN]}>
                    <Text style={[page.LtPrice, {marginRight: 20}]}>DPP=16,663</Text>
                    <Text style={page.LtPrice}>PPN=1,664</Text>
                  </View>
                </View>
                <Text numberOfLines={1} style={[page.dashedLine, {}]}>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</Text>
                {/* info : Ini footer terimakasih */}
                <View style={page.RcFooter}>
                  <Text style={[page.RcHText, page.footerText]}>terima kasih, selamat belanja kembali</Text>
                  <Text style={[page.RcHText, page.footerText]}>=-=-=-=-layanan konsumen piko mart-=-=-=-=</Text>
                  <Text style={[page.RcHText, page.footerText]}>085695263075</Text>
                </View>
                {/* info - - - END */}
              </View>

            </View>
            {/* info : Ini transaction-info */}
            <View style={[page.transactionInfo, {}]}>
              <Text style={page.trsTitle}>Transaction <Text style={{fontWeight: '700'}}>Info</Text></Text>
              <View style={page.trsInfo}>
                <Text style={page.trsInfoText}>Total Payment</Text>
                <View style={page.trsInfoNumSup}>
                  <Text style={page.supScript}>Rp</Text>
                  <Text style={page.trsInfoNumber}><NumberFormat value={this.state.orderList.transaction_total} displayType={'text'} thousandSeparator={true} prefix={''} renderText={value => <Text>{value}</Text>}/></Text>
                </View>
              </View>
              <View style={page.trsInfo}>
                <Text style={page.trsInfoText}>Points</Text>
                <Text style={[page.trsInfoNumber, {fontWeight: '700'}]}>{Math.round(this.state.orderList.transaction_total * 0.0003)}</Text>
              </View>
              <View style={page.trsShare}>
                <View style={page.trsShareTitle}>
                  <Text style={[page.trsInfoText, {fontWeight: '300',}]}>Share and</Text>
                  <Text style={page.trsInfoText}>Get Points</Text>
                </View>
                <View style={page.trsShareIcons}>
                  <Image
                    style={[page.trsShareIcon, {height: 30, width: 30}]}
                    source={require('../assets/icon-whatsapp-white.png')}
                  />
                  <Image
                    style={[page.trsShareIcon, {height: 30, width: 15}]}
                    source={require('../assets/icon-facebook-white.png')}
                  />
                </View>
              </View>
            </View>
            {/* info - - - END */}

          </ScrollView>

          {/* Modal Area, INFO : ini yg versi baru ya, yang versi lama yg bawah */}
          <Modal isVisible={this.state.isModalVisible}>
            <View style={page.modalWp}>
              <View style={page.modalInner}>
                <Image
                    style={page.modalImgAccent}
                    source={require('../assets/modal-img-accent.png')}
                  />
                {/* info : button closah */}
                <TouchableOpacity 
                  style={page.closeButton}
                  onPress={this.toggleModal}>
                    <Image
                      style={{width: 10, height: 10}}
                      source={require('../assets/icon-times-white.png')}
                    />
                </TouchableOpacity>
                {/* info : nama barangnye dimareh */}
                <Text style={page.modalTextName}>{this.state.selectedProduct}</Text>
                <View style={page.ModalrowButton}>
                  {/* info : button info */}
                  <TouchableOpacity style={page.ModalButtonWp}>
                    <ImageBackground source={require('../assets/bg-info.png')} style={page.ModalButton}>
                      <Text style={page.ModalButtonText}>Nutrional Information</Text>
                    </ImageBackground>
                  </TouchableOpacity>
                {/* info : button recipess */}
                  <TouchableOpacity onPress={() => this.openURL(this.state.selectedURL)} style={[page.ModalButtonWp, {}]}>
                    <ImageBackground source={require('../assets/bg-recipe.png')} style={page.ModalButton}>
                      <Text style={page.ModalButtonText}>Recipes</Text>
                    </ImageBackground>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </Modal>

          {/*<Modal isVisible={this.state.isModalVisible}>
            <View style={{ backgroundColor: '#fff', height: 125, width: '100%', borderRadius: 16, paddingTop: 45, paddingHorizontal: 25, paddingBottom: 25}}>
              <TouchableOpacity 
                style={page.closeButton}
                onPress={this.toggleModal}>
                  <Image
                    style={{width: 10, height: 10}}
                    source={require('../assets/icon-times-white.png')}
                  />
              </TouchableOpacity>
              <View style={page.rowButton}>
                <TouchableOpacity style={page.ReceiptButton}>
                  <Text style={page.ReceiptButtonText}>Find Info</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[page.ReceiptButton, {backgroundColor: '#57789F'}]}>
                  <Text style={page.ReceiptButtonText}>Find Recipes</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>*/}
          {/* Modal Area - - - END */}
        </SafeAreaView>
      );
    }
  }
}

export default PayReceipt2;


const page = StyleSheet.create({

  // N E W  S T Y L E , L I K E  N E W  N E W , E V E R Y W E E K  N E W  N E W
  RcWp: {
    width: '100%',
    backgroundColor: '#fff',
    borderRadius: 16,
    paddingVertical: 15,
    paddingHorizontal: 25,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 8 },
    shadowOpacity: 0.3,
    shadowRadius: 12,
    elevation: 10,
    overflow: 'hidden',
  },
  // R E C E I P T  H E A D E R
  RcHeader: {
    width: '100%',
    paddingTop: 15,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  RcHIcon: {
    position: 'relative',
    height: 75,
    width: 75,
    borderRadius: 75,
    overflow: 'hidden',
    marginBottom: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  RcHIconImg: {
    position: 'absolute',
    height: 75,
    width: 75,
  },
  RcHText: {
    fontSize: 12,
    fontFamily: 'CeraPro-Light',
    color: grey,
    fontWeight: '300',
    textAlign: 'center',
    lineHeight: 25
  },
  // R E C E I P T  H E A D E R - - - E N D
  // R E C E I P T  M I D D L E
  RcMiddle: {
    // marginVertical: 8, 
    width: '100%',
  },
  middleLT: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    marginBottom: 8,
    padding: 5,
    borderRadius: 2,
  },
  LthasMod: {
    backgroundColor: '#E8FAFF',
  },
  LtName: {
    width: '50%',
    fontSize: 12,
    fontFamily: 'CeraPro-Bold',
    color: grey,
    fontWeight: '700',
    lineHeight: 18,
    textTransform: 'uppercase',
  },
  LtAmount: {
    fontSize: 12,
    fontFamily: 'CeraPro-Light',
    color: grey,
    fontWeight: '300',
  },
  LtPrice: {
    fontSize: 12,
    fontFamily: 'CeraPro-Light',
    color: grey,
    fontWeight: '300',
  },
  LtSubtotal: {
    fontSize: 12,
    fontFamily: 'CeraPro-Light',
    color: grey,
    fontWeight: '300',
  },
  middleTotal: {
    width: '100%',
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  LtTotal: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  totalText: {
    width: 75,
    fontSize: 12,
    fontFamily: 'CeraPro-Light',
    color: grey,
    fontWeight: '300',
    textAlign: 'left',
    textTransform: 'uppercase',
  },
  totalDotDivider: {
    width: 10,
    fontSize: 12,
    fontFamily: 'CeraPro-Light',
    color: grey,
    fontWeight: '300',
    textAlign: 'center',
  },
  totalNumber: {
    width: 75,
    fontSize: 12,
    fontFamily: 'CeraPro-Light',
    color: grey,
    fontWeight: '300',
    textAlign: 'right',
  },
  middlePPN: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginTop: 15,
  },
  // R E C E I P T  M I D D L E - - - E N D
  RcFooter: {
    width: '100%',
    paddingVertical: 15,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  footerText: {
    textTransform: 'uppercase',
  },
  dashedLine: {
    fontSize: 12,
    fontFamily: 'CeraPro-Light',
    color: grey,
    fontWeight: '300',
    // paddingHorizontal: 20,
    overflow: 'hidden',
    width: '100%',
    textAlign: 'center',
    marginVertical: 8,
  },

  // T R A N S A C T I O N  I N F O
  transactionInfo: {
    paddingBottom: 100,
    paddingHorizontal: 30,
    paddingTop: 200,
    backgroundColor: primary,
    borderTopLeftRadius: 22,
    borderTopRightRadius: 22,
    position: 'relative',
    zIndex: 1,
    marginTop: -175,
  },
  trsTitle: {
    fontSize: 25,
    fontFamily: 'CeraPro-Regular',
    color: '#fff',
    fontWeight: '400',
    marginBottom: 20,
  },
  trsInfo: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    marginBottom: 10,
  },
  trsInfoText: {
    fontSize: 18,
    fontFamily: 'CeraPro-Bold',
    color: '#fff',
    fontWeight: '700',
  },
  trsInfoNumSup: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  trsInfoNumber: {
    fontSize: 32,
    fontFamily: 'CeraPro-Regular',
    color: '#fff',
    fontWeight: '400',
    alignItems: 'flex-start',
  },
  supScript: {
    fontSize: 15,
    fontFamily: 'CeraPro-Regular',
    color: '#fff',
    fontWeight: '400',
    marginRight: 10,
    marginTop: 10,
  },
  trsShare: {
    width: '100%',
    marginTop: 35,
    padding: 25,
    borderRadius: 16,
    backgroundColor: '#3CA794',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  trsShareIcons: {
    flexDirection: 'row',
  },
  trsShareIcon: {
    marginLeft: 25,
  },
  // T R A N S A C T I O N  I N F O - - - E N D

  // M O D A L
  modalWp: {
    backgroundColor: 'rgba(0,0,0,0)',
    height: 250,
    width: '100%',
    paddingTop: 45,
  },
  modalInner: {
    backgroundColor: '#ddd',
    height: '100%',
    width: '100%',
    borderRadius: 16,
    paddingTop: 45,
    paddingHorizontal: 25,
    paddingBottom: 25
  },
  modalImgAccent: {
    position: 'absolute',
    alignSelf: 'flex-start',
    top: -75,
    left: 25,
    height: 100,
    width: 100,
  },
  modalTextName: {
    fontSize: 20,
    fontFamily: 'CeraPro-Regular',
    color: grey,
    fontWeight: '400',
    marginBottom: 25,
  },
  ModalrowButton: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  ModalButtonWp: {
    width: '47%',
    height: 85,
    // padding: 15,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: '#77599A',
    borderRadius: 8,
    overflow: 'hidden',
  },
  ModalButton: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
    zIndex: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  ModalButtonText: {
    fontSize: 17,
    fontFamily: 'CeraPro-Bold',
    color: '#ffffff',
    fontWeight: '700',
    position: 'relative',
    zIndex: 1,
    textAlign: 'center',
  },
  // M O D A L - - - E N D
  // E N D
  

  top: {
    zIndex: 3,
  },
  closeButton: {
    height: 25,
    width: 25,
    backgroundColor: 'rgba(211,62,62,1)',
    borderRadius: 25,
    position: 'absolute',
    right: 7,
    top: 7,
    alignItems: 'center',
    justifyContent: 'center',
  },

  listTax: {
    marginTop: 5,
  },
  alignLeft: {
    textAlign: 'left',
    fontWeight: '700',
    fontSize: 18,
    color: '#2A3348',
  },
  alignRight: {
    textAlign: 'right',
    fontWeight: '700',
    fontSize: 18,
    color: '#2A3348',
  },
  total: {
    fontSize: 20,
    color: '#36A49F',
  },

  pageWrapper: {
    marginTop: -25,
    paddingTop: 0,
    position: 'relative',
    zIndex: 2,
    // paddingBottom: 250,
    paddingHorizontal: 0,
    // marginBottom: 50,
  },
  center: {
    paddingHorizontal: 30,
    marginHorizontal: 0,
  },
  header: {
    height: 75,
    alignItems: 'center',
    marginTop: 85,
    paddingBottom: 0,
  },
  headerTitle: {
    fontSize: 27,
    fontFamily: 'CeraPro-Bold',
    color: black,
    fontWeight: '700',
  },
  headerDate: {
    fontSize: 15,
    fontFamily: 'CeraPro-Regular',
    color: grey,
    letterSpacing: 1.2,
  },
  headerSubtitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  subtitle: {
    fontSize: 16,
    fontFamily: 'CeraPro-Regular',
    color: '#ffffff',
    marginLeft: 8,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: 0, height: 1},
    textShadowRadius: 1,
    letterSpacing: .8,
  },
  searchButtonWp: {
    justifyContent: 'flex-end',
  },
  searchButton: {
    flexDirection: 'row',
    height: 65,
    alignItems: 'center',
    backgroundColor: primary,
    paddingHorizontal: 15,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  text: {
    color: '#ffffff',
    fontSize: 16,
    marginLeft: 8,
  },
  // - - - -
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 12,
    backgroundColor: lightGrey,
    paddingHorizontal: 15,
    marginVertical: 15,
  },
  searchInput: {
    flex: 1,
    marginLeft: 15,
    paddingVertical: Platform.OS === 'ios' ? 15 : 15, 
  },
  // - - - -
  optionButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  // - - - -
  historyWrapper: {
    flexDirection: 'column',
    marginVertical: 25,
    position: 'relative',
    zIndex: 9,
    // paddingBottom: 100,
  },
  dateDivider: {
    marginVertical: 15,
  },
  textDivider:{
    fontSize: 16,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
    textTransform: 'uppercase',
    letterSpacing: 2,
    marginBottom: 15,
  },
  ReceiptIcon: {
    height: 50,
    width: 50,
    borderRadius: 50,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
  },
  ReceiptItem: {
    width: '100%',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 8,
    backgroundColor: lightGrey,
    flexDirection: 'row',
    marginBottom: 15,
    alignItems: 'center',
  },
  ReceiptTitle: {
    fontSize: 16,
    color: black,
    fontFamily: 'CeraPro-Bold',
    marginBottom: 2,
    width: wp('30%'),
  },
  ReceiptSubtitle: {
    textTransform: 'capitalize',
    fontSize: 13,
    color: black,
    width: wp('30%'),
  },
  ReceiptPrice: {
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    flex: 6,
    paddingHorizontal: 0,
    marginHorizontal: 0,
  },
  ReceiptPriceText: {
    textAlign: 'right',
    fontSize: 16,
    color: primary2,
    fontFamily: 'CeraPro-Bold',
  }

});