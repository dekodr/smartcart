import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , TouchableOpacity 
  , TextInput
  , SafeAreaView } from 'react-native';

  import { createAppContainer } from 'react-navigation';
  import { createStackNavigator } from 'react-navigation-stack';
  import { createBottomTabNavigator } from 'react-navigation-tabs';
import Modal from "react-native-modal";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
// import renderIf from './renderIf';

import { 
  styles
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

import PromoBoxNearby from '../components/PromoBoxNearby';
import BoxAccent from '../components/BoxAccent';
import Button from '../components/Button';

let ScreenHeight = Dimensions.get("window").height;

class TodayReceiptDetail extends React.Component {
  static navigationOptions = {
    header: null
  }

  constructor(){
    super();
    this.state ={
      status:false
    }
  }

  toggleStatus(){
    this.setState({
      status:!this.state.status
    });
    console.log('toggle button handler: '+ this.state.status);
  }

  doAtOnce(){
    {this.toggleStatus()}; 
    {this.toggleModal};
  }

  state = {
    isModalVisible: false
  };

  state = {
    pressed: false
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };
  
  state = {
    search: '',
  };

  updateSearch = search => {
    this.setState({ search });
  };

  render() {
    const { search } = this.state;

    const mapMarker = {
      position: 'absolute',
      height: 55,
      width: 55,
    };

    return (
        <SafeAreaView style={{ position: 'relative', zIndex: 9 }}>
          <View style={[styles.mcGrid, page.top]}>

            <View style={[styles.mcRow, styles.topNavigation]}>
              <TouchableOpacity 
                style={[styles.mcCol1, styles.backButton]}
                onPress={() => this.props.navigation.goBack()}>
                <Image
                  style={{width: 35, height: 35}}
                  source={require('../assets/icon-chevron-left.png')}
                />
              </TouchableOpacity>
              <View style={[styles.mcCol4, styles.topNavTitle]}>
                <Text style={styles.pageTitle}></Text>
              </View>
              <View style={[styles.mcCol3, styles.leftIcons]}>
                
              </View>
            </View>

          </View>
          <ScrollView style={[styles.mcGrid, page.pageWrapper, {}]}>
            <BoxAccent
              yAxis={-25}
              xAxis={325}
              width={200}
              height={200}
            ></BoxAccent>
            <BoxAccent
              yAxis={-200}
              xAxis={-100}
              width={300}
              height={300}
            ></BoxAccent>

            <View style={[styles.mcRow, page.center, page.header]}>
              <View style={styles.mcCol6}>
                <Text style={page.headerTitle}>Share Smartcart !</Text>
                <Text style={page.headerDate}></Text>
              </View>
            </View>

            {/* Search Bar HIDDEN */}
            <View style={[styles.mcRow, page.center, {display: 'none'}]}>
              <View style={[styles.mcCol6, {}]}>
                <View style={page.searchBar}>
                  <Image
                    style={{width: 18, height: 18}}
                    source={require('../assets/icon-search-dollar.png')}
                  />
                  <TextInput 
                    style={page.searchInput}
                    placeholder={"Search Transaction"}
                    placeholderTextColor={'#798191'}
                    onChangeText={this.updateSearch}
                    value={search}
                  />
                </View>
              </View>
              <View style={[styles.mcCol1, page.optionButton]}>
                <Image
                  style={{width: 20, height: 20}}
                  source={require('../assets/icon-options.png')}
                />
              </View>
            </View>
            {/* Search Bar - - -  END*/}

            <View style={[styles.mcRow, page.center, page.historyWrapper, {}]}>
              {/* button share */}
                <TouchableOpacity 
                  style={page.shareItem}
                  onPress={this.toggleModal}>
                    <View style={[page.shareItemIcon, {height: 45, width: 23}]}>
                      <Image
                        style={page.ShrIcon}
                        source={require('../assets/icon-facebook-white.png')}
                      />
                    </View>
                    <View style={page.shareItemText}>
                      <Text style={page.ShrText}>Share on facebook get <Text style={page.TextBold}>15 points</Text></Text>
                    </View>
                    <View style={page.circleAccent}>
                      <View style={[page.circleAccentInner]}></View>
                      {/*{renderIf(this.state.status)(
                        <View style={[page.circleAccentInner]}></View>
                      )}*/}
                    </View>
                </TouchableOpacity>
              {/* button share - - - END */}
              {/* button share */}
                <TouchableOpacity 
                  style={[page.shareItem, {backgroundColor: '#60C4F8',}]}
                  onPress={this.toggleModal}>
                    <View style={[page.shareItemIcon, {height: 35, width: 37}]}>
                      <Image
                        style={page.ShrIcon}
                        source={require('../assets/icon-twitter-white.png')}
                      />
                    </View>
                    <View style={page.shareItemText}>
                      <Text style={page.ShrText}>Share on Twitter get <Text style={page.TextBold}>15 points</Text></Text>
                    </View>
                    <View style={page.circleAccent}>
                      <View style={page.circleAccentInner}></View>
                    </View>
                </TouchableOpacity>
              {/* button share - - - END */}
              {/* button share */}
                <TouchableOpacity 
                  style={[page.shareItem, {backgroundColor: '#AF6EBA',}]}
                  onPress={this.toggleModal}>
                    <View style={page.shareItemIcon}>
                      <Image
                        style={page.ShrIcon}
                        source={require('../assets/icon-instagram-white.png')}
                      />
                    </View>
                    <View style={page.shareItemText}>
                      <Text style={page.ShrText}>Share on Instagram get <Text style={page.TextBold}>15 points</Text></Text>
                    </View>
                    <View style={page.circleAccent}>
                      <View style={page.circleAccentInner}></View>
                    </View>
                </TouchableOpacity>
              {/* button share - - - END */}

              {/* old afraid that be unused, FROM BOTTOM HERE */}
              
              {/* old afraid that be unused, FROM BOTTOM HERE - - - END*/}

            </View>

            {/*<BoxAccent
              bottom={0}
              xAxis={335}
              width={200}
              height={200}
            ></BoxAccent>
            <BoxAccent
              bottom={-285}
              xAxis={25}
              width={350}
              height={250}
            ></BoxAccent>*/}

          </ScrollView>

          {/* Modal Area */}  
          <Modal isVisible={this.state.isModalVisible}>
            <View style={page.modalWp}>
              <View style={page.modalInner}>
                <Image
                    style={page.modalImgAccent}
                    source={require('../assets/success-img.png')}
                  />
                {/* info : button closah */}
                <TouchableOpacity 
                  style={page.closeButton}
                  onPress={this.toggleModal}>
                    <Image
                      style={{width: 10, height: 10}}
                      source={require('../assets/icon-times-white.png')}
                    />
                </TouchableOpacity>
                {/* info : nama barangnye dimareh */}
                <Text style={page.modalTextName}>Yay! You Get</Text>
                <View style={page.ModalrowButton}>
                  {/* info : button info */}
                  <TouchableOpacity 
                    style={page.ModalButtonWp}
                    onPress={this.toggleModal}>
                    <Text style={page.ModalButtonText}>15 Points</Text>
                  </TouchableOpacity>

                  <Text style={page.TextSmall, {color: grey, textAlign: 'center'}}>Check your accounf for more info</Text>
                </View>
              </View>
            </View>
          </Modal>
          {/* Modal Area - - - END */}
        </SafeAreaView>
    );
  }
};

export default TodayReceiptDetail;

const page = StyleSheet.create({


  // NEW NEW NEW
  TextBig: {
    fontSize: 32,
  },
  TextSmall: {
    fontSize: 12,
  },
  TextFzMedium: {
    fontSize: 22,
  },
  TextWhite: {
    color: '#fff',
  },
  TextBlack: {
    color: black,
  },
  TextBold: {
    fontWeight: '700',
    fontFamily: 'CeraPro-Bold',
  },
  TextFwMedium: {
    fontWeight: '400',
    fontFamily: 'CeraPro-Regular',
  },
  TextLight: {
    fontWeight: '300',
    fontFamily: 'CeraPro-Light',
  },
  TextCapitalize: {
    textTransform: 'capitalize',
  },
  // B U T T O N  S H A R E  N E W
  shareItem: {
    backgroundColor: '#4481BE',
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 20,
    paddingHorizontal: 25,
    borderRadius: 16,
    overflow: 'hidden',
    marginBottom: 25,
  },
  shareItemIcon: {
    height: 45,
    width: 45,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ShrIcon: {
    height: '100%',
    width: '100%'
  },
  shareItemText: {
    marginLeft: 20,
    width: 150,
  },
  ShrText: {
    lineHeight: 25,
    fontSize: 15,
    fontWeight: '400',
    color: '#fff',
  },
  circleAccent: {
    backgroundColor: 'rgba(0,0,0,.4)',
    width: 45,
    height: 45,
    borderRadius: 45,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 'auto',
  },
  circleAccentInner: {
    width: 30,
    height: 30,
    borderRadius: 35,
    backgroundColor: '#fff',
  },
  // B U T T O N  S H A R E  N E W - - - END
  // M O D A L
  modalWp: {
    backgroundColor: 'rgba(0,0,0,0)',
    height: 350,
    width: '100%',
    paddingTop: 45,
  },
  modalInner: {
    backgroundColor: '#fff',
    height: '100%',
    width: '100%',
    borderRadius: 16,
    paddingTop: 45,
    paddingHorizontal: 25,
    paddingBottom: 25
  },
  modalImgAccent: {
    position: 'absolute',
    alignSelf: 'center',
    top: -160,
    height: 250,
    width: 250,
  },
  modalTextName: {
    fontSize: 32,
    fontFamily: 'CeraPro-Bold',
    color: black,
    fontWeight: '700',
    marginBottom: 25,
    textAlign: 'center',
    marginTop: 25,
  },
  ModalrowButton: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  ModalButtonWp: {
    width: '80%',
    height: 85,
    // padding: 15,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: primary,
    borderRadius: 16,
    overflow: 'hidden',
    marginBottom: 25,
  },
  ModalButton: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
    zIndex: 0,
    justifyContent: 'center',
    alignItems: 'center',

  },
  ModalButtonText: {
    fontSize: 32,
    fontFamily: 'CeraPro-Bold',
    color: '#ffffff',
    fontWeight: '700',
    position: 'relative',
    zIndex: 1,
    textAlign: 'center',
  },
  // M O D A L - - - E N D
  // END

  rowButton: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  ReceiptButton: {
    width: '47%',
    padding: 15,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#77599A',
    borderRadius: 8,
  },
  ReceiptButtonText: {
    fontSize: 17,
    fontFamily: 'CeraPro-Bold',
    color: '#ffffff',
  },

  top: {
    zIndex: 3,
  },
  closeButton: {
    height: 25,
    width: 25,
    backgroundColor: 'rgba(211,62,62,1)',
    borderRadius: 25,
    position: 'absolute',
    right: 7,
    top: 7,
    alignItems: 'center',
    justifyContent: 'center',
  },

  listTax: {
    marginTop: 5,
  },
  alignLeft: {
    textAlign: 'left',
    fontWeight: '700',
    fontSize: 18,
    color: '#2A3348',
  },
  alignRight: {
    textAlign: 'right',
    fontWeight: '700',
    fontSize: 18,
    color: '#2A3348',
  },
  total: {
    fontSize: 20,
    color: '#36A49F',
  },

  pageWrapper: {
    marginTop: -65,
    paddingTop: 0,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 50,
    paddingHorizontal: 0,
    marginBottom: 50,
    // height: '100%'
  },
  center: {
    paddingHorizontal: 30,
    marginHorizontal: 0,
  },
  header: {
    height: 75,
    alignItems: 'center',
    marginTop: 125,
    paddingBottom: 0,
  },
  headerTitle: {
    fontSize: 27,
    fontFamily: 'CeraPro-Bold',
    color: black,
    fontWeight: '700',
  },
  headerDate: {
    fontSize: 15,
    fontFamily: 'CeraPro-Regular',
    color: grey,
    letterSpacing: 1.2,
  },
  headerSubtitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  subtitle: {
    fontSize: 16,
    fontFamily: 'CeraPro-Regular',
    color: '#ffffff',
    marginLeft: 8,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: 0, height: 1},
    textShadowRadius: 1,
    letterSpacing: .8,
  },
  searchButtonWp: {
    justifyContent: 'flex-end',
  },
  searchButton: {
    flexDirection: 'row',
    height: 65,
    alignItems: 'center',
    backgroundColor: primary,
    paddingHorizontal: 15,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  text: {
    color: '#ffffff',
    fontSize: 16,
    marginLeft: 8,
  },
  // - - - -
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 12,
    backgroundColor: lightGrey,
    paddingHorizontal: 15,
    marginVertical: 15,
  },
  searchInput: {
    flex: 1,
    marginLeft: 15,
    paddingVertical: Platform.OS === 'ios' ? 15 : 15, 
  },
  // - - - -
  optionButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  // - - - -
  historyWrapper: {
    flexDirection: 'column',
    marginVertical: 25,
    position: 'relative',
    zIndex: 9,
  },
  dateDivider: {
    marginVertical: 15,
  },
  textDivider:{
    fontSize: 16,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
    textTransform: 'uppercase',
    letterSpacing: 2,
    marginBottom: 15,
  },
  ReceiptIcon: {
    height: 50,
    width: 50,
    // borderRadius: 50,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
  },
  ReceiptItem: {
    width: '100%',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 8,
    backgroundColor: lightGrey,
    flexDirection: 'row',
    marginBottom: 15,
    alignItems: 'center',
  },
  ReceiptTitle: {
    fontSize: 16,
    color: black,
    fontFamily: 'CeraPro-Bold',
    marginBottom: 2,
    width: wp('30%'),
  },
  ReceiptSubtitle: {
    textTransform: 'capitalize',
    fontSize: 13,
    color: black,
    width: wp('30%'),
  },
  ReceiptPrice: {
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    flex: 6,
    paddingHorizontal: 0,
    marginHorizontal: 0,
  },
  ReceiptPriceText: {
    textAlign: 'right',
    fontSize: 20,
    color: '#4977BB',
    fontFamily: 'CeraPro-Bold',
    fontWeight: '700',
  }

});