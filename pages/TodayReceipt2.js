import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , Modal
  , TouchableOpacity 
  , TextInput
  , SafeAreaView } from 'react-native';

  import { createAppContainer } from 'react-navigation';
  import { createStackNavigator } from 'react-navigation-stack';
  import { createBottomTabNavigator } from 'react-navigation-tabs';
  import NumberFormat from 'react-number-format';


import { 
  styles
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

import PromoBoxNearby from '../components/PromoBoxNearby';
import BoxAccent from '../components/BoxAccent';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";


let ScreenHeight = Dimensions.get("window").height;


class NearbyMap extends React.Component {
  static navigationOptions = {
    header: null
  }
  
  state = {
    search: '',
    historyList: [],
    loading: true,
  };

  updateSearch = search => {
    this.setState({ search });
  };



  
  // Load Data to retrieve in page
  async componentDidMount(){
    try {
      let response = await fetch(
        'http://103.3.62.188/osmosis/openData_',
      );
      let responseJson = await response.json();
      
      console.log(responseJson)
      // GROUPING DATA BY DATE
      const groups = responseJson.reduce((groups, data) => {

        var transaction_date = data.transaction_date.split(" ")
        const date = transaction_date[0];
        if (!groups[date]) {
          groups[date] = [];
        }
        groups[date].push(data);
        return groups;
        console.log(date);
      }, {});
      
      // Edit: to add it in the array format instead
      let groupArrays = Object.keys(groups).map((date) => {
        // array return
        return {
          date,
          history: groups[date]
        };
      });
      
      // groupArrays     = groupArrays[0];
      // groupArrays[0]  = groupArrays;

      this.setState({historyList: groupArrays, loading: false});

    }catch (error) {
      console.error(error);
    }
    console.log("component mount");
  }

  //for rendering each item, and pass data as a argument.
  renderHistoryList() {
    let data = this.state.historyList;
    // data = data[0];
    console.log(data)
    return (
            <View style={[styles.mcRow, page.center, page.historyWrapper]}>
              
              { data.map((item, key)=>(
                  <View>
                    <View style={page.dateDivider}>
                        <Text  style={page.textDivider}>{item.date}</Text>
                    </View>

                    { item.history.map((item, key)=>(
                      <TouchableOpacity key={key} onPress={() => this.props.navigation.navigate('PayReceipt2', { receipt_id:item.id, source: 'today' })
                    }> 
                          <View style={page.historyItem}>
                            <View style={page.historyName}>
                              <Text style={page.historyTitle}>{item.store_name}</Text>
                              <Text style={page.historySubtitle}>{item.transaction_date}</Text>
                            </View>
                            <View style={page.historyPrice}>
                              <Text style={page.historyPriceText}>
                                <NumberFormat value={item.transaction_total} displayType={'text'} thousandSeparator={true} prefix={'Rp. '} renderText={value => <Text>{value}</Text>}/>
                              </Text>
                              {/* <NumberFormat style={page.historyPriceText} value={item.total} displayType={'text'} thousandSeparator={true} prefix={'$'} /> */}
                            </View>
                          </View>
                      </TouchableOpacity>
                    ))}
                  </View>
                ))}
            </View>
          );
  }

  render() {
    const { search } = this.state;

    const mapMarker = {
      position: 'absolute',
      height: 55,
      width: 55,
    };

    //The skeleton loader wrapped in <SkeletonPlaceholder> plase dont use style as a variable!
      //else
    //The page view that should be shown once loaded after the skeleton loader!
    if(this.state.loading == true){
      return (
        <SkeletonPlaceholder>
            <View style={{ width: "100%", height: 140 }} />
            <View
              style={{
                width: 100,
                height: 100,
                borderRadius: 100,
                borderWidth: 5,
                borderColor: "white",
                alignSelf: "center",
                position: "relative",
                top: -50
              }}
            />
            <View style={{ width: 120, height: 20, alignSelf: "center" }} />
            <View
              style={{
                width: 240,
                height: 20,
                alignSelf: "center",
                marginTop: 12
              }}
            />
          </SkeletonPlaceholder>
      );
    }else{
      return (
        <SafeAreaView style={{ position: 'relative', zIndex: 99 }}>
          
          <View style={[styles.mcGrid, page.top]}>
            <View style={[styles.mcRow, styles.topNavigation]}>
              <TouchableOpacity 
                style={[styles.mcCol1, styles.backButton]}
                onPress={() => this.props.navigation.navigate('Home')}>
                <Image
                  style={{width: 35, height: 35}}
                  source={require('../assets/icon-chevron-left.png')}
                />
              </TouchableOpacity>
              <View style={[styles.mcCol4, styles.topNavTitle]}>
                <Text style={styles.pageTitle}></Text>
              </View>
              <View style={[styles.mcCol3, styles.leftIcons]}>
                
              </View>
            </View>

          </View>
          <ScrollView style={[styles.mcGrid, page.pageWrapper]}>
            <BoxAccent
              yAxis={-25}
              xAxis={325}
              width={200}
              height={200}
            ></BoxAccent>
            <BoxAccent
              yAxis={-200}
              xAxis={-100}
              width={300}
              height={300}
            ></BoxAccent>

            <View style={[styles.mcRow, page.center, page.header]}>
              <View style={styles.mcCol6}>
                <Text style={page.headerTitle}>Today Receipt</Text>
              </View>
            </View>

            {/* LOAD HISTORY LIST FROM A FUCTION */}
            {this.renderHistoryList()}
              

            <BoxAccent
              bottom={50}
              xAxis={335}
              width={200}
              height={200}
            ></BoxAccent>
            <BoxAccent
              bottom={-225}
              xAxis={25}
              width={350}
              height={250}
            ></BoxAccent>

          </ScrollView>
          {/* </SkeletonPlaceholder> */}
        </SafeAreaView>
      );
    }
    
    
  }
};

export default NearbyMap;

const page = StyleSheet.create({

  top: {
    zIndex: 3,
  },

  pageWrapper: {
    marginTop: -65,
    paddingTop: 0,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 250,
    paddingHorizontal: 0,
    marginBottom: 50,
  },
  center: {
    paddingHorizontal: 30,
    marginHorizontal: 0,
  },
  header: {
    height: 75,
    alignItems: 'center',
    marginTop: 85,
    paddingBottom: 0,
  },
  headerTitle: {
    fontSize: 27,
    fontFamily: 'CeraPro-Bold',
    color: black,
  },
  headerSubtitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  subtitle: {
    fontSize: 16,
    fontFamily: 'CeraPro-Regular',
    color: '#ffffff',
    marginLeft: 8,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: 0, height: 1},
    textShadowRadius: 1,
    letterSpacing: .8,
  },
  searchButtonWp: {
    justifyContent: 'flex-end',
  },
  searchButton: {
    flexDirection: 'row',
    height: 65,
    alignItems: 'center',
    backgroundColor: primary,
    paddingHorizontal: 15,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  text: {
    color: '#ffffff',
    fontSize: 16,
    marginLeft: 8,
  },
  // - - - -
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 12,
    backgroundColor: lightGrey,
    paddingHorizontal: 15,
    marginVertical: 15,
  },
  searchInput: {
    flex: 1,
    marginLeft: 15,
    paddingVertical: Platform.OS === 'ios' ? 15 : 15, 
  },
  // - - - -
  optionButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  // - - - -
  historyWrapper: {
    flexDirection: 'column',
    marginVertical: 25,
    position: 'relative',
    zIndex: 9,
  },
  dateDivider: {
    marginVertical: 15,
  },
  textDivider:{
    fontSize: 16,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
    textTransform: 'uppercase',
    letterSpacing: 2,
    marginBottom: 15,
  },
  historyItem: {
    width: '100%',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 8,
    backgroundColor: lightGrey,
    flexDirection: 'row',
    marginBottom: 15,
  },
  historyTitle: {
    fontSize: 16,
    color: black,
    fontFamily: 'CeraPro-Bold',
    marginBottom: 2,
  },
  historySubtitle: {
    textTransform: 'capitalize',
    fontSize: 13,
    color: darkGrey,
  },
  historyPrice: {
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    flex: 6,
    paddingHorizontal: 0,
    marginHorizontal: 0,
  },
  historyPriceText: {
    textAlign: 'right',
    fontSize: 16,
    color: primary2,
    fontFamily: 'CeraPro-Bold',
  }

});