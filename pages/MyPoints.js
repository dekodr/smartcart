import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , Modal
  , SafeAreaView
  , TouchableOpacity } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
// import LinearGradient from 'react-native-linear-gradient';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { normalize } from '../assets/Normalize';

import Button from '../components/Button';
import UserCard from '../components/UserCard';
// import TextDivider from '../components/TextDivider';
import PromoBox2 from '../components/PromoBox2';
import PromoBox1 from '../components/PromoBox1';
// import PromoBox3 from '../components/PromoBox3';
import BoxAccent from '../components/BoxAccent';
import BoxAccent2 from '../components/BoxAccent2';
import UserBalance from '../components/UserBalance';
import Carousel from 'react-native-snap-carousel';

import { 
  black
  , styles
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

let ScreenHeight = Dimensions.get("window").height;

class Home extends React.Component {
  static navigationOptions = {
    header: null
  }

  constructor(props){
    super(props);

    // data buat slider discover promo
    this.state = {
      entries: [
        { 'id': '1', 
          'name': 'Buy 1 get 1',
          'mL': 35,
          'image': require('../assets/promo-2-1.png')
        },
        { 'id': '2', 
          'name': 'Buy 1 get 1',
          'mL': 25,
          'image': require('../assets/promo-2-2.png')
        },
        { 'id': '3', 
          'name': 'Buy 1 get 1',
          'mL': 25,
          'image': require('../assets/promo-3-1.png')
        }
      ],
    }
    // data buat slider discover promo - - - END

    // data buat slider category
    this.state2 = {
      entries: [
        { 'id': '1', 
          'name': 'Entainment',
          'lineAccentColor': '#6E95E5',
          'image': require('../assets/icon-food-and-beverages.png')
        },
        { 'id': '2', 
          'name': 'Food & Beverages',
          'lineAccentColor': '#6ECDE5',
          'image': require('../assets/icon-entertainment.png')
        },
        { 'id': '3', 
          'name': 'Shopping',
          'lineAccentColor': '#AEE56E',
          'image': require('../assets/icon-shopping.png')
        }
      ],

    }
    // data buat slider category - - - END

    // data buat slider redeemable slider
    this.state3 = {
      entries: [
        { 'id': '1', 
          'name': 'Sunlight Extra Nature Refill 800Ml',
          'point': '2200',
          'image': require('../assets/product-sunlight.png')
        },
        { 'id': '2', 
          'name': 'ROSE BRAND Minyak Goreng 1L',
          'point': '5000',
          'image': require('../assets/produk-rosebrand.png')
        },
        { 'id': '3', 
          'name': 'Margarine Forvita 200gr',
          'point': '2000',
          'image': require('../assets/produk-forvita.png')
        }
      ],
    }
    // data buat slider redeemable slider - - - END
  }

  _onPressCarousel = () => {
    // here handle carousel press
  }

  // carousel discover promo
  _renderItem ({item, index}) {
    return (
      <TouchableOpacity style={[page.PromoBox3, page.slideFirst]}>
        <View style={page.PromoBoxImgWp}>
          <Image
            style={page.boxImg}
            source={item.image}
          />
        </View>
        <View style={page.boxTitleWp}>
          <Text style={[page.boxTitle, page.TextWhite, page.TextBold, page.TextFzMedium]}>{item.name}</Text>
        </View>
        <View style={page.boxTooltipWp}>
          <Text style={[page.boxTolltip, page.TextWhite, page.TextBold, page.TextLight]}>HOT DEALS !</Text>
        </View>
      </TouchableOpacity>
    );
  }
  // carousel discover promo - - - END

  // carousel category
  _renderItem2 ({item, index}) {
    return (
      <TouchableOpacity style={[page.categorySlider, page.slideFirst, {}]}>
        <View style={[page.lineAccent, {backgroundColor: item.lineAccentColor}]}></View>
        <View style={page.categoryInner}>
          <Image
            style={page.categoryItemIcon}
            source={item.image}
          />
          <Text style={[page.cetegoryItemText, page.TextBlack, page.TextBold]}>{item.name}</Text>
        </View>
      </TouchableOpacity>
    );
  }
  // carousel category - - - END

  // carousel redeemable slider
  _renderItem3 ({item, index}) {
    return (
      <TouchableOpacity>
        <View style={[page.pointCard, page.slideFirst]}>
          <View style={page.pointCardImg}>
            <Image
              style={page.cardImg}
              source={item.image}
            />
          </View>
          <Text numberOfLines={3} style={[page.pointCardTitle, page.TextSmall, page.TextBlack, page.TextFwMedium]}>{item.name}</Text>
          <Text style={[page.pointCardNum, page.TextFzMedium, page.TextBold]}>{item.point}</Text>
        </View>
      </TouchableOpacity>
    );
  }
  // carousel redeemable slider - - - END

  render() {
    return (
      <View style={page.container}>
        
        <ScrollView style={page.mcGrid}>
          <ImageBackground style={page.gradientBg} source={require('../assets/linear-bg.png')}>
            <BoxAccent2
              yAxis={110}
              xAxis={-175}
              width={300}
              height={300}
            ></BoxAccent2>
            <BoxAccent2
              yAxis={-125}
              xAxis={25}
              width={200}
              height={200}
            ></BoxAccent2>
            <View style={[page.radiusUpper, {display: 'none'}]}>
            </View>
          </ImageBackground>

          <SafeAreaView>
            {/* Top Navigation */}
              <View style={[styles.mcGrid, page.top]}>

                <View style={[styles.mcRow, styles.topNavigation]}>
                  <TouchableOpacity 
                    style={[styles.mcCol1, styles.backButton]}
                    onPress={() => this.props.navigation.goBack()}>
                    <Image
                      style={{width: 35, height: 35}}
                      source={require('../assets/icon-chevron-left-white.png')}
                    />
                  </TouchableOpacity>
                  <View style={[styles.mcCol4, styles.topNavTitle]}>
                    <Text style={styles.pageTitle}></Text>
                  </View>
                  <View style={[styles.mcCol3, styles.leftIcons]}>
                    
                  </View>
                </View>

              </View>
            {/* Top Navigation - - -  END */}
          </SafeAreaView>

          <View style={page.pageWrapper}>

            {/* tulisan paling atas */}
              <View style={page.pointHdr}> 
                <View style={page.pointHdrText}>
                  <Text style={[page.hdrText, page.TextBig, page.TextWhite, page.TextBold]}>My Points</Text>
                  <Text style={[page.hdrTextStatus, page.TextSmall, page.TextWhite, page.TextLight]}>SILVER MEMBER</Text>
                </View>
                <View style={page.pointHdrNum}>
                  <Text style={[page.hdrText, page.TextBig, page.TextWhite, page.TextBold]}>2400</Text>
                </View>
              </View> 
            {/* tulisan paling atas - - - END */}
            {/* coba coba slide category */}
              <View style={[styles.sliderDefault, {}]}>
                <View styles={[page.carouselWp, {}]}>
                  <Carousel
                    layout={'default'}
                    layoutCardOffset={18}
                    ref={(c) => { this._carousel = c; }}
                    data={this.state3.entries}
                    renderItem={this._renderItem3}
                    sliderWidth={wp('100%')}
                    itemWidth={185}
                    // loop={true}
                    // loopClonesPerSide={6}
                    activeSlideAlignment={'start'}
                    inactiveSlideShift={2}
                    inactiveSlideOpacity={1}
                    callbackOffsetMargin={5}
                    inactiveSlideScale={1}
                  />
                </View>
              </View> 
            {/* coba coba slide category - - - END */}
            {/* text divider */}
              <View style={[page.TextDivider]}>
                <View style={page.TextLeft}>
                  <Text style={[page.TextFzMedium, page.TextBlack]}>Today</Text>
                  <Text style={[page.TextFzMedium, page.TextBlack, page.TextBold]}> Specials!</Text>
                </View>
                <View style={page.TextRight}>
                  <Text style={[page.seeAll, page.TextBold]}>See all</Text>
                </View>
              </View>
            {/* text divider - - - END */}
            {/* card row isinya 2 card per baris */}
            {/* card row 1 */}
            <View style={page.pointCardRow}>
              <View style={[page.pointCardContent]}>
                <View style={page.pointCardImg}>
                  <Image
                    style={page.cardImg}
                    source={require('../assets/product-sipulen.png')}
                  />
                </View>
                <Text numberOfLines={3} style={[page.pointCardTitle, page.TextSmall, page.TextBlack, page.TextFwMedium]}>SI PULEN Beras Long Grain Crystal 5kg</Text>
                <Text style={[page.pointCardNum, page.TextFzMedium, page.TextBold]}>16000</Text>
              </View>
              <View style={[page.pointCardContent]}>
                <View style={page.pointCardImg}>
                  <Image
                    style={page.cardImg}
                    source={require('../assets/product-indomie.png')}
                  />
                </View>
                <Text numberOfLines={3} style={[page.pointCardTitle, page.TextSmall, page.TextBlack, page.TextFwMedium]}>INDOMIE - Mie Instan Goreng 80gr</Text>
                <Text style={[page.pointCardNum, page.TextFzMedium, page.TextBold]}>1200</Text>
              </View>
            </View>
            {/* card row 2 */}
            <View style={page.pointCardRow}>
              <View style={[page.pointCardContent]}>
                <View style={page.pointCardImg}>
                  <Image
                    style={page.cardImg}
                    source={require('../assets/product-kecap.png')}
                  />
                </View>
                <Text numberOfLines={3} style={[page.pointCardTitle, page.TextSmall, page.TextBlack, page.TextFwMedium]}>INDOFOOD Kecap Manis Refill New 520ml</Text>
                <Text style={[page.pointCardNum, page.TextFzMedium, page.TextBold]}>3000</Text>
              </View>
              <View style={[page.pointCardContent]}>
                <View style={page.pointCardImg}>
                  <Image
                    style={page.cardImg}
                    source={require('../assets/product-sasa.png')}
                  />
                </View>
                <Text numberOfLines={3} style={[page.pointCardTitle, page.TextSmall, page.TextBlack, page.TextFwMedium]}>SASA Tepung Bumbu Ayam Goreng Renyah Spesial 900 gr</Text>
                <Text style={[page.pointCardNum, page.TextFzMedium, page.TextBold]}>800</Text>
              </View>
            </View>
            {/* card row isinya 2 card per baris - - - END */}
            {/* text divider versi 2 */}
              <View style={[page.TextDivider2, {marginTop: 65, marginBottom: 15,}]}>
                <View style={page.TextLeft2}>
                  <Text style={[page.TextSmall, page.TextBlack, {fontSize: 15}]}>Browse by</Text>
                  <Text style={[page.TextFzMedium, page.TextBlack, page.TextBold]}>Category</Text>
                </View>
                <View style={page.TextRight2}>
                  <Image
                    style={{width: 25, height: 25}}
                    source={require('../assets/icon-more.png')}
                  />
                </View>
              </View>
            {/* text divider versi 2 - - - END */}
            {/* coba coba slide category */}
              <View style={[styles.sliderDefault, {}]}>
                <View styles={[page.carouselWp, {}]}>
                  <Carousel
                    layout={'default'}
                    layoutCardOffset={18}
                    ref={(c) => { this._carousel = c; }}
                    data={this.state2.entries}
                    renderItem={this._renderItem2}
                    sliderWidth={wp('100%')}
                    itemWidth={210}
                    // loop={true}
                    // loopClonesPerSide={6}
                    activeSlideAlignment={'start'}
                    inactiveSlideShift={2}
                    inactiveSlideOpacity={1}
                    inactiveSlideScale={1}
                    callbackOffsetMargin={5}
                  />
                </View>
              </View> 
            {/* coba coba slide category - - - END */}
            {/* text divider */}
              <View style={[page.TextDivider]}>
                <View style={page.TextLeft}>
                  <Text style={[page.TextFzMedium, page.TextBlack]}>Discover</Text>
                  <Text style={[page.TextFzMedium, page.TextBlack, page.TextBold]}> Promo</Text>
                </View>
                <View style={page.TextRight}>
                  <Text style={[page.seeAll, page.TextBold]}>See all</Text>
                </View>
              </View>
            {/* text divider - - - END */}
            {/* coba coba carousel promo box 3 */}
              <View style={[styles.sliderDefault, {marginTop: 25, marginBottom: 200}]}>
                <View styles={[page.carouselWp]}>
                  <Carousel
                    layout={'default'}
                    layoutCardOffset={18}
                    ref={(c) => { this._carousel = c; }}
                    data={this.state.entries}
                    renderItem={this._renderItem}
                    sliderWidth={wp('100%')}
                    itemWidth={340}
                    // loop={true}
                    // loopClonesPerSide={6}
                    activeSlideAlignment={'start'}
                    inactiveSlideShift={2}
                    inactiveSlideOpacity={1}
                    nactiveSlideScale={.9}
                  />
                </View>
              </View> 
            {/* coba coba carousel promo box 3 - - - END */}
          </View> 
          {/* page wrapper - - - END */}

          <BoxAccent
            bottom={125}
            xAxis={325}
            width={200}
            height={200}
          ></BoxAccent>
          <BoxAccent
            bottom={-175}
            xAxis={150}
            width={250}
            height={350}
          ></BoxAccent>

        </ScrollView>
      </View>
    );
  }
};

export default Home;

const page = StyleSheet.create({

  // B R A N D  B R A N D  N E W  S T Y L I N G 
  TextBig: {
    fontSize: 32,
  },
  TextSmall: {
    fontSize: 12,
  },
  TextFzMedium: {
    fontSize: 22,
  },
  TextWhite: {
    color: '#fff',
  },
  TextBlack: {
    color: black,
  },
  TextBold: {
    // fontWeight: '700',
    fontFamily: 'CeraPro-Bold',
  },
  TextFwMedium: {
    // fontWeight: '400',
    fontFamily: 'CeraPro-Regular',
  },
  TextLight: {
    // fontWeight: '300',
    fontFamily: 'CeraPro-Light',
  },
  TextCapitalize: {
    textTransform: 'capitalize',
  },

  // H E A D E R
  pointHdr: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'space-between',
    paddingHorizontal: 35,
  },
  pointHdrText: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  hdrText: {
    marginBottom: 5,
  },
  hdrTextStatus: {
    letterSpacing: 2,
  },
  // H E A D E R - - - END
  // H E A D E R  S L I D E R 
  pointCard: {
    width: 150,
    height: 250,
    borderRadius: 16,
    backgroundColor: '#fff',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    padding: 20,
    marginRight: 20,
    marginVertical: 20,
  },
  slideFirst: {
    marginLeft: 35,
  },
  pointCardImg: {
    width: 125,
    height: 125,
    left: -10,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cardImg: {
    width: '100%',
    height: '100%',
  },
  pointCardTitle: {
    marginVertical: 10,
    height: 50,
  },
  pointCardNum: {
    color: '#F4376A',
    position: 'absolute',
    left: 20,
    bottom: 10,
  },
  // H E A D E R  S L I D E R - - - END
  // T E X T  D I V I D E R 
  TextDivider: {
    marginTop: 45,
    height: 30,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    width: '100%',
    paddingHorizontal: 35,
  },
  TextLeft: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  TextRight: {
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  seeAll: {
    color: '#6E72D6',
    fontSize: 15,
  },
  // T E X T  D I V I D E R - - - END
  // T E X T  D I V I D E R 2
  TextDivider2: {
    marginTop: 45,
    height: 30,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    paddingHorizontal: 35,
  },
  TextLeft2: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  TextRight2: {
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  // T E X T  D I V I D E R - - - END 
  // C A R D  R O W
  pointCardRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    paddingHorizontal: 35,
  },
  pointCardContent: {
    width: '47%',
    height: 250,
    borderRadius: 16,
    backgroundColor: '#fff',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    padding: 20,
    // marginRight: 20,
    marginTop: 20,
  },
  // C A R D  R O W - - - END
  // S L I D E R  B O X 
  PromoBox3: {
    width: 300,
    height: 185,
    borderRadius: 16,
    overflow: 'hidden',
    position: 'relative',
    paddingTop: 10,
    marginRight: 20,
  },
  PromoBoxImgWp: {
    height: 185,
    width: 300,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 16,
    overflow: 'hidden',
  },
  boxImg: {
    height: '100%',
    width: '100%',
  },
  boxTitleWp: {
    position: 'absolute',
    width: '100%',
    backgroundColor: 'rgba(0,0,0,.4)',
    borderRadius: 16,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingHorizontal: 15,
    paddingVertical: 10,
    bottom: 0,
  },
  boxTitle: {

  },
  boxTooltipWp: {
    position: 'absolute',
    right: 20,
    top: 0,
    paddingVertical: 5,
    paddingHorizontal: 15,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F4376A',
  },
  boxTolltip: {
    letterSpacing: 2,
    fontSize: 10,
  },
  // S L I D E R  B O X - - - END
  // S L I D E R  C A T E G O R Y
  categorySlider: {
    width: 180,
    // borderRadius: 16,
    paddingTop: 2.5,
    position: 'relative',
    marginRight: 20,
    marginTop: 25,
  },
  lineAccent: {
    width: 35,
    height: 5,
    borderRadius: 35,
    backgroundColor: '#6ECDE5',
    position: 'absolute',
    left: 15,
    top: 0,
    zIndex: 9,
  },
  categoryInner: {
    width: 180,
    height: 60,
    overflow: 'hidden',
    borderRadius: 16,
    backgroundColor: '#fff',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    position: 'relative',
    zIndex: 2,
  },
  categoryItemIcon: {
    height: 35,
    width: 35,
  },
  cetegoryItemText: {
    fontSize: 12,
    marginLeft: 20,
    width: 100,
  },
  // S L I D E R  C A T E G O R Y - - - END
  // END

  mini: {
    fontSize: normalize(12),
  },
  small: {
    fontSize: normalize(15),
  },
  medium: {
    fontSize: normalize(17),
  },
  large: {
    fontSize: normalize(20),
  },
  xlarge: {
    fontSize: normalize(25),
  },

  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  mcGrid: {
    margin: 0,
    padding: 0,
    zIndex: 0,
    position: "relative",
  },
  mcRow: {
    marginVertical: 0,
    marginHorizontal: 18,
    flexDirection: 'row',
  },
  fullWidth: {
    marginHorizontal: 0,
  },
  firstRow: {
    marginTop: 95,
  },
  mt15: {
    marginVertical: 15,
  },
  mt25: {
    marginVertical: 25,
  },
  mcCol1: {
    flex: 1,
  },
  mcCol2: {
    flex: 2,
  },
  mcCol3: {
    flex: 3,
  },
  mcCol4: {
    flex: 4,
  },
  mcCol5: {
    flex: 5,
  },
  mcCol6: {
    flex: 6,
  },
  block: {
    backgroundColor: '#dddddd',
    borderRadius: 8,
    height: 50,
    margin: 5,
  },

  gradientBg: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: 460,
  },

  radiusUpper: {
    backgroundColor: '#F5FCFF',
    position: 'absolute',
    left: 0,
    top: hp('52%'),
    // bottom: 35,
    width: '100%',
    height: 35,
    borderTopRightRadius: 16,
    borderTopLeftRadius: 16,
  },

  sliderBox: {
    position: 'relative',
    zIndex: 9,
    elevation: 5,
  },

  buttonIcon: {
    height: 85,
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F6F6F6',
  },
  buttonText: {
    marginTop: 8,
    color: '#2A3348',
    fontFamily: 'CeraPro-Regular',
  },

  // userBalance - - - H E R E 

  // topNavigation - - - H E R E
  topNavigation: {
    height: 50,
    marginVertical: 6,
    position: 'relative',
    zIndex: 9,
    marginHorizontal: 25,
  },
  backButton: {
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  leftIcons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  topNavTitle: {
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  pageTitle: {
    fontSize: 22,
    color: '#2A3348',
    fontWeight: '700',
    paddingBottom: 8,
  },

  mapWrapper: {
    height: ScreenHeight,
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1
  },

  // pageWrapper - - - H E R E
  pageWrapper: {
    marginTop: 0,
    paddingTop: 25,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 25,
    // paddingHorizontal: 35,
  },
  pageHeader: {
    backgroundColor: 'rgba(255,255,255,.6)',
    padding: 15,
    position: 'relative',
    height: 85,
    marginTop: 15,
    borderTopLeftRadius: 14,
    borderTopRightRadius: 14,
  },
  headerTitle: {
    fontSize: 29,
    color: '#2A3348',
    fontWeight: '700',
    flex: 1,
  },
  headerSubtitle: {
    fontSize: 13,
    color: '#2A3348',
  },
  headerImage: {
    position: 'absolute',
    right: 35,
    top: -15,
    width: 65,
    height: 65,
    backgroundColor: 'rgba(255,255,255,.8)',
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pageContent: {
    padding: 15,
    backgroundColor: '#ffffff',
    borderBottomLeftRadius: 14,
    borderBottomRightRadius: 14,
    marginBottom: 100,
  },
  // mcFooter: {
  //   width: '100%',
  //   height: 75,
  // },
  mb75: {
    marginBottom: 75,
  },
  top: {
    top: 35,
  },

});