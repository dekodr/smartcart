import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , TouchableOpacity 
  , TextInput
  , SafeAreaView } from 'react-native';

  import { createAppContainer } from 'react-navigation';
  import { createStackNavigator } from 'react-navigation-stack';
  import { createBottomTabNavigator } from 'react-navigation-tabs';
import Modal from "react-native-modal";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import { 
  styles
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

import PromoBoxNearby from '../components/PromoBoxNearby';
import BoxAccent from '../components/BoxAccent';
import Button from '../components/Button';

let ScreenHeight = Dimensions.get("window").height;

class TodayReceiptDetail extends React.Component {
  static navigationOptions = {
    header: null
  }

  state = {
    isModalVisible: false
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };
  
  state = {
    search: '',
  };

  updateSearch = search => {
    this.setState({ search });
  };

  render() {
    const { search } = this.state;

    const mapMarker = {
      position: 'absolute',
      height: 55,
      width: 55,
    };

    return (
        <SafeAreaView style={{ position: 'relative', zIndex: 9 }}>
          <View style={[styles.mcGrid, page.top]}>

            <View style={[styles.mcRow, styles.topNavigation]}>
              <TouchableOpacity 
                style={[styles.mcCol1, styles.backButton]}
                onPress={() => this.props.navigation.goBack()}>
                <Image
                  style={{width: 35, height: 35}}
                  source={require('../assets/icon-chevron-left.png')}
                />
              </TouchableOpacity>
              <View style={[styles.mcCol4, styles.topNavTitle]}>
                <Text style={styles.pageTitle}></Text>
              </View>
              <View style={[styles.mcCol3, styles.leftIcons]}>
                
              </View>
            </View>

          </View>
          <ScrollView style={[styles.mcGrid, page.pageWrapper, {}]}>
            <BoxAccent
              yAxis={-25}
              xAxis={325}
              width={200}
              height={200}
            ></BoxAccent>
            <BoxAccent
              yAxis={-200}
              xAxis={-100}
              width={300}
              height={300}
            ></BoxAccent>

            <View style={[styles.mcRow, page.center, page.header]}>
              <View style={styles.mcCol6}>
                <Text style={page.headerTitle}>Share to get point</Text>
                <Text style={page.headerDate}></Text>
              </View>
            </View>

            {/* Search Bar HIDDEN */}
            <View style={[styles.mcRow, page.center, {display: 'none'}]}>
              <View style={[styles.mcCol6, {}]}>
                <View style={page.searchBar}>
                  <Image
                    style={{width: 18, height: 18}}
                    source={require('../assets/icon-search-dollar.png')}
                  />
                  <TextInput 
                    style={page.searchInput}
                    placeholder={"Search Transaction"}
                    placeholderTextColor={'#798191'}
                    onChangeText={this.updateSearch}
                    value={search}
                  />
                </View>
              </View>
              <View style={[styles.mcCol1, page.optionButton]}>
                <Image
                  style={{width: 20, height: 20}}
                  source={require('../assets/icon-options.png')}
                />
              </View>
            </View>
            {/* Search Bar - - -  END*/}

            <View style={[styles.mcRow, page.center, page.historyWrapper, {height: ScreenHeight}]}>
              <TouchableOpacity 
                style={page.ReceiptItem}
                onPress={this.toggleModal}>
                <View style={page.ReceiptIcon}>
                  <Image
                    style={{width: 30, height: 30}}
                    source={require('../assets/icon-instagram-colored.png')}
                  />
                </View>
                <View style={page.historyName}>
                  <Text style={page.ReceiptSubtitle}>Share to Instagram and get points</Text>
                </View>
                <View style={page.ReceiptPrice}>
                  <Text style={page.ReceiptPriceText}>+200</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity 
                style={page.ReceiptItem}
                onPress={this.toggleModal}>
                <View style={page.ReceiptIcon}>
                  <Image
                    style={{width: 30, height: 30}}
                    source={require('../assets/icon-facebook-colored.png')}
                  />
                </View>
                <View style={page.historyName}>
                  <Text style={page.ReceiptSubtitle}>Share to Faceebook and get points</Text>
                </View>
                <View style={page.ReceiptPrice}>
                  <Text style={page.ReceiptPriceText}>+200</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity 
                style={page.ReceiptItem}
                onPress={this.toggleModal}>
                <View style={page.ReceiptIcon}>
                  <Image
                    style={{width: 30, height: 25}}
                    source={require('../assets/icon-twitter-colored.png')}
                  />
                </View>
                <View style={page.historyName}>
                  <Text style={page.ReceiptSubtitle}>Share to twitter and get points</Text>
                </View>
                <View style={page.ReceiptPrice}>
                  <Text style={page.ReceiptPriceText}>+200</Text>
                </View>
              </TouchableOpacity>

            </View>

            <BoxAccent
              bottom={0}
              xAxis={335}
              width={200}
              height={200}
            ></BoxAccent>
            <BoxAccent
              bottom={-285}
              xAxis={25}
              width={350}
              height={250}
            ></BoxAccent>

          </ScrollView>

          {/* Modal Area */}  
          <Modal isVisible={this.state.isModalVisible}>
            <View style={{ backgroundColor: '#fff', height: 125, width: '100%', borderRadius: 16, paddingTop: 45, paddingHorizontal: 25, paddingBottom: 25}}>
              <TouchableOpacity 
                style={page.closeButton}
                onPress={this.toggleModal}>
                  <Image
                    style={{width: 10, height: 10}}
                    source={require('../assets/icon-times-white.png')}
                  />
              </TouchableOpacity>
              <View style={page.rowButton}>
                <TouchableOpacity style={page.ReceiptButton}>
                  <Text style={page.ReceiptButtonText}>Find Info</Text>
                </TouchableOpacity>
                <TouchableOpacity style={[page.ReceiptButton, {backgroundColor: '#57789F'}]}>
                  <Text style={page.ReceiptButtonText}>Find Recipes</Text>
                </TouchableOpacity>
              </View>
            </View>
          </Modal>
          {/* Modal Area - - - END */}
        </SafeAreaView>
    );
  }
};

export default TodayReceiptDetail;

const page = StyleSheet.create({

  rowButton: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  ReceiptButton: {
    width: '47%',
    padding: 15,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#77599A',
    borderRadius: 8,
  },
  ReceiptButtonText: {
    fontSize: 17,
    fontFamily: 'CeraPro-Bold',
    color: '#ffffff',
  },

  top: {
    zIndex: 3,
  },
  closeButton: {
    height: 25,
    width: 25,
    backgroundColor: 'rgba(211,62,62,1)',
    borderRadius: 25,
    position: 'absolute',
    right: 7,
    top: 7,
    alignItems: 'center',
    justifyContent: 'center',
  },

  listTax: {
    marginTop: 5,
  },
  alignLeft: {
    textAlign: 'left',
    fontWeight: '700',
    fontSize: 18,
    color: '#2A3348',
  },
  alignRight: {
    textAlign: 'right',
    fontWeight: '700',
    fontSize: 18,
    color: '#2A3348',
  },
  total: {
    fontSize: 20,
    color: '#36A49F',
  },

  pageWrapper: {
    marginTop: -65,
    paddingTop: 0,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 250,
    paddingHorizontal: 0,
    marginBottom: 50,
    height: '100%'
  },
  center: {
    paddingHorizontal: 30,
    marginHorizontal: 0,
  },
  header: {
    height: 75,
    alignItems: 'center',
    marginTop: 85,
    paddingBottom: 0,
  },
  headerTitle: {
    fontSize: 27,
    fontFamily: 'CeraPro-Bold',
    color: black,
  },
  headerDate: {
    fontSize: 15,
    fontFamily: 'CeraPro-Regular',
    color: grey,
    letterSpacing: 1.2,
  },
  headerSubtitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  subtitle: {
    fontSize: 16,
    fontFamily: 'CeraPro-Regular',
    color: '#ffffff',
    marginLeft: 8,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: 0, height: 1},
    textShadowRadius: 1,
    letterSpacing: .8,
  },
  searchButtonWp: {
    justifyContent: 'flex-end',
  },
  searchButton: {
    flexDirection: 'row',
    height: 65,
    alignItems: 'center',
    backgroundColor: primary,
    paddingHorizontal: 15,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  text: {
    color: '#ffffff',
    fontSize: 16,
    marginLeft: 8,
  },
  // - - - -
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 12,
    backgroundColor: lightGrey,
    paddingHorizontal: 15,
    marginVertical: 15,
  },
  searchInput: {
    flex: 1,
    marginLeft: 15,
    paddingVertical: Platform.OS === 'ios' ? 15 : 15, 
  },
  // - - - -
  optionButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  // - - - -
  historyWrapper: {
    flexDirection: 'column',
    marginVertical: 25,
    position: 'relative',
    zIndex: 9,
  },
  dateDivider: {
    marginVertical: 15,
  },
  textDivider:{
    fontSize: 16,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
    textTransform: 'uppercase',
    letterSpacing: 2,
    marginBottom: 15,
  },
  ReceiptIcon: {
    height: 50,
    width: 50,
    // borderRadius: 50,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
  },
  ReceiptItem: {
    width: '100%',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 8,
    backgroundColor: lightGrey,
    flexDirection: 'row',
    marginBottom: 15,
    alignItems: 'center',
  },
  ReceiptTitle: {
    fontSize: 16,
    color: black,
    fontFamily: 'CeraPro-Bold',
    marginBottom: 2,
    width: wp('30%'),
  },
  ReceiptSubtitle: {
    textTransform: 'capitalize',
    fontSize: 13,
    color: black,
    width: wp('30%'),
  },
  ReceiptPrice: {
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    flex: 6,
    paddingHorizontal: 0,
    marginHorizontal: 0,
  },
  ReceiptPriceText: {
    textAlign: 'right',
    fontSize: 20,
    color: '#4977BB',
    fontFamily: 'CeraPro-Bold',
    fontWeight: '700',
  }

});