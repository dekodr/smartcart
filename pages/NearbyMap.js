import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , Modal
  , TouchableOpacity 
  , TextInput
  , SafeAreaView } from 'react-native';

  import { createAppContainer } from 'react-navigation';
  import { createStackNavigator } from 'react-navigation-stack';
  import { createBottomTabNavigator } from 'react-navigation-tabs';
  import MapView, { PROVIDER_GOOGLE } from 'react-native-maps'; // --> google maps

import { 
  styles
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

import PromoBoxNearby from '../components/PromoBoxNearby';

let ScreenHeight = Dimensions.get("window").height;

class NearbyMap extends React.Component {
  static navigationOptions = {
    header: null
  }
  
  state = {
    search: '',
  };

  updateSearch = search => {
    this.setState({ search });
  };

  render() {
    const { search } = this.state;

    const mapMarker = {
      position: 'absolute',
      height: 55,
      width: 55,
    };
    var markers = [
      {
        latitude: -6.201510,
        longitude: 106.823491,
        title: 'Smartcart HQ',
        subtitle: 'Jembut'
      }
    ];
    return (
      <View>
        <View style={styles.mapWrapper}>
          <View accessible={true} style={map_.container}>
            <MapView
              style={ map_.map }
              initialRegion={{
                latitude: -6.201510,
                longitude: 106.823491,
                latitudeDelta: 0.00922,
                longitudeDelta: 0.00421,
              }}
              >
              <MapView.Marker
                  coordinate={{latitude: -6.201510,
                  longitude: 106.823491}}
                  title={"Smartcart HQ"}
                  description={"jembut"}
              />
            </MapView>
          </View>
        </View>
        <SafeAreaView style={{ position: 'relative', zIndex: 99 }}>
          <View style={styles.mcGrid}>

            <View style={[styles.mcRow, styles.topNavigation]}>
              <TouchableOpacity 
                style={[styles.mcCol1, styles.backButton]}
                onPress={() => this.props.navigation.goBack()}>
                <Image
                  style={{width: 35, height: 35}}
                  source={require('../assets/icon-chevron-left.png')}
                />
              </TouchableOpacity>
              <View style={[styles.mcCol4, styles.topNavTitle]}>
                <Text style={styles.pageTitle}></Text>
              </View>
              <View style={[styles.mcCol3, styles.leftIcons]}>
                
              </View>
            </View>

            <ScrollView style={[styles.mcGrid, page.pageWrapper]}>

              <View style={[styles.mcRow, page.center, page.header]}>
                <View style={styles.mcCol6}>
                  <Text style={page.headerTitle}>Nearby</Text>
                  <View style={page.headerSubtitle}>
                    <Image
                      style={{width: 12, height: 16}}
                      source={require('../assets/icon-location-mark.png')}
                    />
                    <Text style={page.subtitle}>Sudirman, Jakarta, ID</Text>
                  </View>
                </View>
              </View>

              <View style={[styles.mcRow, page.center, page.search]}>
                <View style={styles.mcCol6}>
                  <View style={page.searchBar}>
                    <Image
                      style={{width: 18, height: 18}}
                      source={require('../assets/icon-search-dollar.png')}
                    />
                    <TextInput 
                      style={page.searchInput}
                      placeholder={"Search here and let's go"}
                      placeholderTextColor={'#798191'}
                      onChangeText={this.updateSearch}
                      value={search}
                    />
                  </View>
                </View>
                <View style={[styles.mcCol1, page.optionButton]}>
                  <Image
                    style={{width: 20, height: 20}}
                    source={require('../assets/icon-options-grey.png')}
                  />
                </View>
              </View>

              <View style={{width: '100%', height: 235, backgroundColor: 'rgba(0,0,0,0)'}}></View>

              <View style={[styles.sliderBox]}>
                <ScrollView 
                  horizontal={true}
                  showsHorizontalScrollIndicator={false}
                >
                  <PromoBoxNearby 
                    title='Unilever Mega Sale WEEK!'
                    subtitle='Grand Indonesia'
                    distance='0.1 KM'
                  />
                  <PromoBoxNearby 
                    title='Unilever Mega Sale WEEK!'
                    subtitle='Thamrin City'
                    distance='0.8 KM'
                  />
                  <PromoBoxNearby 
                    title='Unilever Mega Sale WEEK!'
                    subtitle='Senayan City'
                    distance='2.7 KM'
                    lastSlide
                  />
                </ScrollView>
              </View>

            </ScrollView>
          </View>
        </SafeAreaView>
      </View>
    );
  }
};

export default NearbyMap;

const page = StyleSheet.create({

  pageWrapper: {
    marginTop: -65,
    paddingTop: 65,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 150,
    paddingHorizontal: 0,
  },
  center: {
    paddingHorizontal: 30,
    marginHorizontal: 0,
  },
  header: {
    height: 75,
    alignItems: 'center',
    marginTop: 15,
  },
  headerTitle: {
    fontSize: 37,
    fontFamily: 'CeraPro-Bold',
    color: black,
  },
  headerSubtitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  subtitle: {
    fontSize: 16,
    fontFamily: 'CeraPro-Regular',
    color: '#ffffff',
    marginLeft: 8,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: 0, height: 1},
    textShadowRadius: 1,
    letterSpacing: .8,
  },
  searchButtonWp: {
    justifyContent: 'flex-end',
  },
  searchButton: {
    flexDirection: 'row',
    height: 65,
    alignItems: 'center',
    backgroundColor: primary,
    paddingHorizontal: 15,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  text: {
    color: '#ffffff',
    fontSize: 16,
    marginLeft: 8,
  },
  // - - - -
  search: {
    alignItems: 'center',
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 4 },
    // shadowOpacity: 0.3,
    // shadowRadius: 4,
    // elevation: 10,
    // borderRadius: 8,
  },
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    backgroundColor: '#ffffff',
    paddingHorizontal: 15,
    marginVertical: 24,
  },
  searchInput: {
    flex: 1,
    marginLeft: 15,
    height: 45,
  },
  // - - - -
  optionButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: lightGrey,
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,
    height: 45,
  },
  // - - - -
   mapWrapper: {
    height: ScreenHeight,
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1,
  },
});

const map_ = StyleSheet.create({
  fadeIn:{
    width:250,
    height:50,
    backgroundColor:'#bdc3c7',
  },
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
});