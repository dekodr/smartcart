import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , TouchableOpacity 
  , TextInput
  , SafeAreaView } from 'react-native';

  import { createAppContainer } from 'react-navigation';
  import { createStackNavigator } from 'react-navigation-stack';
  import { createBottomTabNavigator } from 'react-navigation-tabs';
import Modal from "react-native-modal";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import { 
  styles
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

import PromoBoxNearby from '../components/PromoBoxNearby';
import BoxAccent from '../components/BoxAccent';
import Button from '../components/Button';

let ScreenHeight = Dimensions.get("window").height;

class ShoppingHistory extends React.Component {
  static navigationOptions = {
    header: null
  }
  
  state = {
    search: '',
  };

  updateSearch = search => {
    this.setState({ search });
  };

  render() {
    const { search } = this.state;

    const mapMarker = {
      position: 'absolute',
      height: 55,
      width: 55,
    };

    return (
        <SafeAreaView style={{ position: 'relative', zIndex: 99 }}>
          <View style={[styles.mcGrid, page.top]}>

            <View style={[styles.mcRow, styles.topNavigation]}>
              <TouchableOpacity 
                style={[styles.mcCol1, styles.backButton]}
                onPress={() => this.props.navigation.goBack()}>
                <Image
                  style={{width: 35, height: 35}}
                  source={require('../assets/icon-chevron-left.png')}
                />
              </TouchableOpacity>
              <View style={[styles.mcCol4, styles.topNavTitle]}>
                <Text style={styles.pageTitle}></Text>
              </View>
              <View style={[styles.mcCol3, styles.leftIcons]}>
                
              </View>
            </View>

          </View>
          <ScrollView style={[styles.mcGrid, page.pageWrapper]}>
            <BoxAccent
              yAxis={-25}
              xAxis={325}
              width={200}
              height={200}
            ></BoxAccent>
            <BoxAccent
              yAxis={-200}
              xAxis={-100}
              width={300}
              height={300}
            ></BoxAccent>
            {/* Header paling atas */}
              <View style={[styles.mcRow, page.center, page.header]}>
                <View style={styles.mcCol6}>
                  <Text style={page.headerTitle}>Shopping History</Text>
                </View>
              </View>
            {/* Header paling atas - - - END */}
            {/* Search bar */}
              <View style={[styles.mcRow, page.center]}>
                <View style={styles.mcCol6}>
                  <View style={page.searchBar}>
                    <Image
                      style={{width: 18, height: 18}}
                      source={require('../assets/icon-search-dollar.png')}
                    />
                    <TextInput 
                      style={page.searchInput}
                      placeholder={"Search Transaction"}
                      placeholderTextColor={'#798191'}
                      onChangeText={this.updateSearch}
                      value={search}
                    />
                  </View>
                </View>
                <View style={[styles.mcCol1, page.optionButton]}>
                  <Image
                    style={{width: 20, height: 20}}
                    source={require('../assets/icon-options.png')}
                  />
                </View>
              </View>
            {/* Search bar - - - END */}

            {/* ITEM TRANSACTION DIBAWAH INI DIBUAT UNTUK MEMENUHI VERSI HISTORY */}
            <View style={[styles.mcRow, page.center, page.historyWrapper]}>
              {/* INFO : ini date divider */}
                <View style={page.dateDivider}>
                  <Text style={page.textDateDivider}>today</Text>
                </View>
              {/* INFO - - - END */}
              {/* INFO : ini item yang bentuknya kek tiket ya*/}
                <TouchableOpacity 
                  style={page.TrItem}
                  onPress={() => this.props.navigation.navigate('TodayReceiptDetail2')}>
                  {/* info : bagian atas, ada nama, location, ama image */}
                  <View style={page.TrUpper}>
                    <View style={page.TrIcon}>
                      <Image
                        style={page.TrIconImg}
                        source={require('../assets/brand-piko-mart.png')}
                      />
                    </View>
                    <View style={page.TrTitle}>
                      <Text style={page.storeName}>Piko Mart</Text>
                      <Text style={page.storeLocation}>Bekasi Store</Text>
                    </View>
                    <View style={page.TrPrice}>
                      <Text style={page.storeTotalPrice}>36.800</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              {/* INFO - - - END */}
              {/* INFO : ini item yang bentuknya kek tiket ya*/}
                <TouchableOpacity 
                  style={page.TrItem}
                  onPress={() => this.props.navigation.navigate('TodayReceiptDetail2')}>
                  {/* info : bagian atas, ada nama, location, ama image */}
                  <View style={page.TrUpper}>
                    <View style={page.TrIcon}>
                      <Image
                        style={page.TrIconImg}
                        source={require('../assets/brand-piko-mart.png')}
                      />
                    </View>
                    <View style={page.TrTitle}>
                      <Text style={page.storeName}>Piko Mart</Text>
                      <Text style={page.storeLocation}>Bekasi Store</Text>
                    </View>
                    <View style={page.TrPrice}>
                      <Text style={page.storeTotalPrice}>36.800</Text>
                    </View>
                  </View>
                </TouchableOpacity>              
              {/* INFO - - - END */}

              {/* EXAMPLE ROW 2 */}
              {/* INFO : ini date divider */}
                <View style={page.dateDivider}>
                  <Text style={page.textDateDivider}>YESTERDAY</Text>
                </View>
              {/* INFO - - - END */}
              {/* INFO : ini item yang bentuknya kek tiket ya*/}
                <TouchableOpacity 
                  style={page.TrItem}
                  onPress={() => this.props.navigation.navigate('TodayReceiptDetail2')}>
                  {/* info : bagian atas, ada nama, location, ama image */}
                  <View style={page.TrUpper}>
                    <View style={page.TrIcon}>
                      <Image
                        style={page.TrIconImg}
                        source={require('../assets/brand-piko-mart.png')}
                      />
                    </View>
                    <View style={page.TrTitle}>
                      <Text style={page.storeName}>Piko Mart</Text>
                      <Text style={page.storeLocation}>Bekasi Store</Text>
                    </View>
                    <View style={page.TrPrice}>
                      <Text style={page.storeTotalPrice}>36.800</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              {/* INFO - - - END */}
              {/* INFO : ini item yang bentuknya kek tiket ya*/}
                <TouchableOpacity 
                  style={page.TrItem}
                  onPress={() => this.props.navigation.navigate('TodayReceiptDetail2')}>
                  {/* info : bagian atas, ada nama, location, ama image */}
                  <View style={page.TrUpper}>
                    <View style={page.TrIcon}>
                      <Image
                        style={page.TrIconImg}
                        source={require('../assets/brand-piko-mart.png')}
                      />
                    </View>
                    <View style={page.TrTitle}>
                      <Text style={page.storeName}>Piko Mart</Text>
                      <Text style={page.storeLocation}>Bekasi Store</Text>
                    </View>
                    <View style={page.TrPrice}>
                      <Text style={page.storeTotalPrice}>36.800</Text>
                    </View>
                  </View>
                </TouchableOpacity>              
              {/* INFO - - - END */}
            </View>

          </ScrollView>

        </SafeAreaView>
    );
  }
};

export default ShoppingHistory;

const page = StyleSheet.create({

  // N E W  C A R D  T I C K E T  S T Y L E
  TrItem: {
    backgroundColor: lightGrey,
    paddingVertical: 20,
    borderRadius: 16,
    position: 'relative',
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: 25,
  },
  // T O D A Y  R E C E I P T  U P P E R
  TrUpper: {
    position: 'relative',
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    height: 45,
    paddingHorizontal: 20,
  },
  TrIcon: {
    position: 'relative',
    height: 45,
    width: 45,
    borderRadius: 50,
    overflow: 'hidden',
    marginRight: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  TrIconImg: {
    position: 'absolute',
    height: 45,
    width: 45,
  },
  TrTitle: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  storeName: {
    fontSize: 18,
    fontFamily: 'CeraPro-Bold',
    color: black,
    fontWeight: '700',
  },
  storeLocation: {
    fontSize: 12,
    fontFamily: 'CeraPro-Regular',
    color: grey,
    fontWeight: '400',
  },
  TrPrice: {
    marginLeft: 'auto',
  },
  storeTotalPrice: {
    fontSize: 20,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
    fontWeight: '700',
  },
  // T O D A Y  R E C E I P T  U P P E R - - - E N D
  // T O D A Y  R E C E I P T  U N D E R
  TrUnder: {
    position: 'relative',
    marginTop: 35,
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flexDirection: 'row',
    height: 45,
    paddingHorizontal: 20,
  },
  TrUnderInfo: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  infoAbove: {
    fontSize: 12,
    fontFamily: 'CeraPro-Bold',
    color: grey,
    fontWeight: '400',
  },
  infoBelow: {
    fontSize: 18,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
    fontWeight: '700',
  },
  // T O D A Y  R E C E I P T  U N D E R - - - E N D
  // T O D A Y  R E C E I P T  M I D D L E  A C C E N T
  TrMiddle: {
    // position: 'absolute',
    // left: 0,
    // top: '50%',
    // transform: [{translateY: 50}],
    width: '100%',
    height: 30,
    overflow: 'hidden',
    top: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  circleWhiteLeft: {
    width: 30,
    height: 30,
    borderRadius: 15,
    position: 'absolute',
    left: -15,
    backgroundColor: '#fff',
    zIndex: 99,
  },
  circleWhiteRight: {
    width: 30,
    height: 30,
    borderRadius: 15,
    position: 'absolute',
    right: -15,
    backgroundColor: '#fff',
    zIndex: 99,
  },
  dashedLine: {
    fontSize: 12,
    fontFamily: 'CeraPro-Bold',
    color: grey,
    fontWeight: '300',
    paddingHorizontal: 20,
    width: '100%',
    overflow: 'hidden',
    width: '120%',
  },
  // T O D A Y  R E C E I P T  M I D D L E  A C C E N T - - - E N D
  // I N I  B U T T O N  D A N  W P - N Y A
  buttonWp: {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 100,
  },
  button: {
    width: '80%',
    padding: 20,
    backgroundColor: primary,
    borderRadius: 16,
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 20,
    fontFamily: 'CeraPro-Bold',
    color: '#fff',
    fontWeight: '700',
  },
  // I N I  B U T T O N  D A N  W P - N Y A - - - E N D

  top: {
    zIndex: 3,
  },

  pageWrapper: {
    marginTop: -65,
    paddingTop: 0,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 250,
    paddingHorizontal: 0,
    marginBottom: 50,
  },
  center: {
    paddingHorizontal: 30,
    marginHorizontal: 0,
  },
  header: {
    height: 75,
    alignItems: 'center',
    marginTop: 85,
    paddingBottom: 0,
  },
  headerTitle: {
    fontSize: 27,
    fontFamily: 'CeraPro-Bold',
    color: black,
    fontWeight: '700',
  },
  headerSubtitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  subtitle: {
    fontSize: 16,
    fontFamily: 'CeraPro-Regular',
    color: '#ffffff',
    marginLeft: 8,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: 0, height: 1},
    textShadowRadius: 1,
    letterSpacing: .8,
  },
  searchButtonWp: {
    justifyContent: 'flex-end',
  },
  searchButton: {
    flexDirection: 'row',
    height: 65,
    alignItems: 'center',
    backgroundColor: primary,
    paddingHorizontal: 15,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  text: {
    color: '#ffffff',
    fontSize: 16,
    marginLeft: 8,
  },
  // - - - -
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 12,
    backgroundColor: lightGrey,
    paddingHorizontal: 15,
    marginVertical: 15,
  },
  searchInput: {
    flex: 1,
    marginLeft: 15,
    paddingVertical: Platform.OS === 'ios' ? 15 : 15, 
  },
  // - - - -
  optionButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  // - - - -
  historyWrapper: {
    flexDirection: 'column',
    marginVertical: 25,
    position: 'relative',
    zIndex: 9,
  },
  dateDivider: {
    marginVertical: 15,
  },
  textDateDivider:{
    fontSize: 16,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
    textTransform: 'uppercase',
    letterSpacing: 2,
    marginLeft: 10,
    fontWeight: '700',
  },
  ReceiptIcon: {
    height: 50,
    width: 50,
    borderRadius: 50,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
  },
  ReceiptItem: {
    width: '100%',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 8,
    backgroundColor: lightGrey,
    flexDirection: 'row',
    marginBottom: 15,
    alignItems: 'center',
  },
  ReceiptTitle: {
    fontSize: 16,
    color: black,
    fontFamily: 'CeraPro-Bold',
    marginBottom: 2,
    width: wp('30%'),
  },
  ReceiptSubtitle: {
    textTransform: 'capitalize',
    fontSize: 13,
    color: darkGrey,
  },
  ReceiptPrice: {
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    flex: 6,
    paddingHorizontal: 0,
    marginHorizontal: 0,
  },
  ReceiptPriceText: {
    textAlign: 'right',
    fontSize: 16,
    color: primary2,
    fontFamily: 'CeraPro-Bold',
  }

});