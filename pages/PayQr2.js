
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  StyleSheet,
  Dimensions,
  Vibration,
  Animated,
  Easing,
  View,
  Image,
  Modal,
  ImageBackground,
  TouchableOpacity,
  SafeAreaView,
  ScrollView,
  TextInput,
  Text,
  Platform,
  PermissionsAndroid,
} from 'react-native';

import Permissions from 'react-native-permissions';
import { RNCamera } from 'react-native-camera';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import { 
  styles
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

import BoxAccent from '../components/BoxAccent';
import Button from '../components/Button';

let ScreenHeight = Dimensions.get("window").height;

const PERMISSION_AUTHORIZED = 'authorized';
const CAMERA_PERMISSION = 'camera';


export default class QRCodeScanner extends Component {

  static navigationOptions = {
    header: null
  };

  static propTypes = {
    onRead: PropTypes.func.isRequired,
    vibrate: PropTypes.bool,
    reactivate: PropTypes.bool,
    reactivateTimeout: PropTypes.number,
    fadeIn: PropTypes.bool,
    showMarker: PropTypes.bool,
    cameraType: PropTypes.oneOf(['front', 'back']),
    customMarker: PropTypes.element,
    containerStyle: PropTypes.any,
    cameraStyle: PropTypes.any,
    markerStyle: PropTypes.any,
    topViewStyle: PropTypes.any,
    bottomViewStyle: PropTypes.any,
    topContent: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
    bottomContent: PropTypes.oneOfType([PropTypes.element, PropTypes.string]),
    notAuthorizedView: PropTypes.element,
    permissionDialogTitle: PropTypes.string,
    permissionDialogMessage: PropTypes.string,
    checkAndroid6Permissions: PropTypes.bool,
    cameraProps: PropTypes.object,
  };

  static defaultProps = {
    onRead: (e) => console.log("data scanned! ",e),
    // onRead: () => console.log('QR code scanned!'),
    reactivate: false,
    vibrate: true,
    reactivateTimeout: 5,
    fadeIn: true,
    showMarker: false,
    cameraType: 'back',
    notAuthorizedView: (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Text
          style={{
            textAlign: 'center',
            fontSize: 16,
          }}
        >
          Camera not authorized
        </Text>
      </View>
    ),
    pendingAuthorizationView: (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}
      >
        <Text
          style={{
            textAlign: 'center',
            fontSize: 16,
          }}
        >
          ...
        </Text>
      </View>
    ),
    permissionDialogTitle: 'Info',
    permissionDialogMessage: 'Need camera permission',
    checkAndroid6Permissions: false,
    cameraProps: {},
  };

  constructor(props) {
    super(props);
    this.state = {
      scanning: false,
      fadeInOpacity: new Animated.Value(0),
      isAuthorized: false,
      isAuthorizationChecked: false,
      disableVibrationByUser: false,
      modalVisible: false,
    };

    this._scannerTimeout    = null;
    this._handleBarCodeRead = this._handleBarCodeRead.bind(this);
    this.redirectToReceipt  = this.redirectToReceipt.bind(this);
  }


  async redirectToReceipt(e){
    console.log("e:")
    console.log(e)

    try {
        let response = await fetch(
            'http://103.3.62.188/osmosis/feedData_?member_id=26&store_id='+e.data,
        );
        console.log("redirect to home2")
        this.props.navigation.navigate('AfterScanPage'); 
    // this.props.navigation.navigate('PayReceipt', { receipt_id:e.data })

    }catch (error) {
        console.error(error);
    }
  }

  componentDidMount() {
    

    if (Platform.OS === 'ios') {
      Permissions.request(CAMERA_PERMISSION).then(response => {
        this.setState({
          isAuthorized: response === PERMISSION_AUTHORIZED,
          isAuthorizationChecked: true,
        });
      });

    } else if ( Platform.OS === 'android' && this.props.checkAndroid6Permissions) {

      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.CAMERA, {
        title: this.props.permissionDialogTitle,
        message: this.props.permissionDialogMessage,
      }).then(granted => {
        const isAuthorized =
          Platform.Version >= 23
            ? granted === PermissionsAndroid.RESULTS.GRANTED
            : granted === true;

        this.setState({ isAuthorized, isAuthorizationChecked: true });
      });
    } else {
      this.setState({ isAuthorized: true, isAuthorizationChecked: true });
    }
    
    if (this.props.fadeIn) {
      Animated.sequence([
        Animated.delay(1000),
        Animated.timing(this.state.fadeInOpacity, {
          toValue: 1,
          easing: Easing.inOut(Easing.quad),
        }),
      ]).start();
    }
  }

  componentWillUnmount() {

    if(this._scannerTimeout !== null) {
      clearTimeout(this._scannerTimeout);
    }
    this._scannerTimeout = null;

  }

  disable() {
    this.setState({ disableVibrationByUser: true });
  }
  enable() {
    this.setState({ disableVibrationByUser: false });
  }

  _setScanning(value) {
    this.setState({ scanning: value });
  }

  _handleBarCodeRead(e) {
    if (!this.state.scanning && !this.state.disableVibrationByUser) {
      if (this.props.vibrate) {
        Vibration.vibrate();
        this.redirectToReceipt(e);
        
      }
      this._setScanning(true);
      this.props.onRead(e);
      if (this.props.reactivate) {
        this._scannerTimeout = setTimeout(
          () => this._setScanning(false),
          this.props.reactivateTimeout
        );
      }
    }
  }

  _renderTopContent() {
    if (this.props.topContent) {
      return this.props.topContent;
    }
    return null;
  }

  _renderBottomContent() {
    if (this.props.bottomContent) {
      return this.props.bottomContent;
    }
    return null;
  }

  _renderCameraMarker() {
    if (this.props.showMarker) {
      if (this.props.customMarker) {
        return this.props.customMarker;
      } else {
        return (
          <View style={styles_.rectangleContainer}>
            <View style={[styles_.rectangle, this.props.markerStyle ? this.props.markerStyle : null]} />
          </View>
        );
      }
    }
    return null;
  }

  _renderCamera() {
    const {
      notAuthorizedView,
      pendingAuthorizationView,
      cameraType,
    } = this.props;
    const { isAuthorized, isAuthorizationChecked } = this.state;
    if (isAuthorized) {
      if (this.props.fadeIn) {
        return (
          <Animated.View
            style={{
              opacity: this.state.fadeInOpacity,
              backgroundColor: 'transparent',
            }}
          >
            <RNCamera
              style={[styles_.camera, this.props.cameraStyle]}
              onBarCodeRead={this._handleBarCodeRead.bind(this)}
              type={this.props.cameraType}
              captureAudio={false}
              {...this.props.cameraProps}
            >
              {this._renderCameraMarker()}
            </RNCamera>
          </Animated.View>
        );
      }
      return (
        <RNCamera
          type={cameraType}
          style={[styles_.camera, this.props.cameraStyle]}
          onBarCodeRead={this._handleBarCodeRead.bind(this)}
          captureAudio={false}
          {...this.props.cameraProps}
        >
          {this._renderCameraMarker()}
        </RNCamera>
      );
    } else if (!isAuthorizationChecked) {
      return pendingAuthorizationView;
    } else {
      return notAuthorizedView;
    }
  }

  reactivate() {
    this._setScanning(false);
  }

  // setModalVisible(visible) {
  //   this.setState({modalVisible: visible});
  // };

  render() {
    return (

      <View>
        {/* QR WRapper area */}
        <View style={page.QRWrapper}>
          {this._renderTopContent()}
          {this._renderCamera()}
          {this._renderBottomContent()}
        </View>
        {/* QR WRapper area - - - END */}

        {/* SafeAreaView  */}
        <SafeAreaView style={{ position: 'relative', zIndex: 99 }}>

          <Image
            style={{
              width:Dimensions.get('window').width,
              height:Dimensions.get('window').height,
              position: 'absolute',
              left: 0,
              top: 0,
            }}
            source={require('../assets/Union-2.png')}
          />

          <View style={[styles.mcGrid, page.top]}>
            {/* Top Navigation */}
            <View style={[styles.mcRow, styles.topNavigation]}>
              <TouchableOpacity 
                style={[styles.mcCol1, styles.backButton]}
                onPress={() => {this.props.navigation.navigate('Home'); }}>
                <View style={page.circleButton}>
                  <Image
                    style={{width: 30, height: 30, marginTop: 5}}
                    source={require('../assets/icon-chevron-left.png')}
                  />
                </View>
              </TouchableOpacity>
              <View style={[styles.mcCol4, styles.topNavTitle]}>
                <Text style={styles.pageTitle}></Text>
              </View>
              <View style={[styles.mcCol3, styles.leftIcons]}>
                {/* <View style={page.circleButton}>
                  <Image
                    style={{width: 16, height: 22}}
                    source={require('../assets/icon-lightbulb.png')}
                  />
                </View> */}
              </View>
            </View>
            {/* Top Navigation - - - END */}
          </View>
          
          {/* Square Accent Image */}  
          <View style={page.squareArea}>
            <TouchableOpacity
            style={{display: 'none'}}
            onPress={() => {this.props.navigation.navigate('AfterScanPage'); }}>
              <Image
                style={{width: 200, height: 200, marginTop: -100}}
                source={require('../assets/square-area.png')}
              />
            </TouchableOpacity>
            <Text style={page.textTitle}>Scan qr code and pay</Text>
            <Text style={page.textSubtitle}>As easy point your camera to qr code!</Text>

            <View style={page.circleButtonBottom}>
              <Image
                style={{width: 16, height: 22}}
                source={require('../assets/icon-lightbulb.png')}
              />
            </View>
          </View>
          {/* Square Accent Image - - - END */}

        </SafeAreaView>
        {/* SafeAreaView - - - END*/}

        {/* Modal Area */}  
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={page.modalOverlay}>
            {/* Close navigation for modal */}  
            <SafeAreaView>
              <View style={[styles.mcRow, styles.topNavigation]}>
                <TouchableOpacity 
                  style={[styles.mcCol1, styles.backButton]}
                  onPress={() => {
                  this.setModalVisible(false);
                }}>
                  <View style={page.circleButton}>
                    <Image
                      style={{ width: 30, height: 30, marginLeft: 5, transform: [{ rotate: '-90deg' }] }}
                      source={require('../assets/icon-chevron-left.png')}
                    />
                  </View>
                </TouchableOpacity>
                <View style={[styles.mcCol4, styles.topNavTitle]}>
                  <Text style={styles.pageTitle}></Text>
                </View>
                <View style={[styles.mcCol3, styles.leftIcons]}>
                </View>
              </View>
            </SafeAreaView>
            {/* Close navigation for modal - - - END */}
            <View style={page.modalBody}>
              <BoxAccent
                yAxis={50}
                xAxis={-125}
                width={200}
                height={200}
              ></BoxAccent>
              <BoxAccent
                yAxis={145}
                xAxis={225}
                width={350}
                height={200}
              ></BoxAccent>
              <View style={page.modalHeader}>
                <View style={page.modalHeaderImageWp}>
                  <View style={page.modalHeaderImageBg}>
                    <Image
                      style={page.modalHeaderImage}
                      source={require('../assets/brand-starbucks.png')}
                    />
                  </View>
                </View>
                {/* Name of store */}  
                <Text style={page.modalHeaderTitle}>
                  Starbucks
                </Text>
              {/* Location of store */} 
                <Text style={page.modalHeaderSubtitle}>
                  Ground Floor, Grand Indonesia
                </Text>
              </View>
              <View style={page.modalContent}>
                {/* Total Payment of store */} 
                <View style={page.modalTotalPayment}>
                  <Text style={[page.totalText, styles.medium]}>
                    Total Payment
                  </Text>
                  <View style={page.totalAmount}>
                    <Text style={[page.sup, styles.small]}>
                      Rp
                    </Text>
                    <Text style={[page.number, styles.xlarge]}>
                      12.280.720
                    </Text>
                  </View>
                </View>
              {/* Cashback of store */} 
                <View style={page.modalCashback}>
                  <Text style={[page.cashbackText, styles.medium]}>
                    Potensial Cashback
                  </Text>
                  <View style={page.cashbackAmount}>
                    <Text style={[page.cashbackText, styles.medium]}>
                      RP 20.000 !
                    </Text>
                  </View>
                </View>
              </View>
            {/* Next button actually */} 
              <View style={page.modalButton}>
                <Button 
                  text="Proceed to pay"
                  size="large"
                  theme="primary2"
                  onPress={() => {this.props.navigation.navigate('PayEnterPin'); this.setModalVisible(false)}} 
                />
              </View>
            </View>
          </View>
        </Modal>
        {/* Modal Area - - - END */}
        </View>
    );
  }
}

const styles_ = StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  infoView: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
    width: Dimensions.get('window').width,
  },

  camera: {
    flex: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
  },

  rectangleContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },

  rectangle: {
    height: 250,
    width: 250,
    borderWidth: 2,
    borderColor: '#00FF00',
    backgroundColor: 'transparent',
  },
});

const page = StyleSheet.create({

  top: {
    zIndex: 3,
  },

  QRWrapper: {
    height: ScreenHeight,
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1,
  },
  circleButton: {
    height: 40,
    width: 40,
    backgroundColor: '#fff',
    borderRadius: 60,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 6,
    elevation: 9,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
  },
  circleButtonBottom: {
    marginTop: 0,
    top: 25,
    height: 70,
    width: 70,
    backgroundColor: '#fff',
    borderRadius: 60,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 6,
    elevation: 9,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
  },
  squareArea: {
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTitle: {
    marginTop: 50,
    fontSize: 22,
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'CeraPro-Bold',
  },
  textSubtitle: {
    fontSize: 12,
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'CeraPro-Regular',
  },
  modalOverlay: {
    width: '100%', 
    height: '100%', 
    backgroundColor: 'rgba(0,0,0,.3)', 
    position: 'relative'
  },
  modalBody: {
    width: '100%', 
    height: 400, 
    backgroundColor: '#fff', 
    position: 'absolute', 
    bottom: 0,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
  },
  modalHeader: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -55,
  },
  modalHeaderImageWp: {
    height: 113,
    width: 113,
    backgroundColor: 'rgba(255,255,255, .7)',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalHeaderImageBg: {
    backgroundColor: 'rgba(255,255,255,1)',
    height: 85,
    width: 85,
    borderRadius: 100,
    // overflow: 'hidden',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 3,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
  },
  modalHeaderImage: {
    height: 63,
    width: 63,
  },
  modalHeaderTitle: {
    fontSize: 26,
    color: black,
    fontFamily: 'CeraPro-Bold',
  },
  modalHeaderSubtitle: {
    fontSize: 14,
    color: grey,
    fontFamily: 'CeraPro-Regular',
  },
  modalContent: {
    paddingHorizontal: 25,
  },
  modalTotalPayment: {
    position: 'relative',
    zIndex: 9,
    marginTop: 35,
    width: '100%',
    borderRadius: 8,
    backgroundColor: lightGrey,
    paddingHorizontal: 20,
    paddingVertical: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  totalText: {
    color: black,
    fontFamily: 'CeraPro-Bold',
    flex: 1,
  },
  totalAmount: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-end'
  },
  sup: {
    // fontSize: 14,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
    marginRight: 3,
  },
  number: {
    // fontSize: 24,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
  },
  modalCashback: {
    position: 'relative',
    zIndex: 8,
    width: '100%',
    borderRadius: 8,
    backgroundColor: '#E0EAFF',
    marginTop: -15,
    paddingTop: 30,
    paddingHorizontal: 20,
    paddingVertical: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
  cashbackText: {
    color: '#6E95E5',
    fontFamily: 'CeraPro-Bold',
    flex: 1,
  },
  modalButton: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    paddingBottom: 25,
  },


  pageWrapper: {
    marginTop: -65,
    paddingTop: 125,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 250,
    paddingHorizontal: 0,
    marginBottom: 50,
  },
  center: {
    paddingHorizontal: 30,
    marginHorizontal: 0,
  }

});