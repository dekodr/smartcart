import React from 'react';
import PropTypes from 'prop-types';
import { 
  View
  , Text
  , Button
  , StyleSheet
  , Dimensions 
  , TouchableHighlight
  , Image
  , ScrollView 
  , TextInput
  , SafeAreaView } from 'react-native';

import ListBox from '../components/ListBox';

let ScreenHeight = Dimensions.get("window").height;

class PayPopup extends React.Component {
  static navigationOptions = {
    header: null
  }

  state = {
    search: '',
  };

  updateSearch = search => {
    this.setState({ search });
  };

  render() {
    const mapMarker = {
      position: 'absolute',
      height: 55,
      width: 55,
    };
    const { search } = this.state;

    return (
      <View>
        <View style={styles.mapWrapper}>
          <Image
            style={{width: '100%', height: '100%'}}
            source={require('../assets/map-background.png')}
          />
        </View>
        <SafeAreaView style={{ position: 'relative', zIndex: 99, paddingBottom: 120}}>
          <View style={styles.mcGrid}>
            <View style={[styles.mcRow, styles.topNavigation]}>
              <TouchableHighlight 
                style={[styles.mcCol1, styles.backButton]}
                onPress={() => this.props.navigation.dismiss()}>
                <Image
                  style={{width: 35, height: 35}}
                  source={require('../assets/icon-chevron-left.png')}
                />
              </TouchableHighlight>
              <View style={[styles.mcCol4, styles.topNavTitle]}>
                <Text style={styles.pageTitle}></Text>
              </View>
              <View style={[styles.mcCol3, styles.leftIcons]}>
                
              </View>
            </View>

            <ScrollView style={[styles.mcGrid, styles.pageWrapper]}>

              <View style={[styles.mcRow, styles.firstRow, styles.header]}>
                <Text style={styles.title}>Pay at restaurant</Text>
                <Text style={styles.subtitle}>Select your restaurant first</Text>
              </View>
              <View style={[styles.mcRow, styles.center, styles.search]}>
                <View style={styles.mcCol6}>
                  <View style={styles.searchBar}>
                    <TextInput 
                      style={styles.searchInput}
                      placeholder={"Search here and let's go"}
                      placeholderTextColor={'#798191'}
                      onChangeText={this.updateSearch}
                      value={search}
                    />
                  </View>
                </View>
                <View style={[styles.mcCol1, styles.optionButton]}>
                  <Image
                    style={{width: 20, height: 20}}
                    source={require('../assets/icon-search-dollar.png')}
                  />
                </View>
              </View>

              <View style={{width: '100%', height: 200, backgroundColor: 'rgba(0,0,0,0)'}}></View>

              <View style={styles.pageContent}>
                <TouchableHighlight
                  onPress={
                    () => this.props.navigation.navigate('PayReceipt', { Component: { options: { bottomTabs: { visible: false, drawBehind: true, animate: true } } } })
                }>
                  <ListBox 
                    name="Lay's Store"
                    location="Thamrin City"
                    time="9:00 AM - 10:00 PM"
                    distance="2.8 KM"
                  />
                </TouchableHighlight>
                <ListBox 
                  name="Lay's Store"
                  location="Thamrin City"
                  time="9:00 AM - 10:00 PM"
                  distance="2.8 KM"
                />
                <ListBox 
                  name="Lay's Store"
                  location="Thamrin City"
                  time="9:00 AM - 10:00 PM"
                  distance="2.8 KM"
                />
                <ListBox 
                  name="Lay's Store"
                  location="Thamrin City"
                  time="9:00 AM - 10:00 PM"
                  distance="2.8 KM"
                />
                <ListBox 
                  name="Lay's Store"
                  location="Thamrin City"
                  time="9:00 AM - 10:00 PM"
                  distance="2.8 KM"
                />
              </View>
            </ScrollView>
          </View>
        </SafeAreaView>
        </View>
    );
  }
}

export default PayPopup;

const styles = StyleSheet.create({

  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  mcGrid: {
    margin: 0,
    padding: 0,
    zIndex: 0,
    position: "relative",
  },
  mcRow: {
    marginVertical: 0,
    marginHorizontal: 18,
    flexDirection: 'row',
  },
  fullWidth: {
    marginHorizontal: 0,
  },
  firstRow: {
    marginTop: 45,
  },
  mt15: {
    marginVertical: 15,
  },
  mt25: {
    marginVertical: 25,
  },
  mcCol1: {
    flex: 1,
  },
  mcCol2: {
    flex: 2,
  },
  mcCol3: {
    flex: 3,
  },
  mcCol4: {
    flex: 4,
  },
  mcCol5: {
    flex: 5,
  },
  mcCol6: {
    flex: 6,
  },
  block: {
    backgroundColor: '#dddddd',
    borderRadius: 8,
    height: 50,
    margin: 5,
  },

  sliderBox: {
    position: 'relative',
    zIndex: 9,
    elevation: 5,
  },

  buttonIcon: {
    height: 85,
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F6F6F6',
  },
  buttonText: {
    marginTop: 5,
    fontSize: 15,
    color: '#2A3348',
  },

  // topNavigation - - - H E R E
  topNavigation: {
    height: 50,
    marginVertical: 6,
    position: 'relative',
    zIndex: 9,
  },
  backButton: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  leftIcons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  topNavTitle: {
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  pageTitle: {
    fontSize: 22,
    color: '#2A3348',
    fontWeight: '700',
    paddingBottom: 8,
  },

  mapWrapper: {
    height: ScreenHeight,
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1
  },

  // pageWrapper - - - H E R E
  pageWrapper: {
    marginTop: -65,
    paddingTop: 65,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 175,
    marginBottom: 0,
    paddingHorizontal: 35,
  },
  pageHeader: {
    backgroundColor: 'rgba(255,255,255,.6)',
    padding: 15,
    position: 'relative',
    height: 85,
    marginTop: 15,
    borderTopLeftRadius: 14,
    borderTopRightRadius: 14,
  },
  headerTitle: {
    fontSize: 29,
    color: '#2A3348',
    fontWeight: '700',
    flex: 1,
  },
  headerSubtitle: {
    fontSize: 13,
    color: '#2A3348',
  },
  headerImage: {
    position: 'absolute',
    right: 35,
    top: -15,
    width: 65,
    height: 65,
    backgroundColor: 'rgba(255,255,255,.8)',
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pageContent: {
    padding: 15,
    backgroundColor: '#ffffff',
    borderRadius: 14,
    marginBottom: 200,
  },

  // - - - -
  header: {
    flexDirection: 'column',
  },
  title: {
    fontSize: 29,
    fontWeight: '700',
    color: '#2A3348',
  },
  subtitle: {
    fontSize: 16,
    color: '#798191',
  },

  search: {
    alignItems: 'center',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
    borderRadius: 8,
    height: 45,
    borderRadius: 8,
    marginVertical: 24,
  },
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    backgroundColor: '#ffffff',
    paddingHorizontal: 15,
    marginVertical: 24,
  },
  searchInput: {
    flex: 1,
    height: 45,
  },
  // - - - -
  optionButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f6f6f6',
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,
    height: 45,
  },
});




