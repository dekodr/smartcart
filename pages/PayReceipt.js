import React from 'react';
import PropTypes from 'prop-types';
import { 
  View
  , Text
  , StyleSheet
  , Dimensions 
  , TouchableHighlight
  , Image
  , ScrollView
  , TextInput
  , SafeAreaView
  , TouchableOpacity
  , Linking } from 'react-native';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import { 
  styles
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

import BoxAccent from '../components/BoxAccent2';
import Button from '../components/Button';
import SwipeablePanel from 'rn-swipeable-panel';

import ListBox from '../components/ListBox';
// import { TouchableOpacity } from 'react-native-gesture-handler';

let ScreenHeight = Dimensions.get("window").height;

class PayReceipt extends React.Component {

  constructor(props) {
    super(props);
    
  }
  static navigationOptions = {
    header: null
  }

  static propTypes = { 
    url: PropTypes.string 
  };

  state = {
    search     : '',
    receipt_id : '',
    loading    : true,
    orderList  : [],
    swipeablePanelActive : false,
    modalVisible : false,
    selectedProduct: '',
    selectedURL: '',
    selectedImage: '',
  };

  updateSearch = search => {
    this.setState({ search });
  };

  openPanel(product, link, image){
    if(link == '' || link == 'http://'){
      return false;
    }else{
      this.setState({ swipeablePanelActive: true, selectedProduct: product, selectedURL: link, selectedImage: image });
    }
    
  }

  closePanel =() =>{
    this.setState({ swipeablePanelActive: false });
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  };

  openURL = () => {
    
    Linking.canOpenURL(this.state.selectedURL).then(supported => {
      if (supported) {
        Linking.openURL(this.state.selectedURL);
      } else {
        console.log("Don't know how to open URI: " + this.state.selectedURL);
      }
    });
  };



  // Load Data to retrieve in page
  async componentDidMount(){
    try {
      let response = await fetch(
        'http://103.3.62.188/osmosis/getData_/'+this.props.navigation.state.params.receipt_id,
      );
      // +this.props.navigation.state.params.receipt_id
      let responseJson = await response.json();

      this.setState({orderList: responseJson, loading: false});
      console.log("orderlist:")
      console.log(this.state.orderList);

    }catch (error) {
      console.error(error);
    }
    console.log("component mount");
  }

  //for rendering each item, and pass data as a argument.
  renderOrderList() {
    let data = this.state.orderList.detail;
    console.log(data)
    return (<View>
                { data.map((item, key)=>(
                  <View style={[styles_.listDefault]}>
                    <View style={[styles_.mcCol1]}>
                      <Text style={styles_.listAmount}>{item.total_item}x</Text>
                    </View>
                    <View style={{flex: 6, justifyContent: 'flex-start', paddingHorizontal: 15,}}>
                      
                      {/* BUTTON TOUCHABLE TO OPEN WEBSITE IF ANY */}
                      <TouchableOpacity onPress={() => { this.openPanel(item.product_name, item.website, item.product_id); }}>
                        <Text style={styles_.listName}>{item.product_name}</Text>
                      </TouchableOpacity>

                    </View>
                    <View style={[styles_.mcCol2]}>
                      <Text style={styles_.listPrice}>{item.total_price}</Text>
                    </View>
                  </View>
                ))}

          </View>
          );
  }

  


  redirectToPayment(){
    console.log(this.state.orderList.transaction_total)

    this.props.navigation.navigate('PayEnterPin', { transaction_total:this.state.orderList.transaction_total })
  }

  render() {
    const mapMarker = {
      position: 'absolute',
      height: 55,
      width: 55,
    };
    
    const { search }      = this.state;
    const { receipt_id }  = this.props.navigation.state.params.receipt_id;
    let id_ = this.state.selectedImage.toString();

    // const linkImage       = 'file:///Users/didd/Documents/Office/mobile_/assets/product/'+this.state.selectedImage+'.jpg';
    
    
    const productImg = {
      25272: require('../assets/product/25272.jpg'),
      25273: require('../assets/product/25273.jpg'),
      25274: require('../assets/product/25274.jpg'),
      25275: require('../assets/product/25275.jpg'),
      25276: require('../assets/product/25276.jpg'),
      25277: require('../assets/product/25277.jpg'),
      25278: require('../assets/product/25278.jpg'),
      25279: require('../assets/product/25279.jpg'),
      25280: require('../assets/product/25280.jpg'),
      25281: require('../assets/product/25281.jpg'),
      25282: require('../assets/product/25282.jpg'),
      25283: require('../assets/product/25283.jpg'),
      25284: require('../assets/product/25284.jpg'),
      25285: require('../assets/product/25285.jpg'),
      25286: require('../assets/product/25286.jpg'),
      25287: require('../assets/product/25287.jpg'),
      25288: require('../assets/product/25288.jpg'),
      25271: require('../assets/product/25271.jpg'),
      25270: require('../assets/product/25270.jpg'),
      25269: require('../assets/product/25269.jpg'),
      25268: require('../assets/product/25268.jpg'),
      25267: require('../assets/product/25267.jpg'),
      25266: require('../assets/product/25266.jpg'),
      25262: require('../assets/product/25262.jpg'),
      25261: require('../assets/product/25261.jpg'),
      25260: require('../assets/product/25260.jpg'),
      25259: require('../assets/product/25259.jpg'),
      25258: require('../assets/product/25258.jpg'),
      25265: require('../assets/product/25265.jpg'),
      25264: require('../assets/product/25264.jpg'),
      25263: require('../assets/product/25263.jpg'),
    }
    

    if(this.state.loading == true){
      return (
        <View></View>
      );
    }else{
      return (
        <SafeAreaView style={{ paddingBottom: 125 }}>

          {/* closoe button  */}
          <TouchableOpacity style={page.roundedButton}>
              <Text style={page.roundedButtonText}>Close</Text>
          </TouchableOpacity>
          {/* closoe button - - - END */}

          <View style={styles_.mcGrid}>
            <View style={[styles_.mcRow, styles_.topNavigation]}>
              <TouchableHighlight 
                style={[styles_.mcCol1, styles_.backButton]}
                onPress={() => this.props.navigation.goBack()}>
                <Image
                  style={{width: 35, height: 35}}
                  source={require('../assets/icon-chevron-left.png')}
                />
              </TouchableHighlight>
              <View style={[styles_.mcCol4, styles_.topNavTitle]}>
                <Text style={styles_.pageTitle}></Text>
              </View>
              <View style={[styles_.mcCol3, styles_.leftIcons]}>
                
              </View>
            </View>
            <ScrollView style={[styles_.mcGrid, styles_.pageWrapper]}>

              <View style={{width: '100%', height: 10, backgroundColor: 'rgba(0,0,0,0)'}}></View>

              <TouchableOpacity  onPress={() => this.redirectToPayment()} >
                <View style={[styles_.mcRow, styles_.firstRow, styles_.header, styles_.center]}>
                  <Text style={styles_.title}>Order ID #{this.props.navigation.state.params.receipt_id}</Text>
                  <Text style={styles_.subtitle}>Just make sure everything is right!</Text>
                </View>
              </TouchableOpacity>

              <View style={[styles_.mcRow, styles_.contentList, styles_.center]}>

              {/* LOAD FROM renderOrderList */}
              {this.renderOrderList()}
              
                <View style={[styles_.mcRow, styles_.listTax]}>
                  <View style={[styles_.mcCol6]}>
                    <Text style={[styles_.alignLeft, styles_.total]}>Total</Text>
                  </View>
                  <View style={[styles_.mcCol6]}>
                    <TouchableOpacity  onPress={() => this.redirectToPayment()} >
                      <Text style={[styles_.alignRight, styles_.total]}>{this.state.orderList.transaction_total}</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>

              <View style={[styles_.mcRow, styles_.bottomButton]}>
                <View style={styles_.mcCol1}>
                  <TouchableOpacity  onPress={() => this.redirectToPayment()} >
                    <Button 
                      text="Proceed to Pay"
                      size="large"
                      theme="primary"
                      onPress={() => this.redirectToPayment()}
                    />
                  </TouchableOpacity>
                </View>
              </View>

            </ScrollView>
          </View>

          {/* POPUP */}
          <SwipeablePanel
            fullWidth
            isActive={this.state.swipeablePanelActive}
            onClose={this.closePanel}
            >
            <View style={[styles_.mcRow, styles_.topNavigation]}>
              <View 
                style={[styles_.mcCol3, styles_.backButton]}
                onPress={() => this.props.navigation.goBack()}>

              </View>
              <View style={[styles_.mcCol4, styles_.topNavTitle]}>
                <Text style={styles_.pageTitle}></Text>
              </View>
              <TouchableHighlight 
                style={[styles_.mcCol1, styles_.leftIcons]}
                onPress={() => this.closePanel()}>
                <View style={styles_.closeIconWp}>
                  <Image
                    style={{width: 30, height: 30}}
                    source={require('../assets/cancel.png')}
                  />
                </View>
              </TouchableHighlight>
            </View>
            <View style={page.panelHeader}>
              <Text style={page.panelHeaderText}>
                {this.state.selectedProduct}
              </Text>


              <View style={page.modalHeaderImageWp}>
                <View style={page.modalHeaderImageBg}>
                  <Image
                    style={page.modalHeaderImage}
                    source={productImg[this.state.selectedImage]}
                  />
                  
                </View>
              </View>
            </View>
            
            
            <View style={page.modalContent}>
              
              <TouchableOpacity style={[page.modalTotalPayment, styles_.visitLink]} onPress={() => this.openURL(this.state.selectedURL)}>
                <Text style={[page.totalText, styles_.medium, styles_.visitLinkText]}>
                  Visit 
                </Text>
              </TouchableOpacity>
              
            </View>
            
          </SwipeablePanel>

        </SafeAreaView>
      );
    }
  }
}

export default PayReceipt;

const styles_ = StyleSheet.create({

  visitLink: {
    backgroundColor: primary,
    paddingHorizontal: 25,
    borderRadius: 8,
    marginTop: 25,
    // backgroundColor: lightGrey,
    padding: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  visitLinkText: {
    color: '#fff',
    fontFamily: 'CeraPro-Bold',
    fontSize: 20,
    fontWeight: '700',
    textAlign: 'center',
  },

  contentList: {
    marginVertical: 35,
    flexDirection: 'column',
  },
  listDefault: {
    width: '100%',
    flexDirection: 'row',
    height: 55,
    alignItems: 'flex-start',
    borderBottomWidth: 1,
    borderColor: '#dddddd',
    marginTop: 10,
  },
  listAmount: {
    fontSize: 19,
    fontWeight: '700',
    letterSpacing: 2,
    color: '#2A3348',
  },
  alignLeft: {
    textAlign: 'left',
    fontWeight: '700',
    fontSize: 18,
    color: '#2A3348',
  },
  alignRight: {
    textAlign: 'right',
    fontWeight: '700',
    fontSize: 18,
    color: '#2A3348',
  },
  total: {
    fontSize: 20,
    color: '#36A49F',
  },
  listName: {
    fontSize: 14,
    color: '#2A3348',
    textAlign: 'left',
  },
  listPrice: {
    fontSize: 17,
    fontWeight: '700',
    color: '#36A49F',
  },
  listTax: {
    marginTop: 5,
  },
  bottomButton: {
    marginHorizontal: 5,
    paddingTop: 10,
    paddingBottom: 150,
  },

  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  mcGrid: {
    margin: 0,
    padding: 0,
    zIndex: 0,
    position: "relative",
    backgroundColor: '#ffffff',
  },
  mcRow: {
    marginVertical: 0,
    marginHorizontal: 18,
    flexDirection: 'row',
  },
  fullWidth: {
    marginHorizontal: 0,
  },
  firstRow: {
    marginTop: 45,
  },
  mt15: {
    marginVertical: 15,
  },
  mt25: {
    marginVertical: 25,
  },
  mcCol1: {
    flex: 1,
  },
  mcCol2: {
    flex: 2,
  },
  mcCol3: {
    flex: 3,
  },
  mcCol4: {
    flex: 4,
  },
  mcCol5: {
    flex: 5,
  },
  mcCol6: {
    flex: 6,
  },
  block: {
    backgroundColor: '#dddddd',
    borderRadius: 8,
    height: 50,
    margin: 5,
  },

  sliderBox: {
    position: 'relative',
    zIndex: 9,
    elevation: 5,
  },

  buttonIcon: {
    height: 85,
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F6F6F6',
  },
  buttonText: {
    marginTop: 5,
    fontSize: 15,
    color: '#2A3348',
  },

  // topNavigation - - - H E R E
  topNavigation: {
    height: 50,
    marginVertical: 6,
    position: 'relative',
    zIndex: 9,
  },
  backButton: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  leftIcons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  closeIconWp: {
    width: 30,
    height: 30,
    // backgroundColor: 'rgba(226,52,25,0.4)',
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  topNavTitle: {
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  pageTitle: {
    fontSize: 22,
    color: '#2A3348',
    fontWeight: '700',
    paddingBottom: 8,
  },

  mapWrapper: {
    height: ScreenHeight,
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1
  },

  center: {
    paddingHorizontal: 25,
  },
  // pageWrapper - - - H E R E
  pageWrapper: {
    marginTop: -65,
    paddingTop: 65,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 225,
  },
  pageHeader: {
    backgroundColor: 'rgba(255,255,255,.6)',
    padding: 15,
    position: 'relative',
    height: 85,
    marginTop: 15,
    borderTopLeftRadius: 14,
    borderTopRightRadius: 14,
  },
  headerTitle: {
    fontSize: 29,
    color: '#2A3348',
    fontWeight: '700',
    flex: 1,
  },
  headerSubtitle: {
    fontSize: 13,
    color: '#2A3348',
  },
  headerImage: {
    position: 'absolute',
    right: 35,
    top: -15,
    width: 65,
    height: 65,
    backgroundColor: 'rgba(255,255,255,.8)',
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pageContent: {
    padding: 15,
    backgroundColor: '#ffffff',
    borderBottomLeftRadius: 14,
    borderBottomRightRadius: 14,
    marginBottom: 100,
  },

  // - - - -
  header: {
    flexDirection: 'column',
  },
  title: {
    fontSize: 29,
    fontWeight: '700',
    color: '#2A3348',
  },
  subtitle: {
    fontSize: 16,
    color: '#798191',
  },

  search: {
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
    borderRadius: 8,
    height: 45,
    borderRadius: 8,
    marginVertical: 24,
    marginHorizontal: 35, 
  },
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    backgroundColor: '#ffffff',
    paddingHorizontal: 15,
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 4 },
    // shadowOpacity: 0.3,
    // shadowRadius: 4,
    // elevation: 10,
    // borderRadius: 8,
  },
  searchInput: {
    flex: 1,
    height: 45,
  },
  // - - - -
  optionButton: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f6f6f6',
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,
    height: 45,
  },

});


const page = StyleSheet.create({

  top: {
    zIndex: 3,
  },

  BgWrapper: {
    height: ScreenHeight,
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1,
    backgroundColor: primary2,
  },
  circleButton: {
    height: 40,
    width: 40,
    backgroundColor: '#fff',
    borderRadius: 35,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 6,
    elevation: 9,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
  },
  centerArea: {
    width: '100%',
    paddingTop: 50,
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 0,
  },
  centerText: {
    fontSize: 27,
    fontFamily: 'CeraPro-Bold',
    color: '#fff',
    marginBottom: 165,
  },
  centerCard: {
    position: 'relative',
    zIndex: 0,
    width: 288,
    height: 161,
    borderRadius: 12,
    backgroundColor: '#fff',
    paddingVertical: 10,
    paddingHorizontal: 15,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 18,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 12 },
    shadowOpacity: 0.2,
    shadowRadius: 16,
  },
  centerImage: {
    position: 'absolute',
    top: -150,
  },
  cardTotal: {
    fontSize: 36,
    fontFamily: 'CeraPro-Bold',
    color: black,
    marginTop: 25,
  },
  cardStore: {
    fontSize: 20,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
  },
  roundedButton: {
    marginTop: 35,
    width: 170,
    alignItems: 'center',
    justifyContent: 'center',
    height: 49,
    borderRadius: 50,
    backgroundColor: 'rgba(255,255,255,.30)',
  },
  roundedButtonText: {
    fontSize: 23,
    fontFamily: 'CeraPro-Regular',
    color: '#fff',
    marginTop: 5,
  },
  bottomPanel: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    width: '100%',
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
    backgroundColor: '#fff',
    paddingVertical: 25,
    paddingBottom : 40,
    marginTop: -15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bottomPanelText: {
    fontSize: 16,
    color: primary2,
    fontFamily: 'CeraPro-Regular'
  },

  // - - - Panel Styling
  panelHeader: {
    width: '100%',
    alignItems: 'center',
    marginTop: 15,
  },
  panelHeaderText: {
    fontSize: 16,
    color: primary2,
    fontFamily: 'CeraPro-Regular'
  },
  panelBottom: {
    width: '100%',
    alignItems: 'center',
    marginTop: 35,
  },
  panelBottomText: {
    fontSize: 16,
    color: primary2,
    fontFamily: 'CeraPro-Regular'
  },
  textBold: {
    fontFamily: 'CeraPro-Bold',
  },

  // - - - Modal Styling
  modalHeader: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalHeaderImageWp: {
    height: 113,
    width: 113,
    backgroundColor: 'rgba(255,255,255, .7)',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalHeaderImageBg: {
    backgroundColor: 'rgba(255,255,255,1)',
    height: 85,
    width: 85,
    borderRadius: 100,
    // overflow: 'hidden',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 3,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
  },
  modalHeaderImage: {
    height: 63,
    width: 63,
  },
  modalHeaderTitle: {
    fontSize: 26,
    color: black,
    fontFamily: 'CeraPro-Bold',
  },
  modalHeaderSubtitle: {
    fontSize: 14,
    color: grey,
    fontFamily: 'CeraPro-Regular',
  },
  modalContent: {
    paddingHorizontal: 25,
    // borderRadius: 8,
    // marginTop: 25,
    // backgroundColor: lightGrey,
    // padding: 8,
    // marginHorizontal: 25,
  },
  modalTotalPayment: {
    position: 'relative',
    zIndex: 9,
    width: '100%',
    backgroundColor: lightGrey,
    paddingHorizontal: 0,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  totalText: {
    color: black,
    fontFamily: 'CeraPro-Bold',
    flex: 1,
    fontSize: 16,
  },
  totalAmount: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-end'
  },
  sup: {
    // fontSize: 14,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
    marginRight: 3,
  },
  number: {
    // fontSize: 24,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
  },
  // - - - Modal Styling End

  // - - - 
  swipeableWp: {
    position: 'relative',
    height: ScreenHeight,
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 3,
  },
  SwipeUpDown: {
    marginTop: '10%',
    paddingHorizontal: 0,
    backgroundColor: 'rgba(0,0,0,.2)',
    overflow: 'hidden',
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
  },
  miniSwipe: {
    width: '100%',
    backgroundColor: '#fff',
    height: 100,
    alignItems: 'center',
    marginTop: -25,
    paddingTop: 25,
  },
  miniSwipeText: {
    fontSize: 16,
    color: primary2,
    fontFamily: 'CeraPro-Regular'
  },


  pageWrapper: {
    marginTop: -65,
    paddingTop: 125,
    position: 'relative',
    zIndex: 0,
    paddingBottom: 250,
    paddingHorizontal: 0,
    marginBottom: 50,
  },
  center: {
    paddingHorizontal: 30,
    marginHorizontal: 0,
  }

});