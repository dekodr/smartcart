import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , TouchableOpacity 
  , TextInput
  , Footer
  , SafeAreaView } from 'react-native';

  import { createAppContainer } from 'react-navigation';
  import { createStackNavigator } from 'react-navigation-stack';
  import { createBottomTabNavigator } from 'react-navigation-tabs';
import Modal from "react-native-modal";

import { 
  styles
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

import PromoBoxNearby from '../components/PromoBoxNearby';
import BoxAccent from '../components/BoxAccent';
import Button from '../components/Button';
import Video from 'react-native-video';

let ScreenHeight = Dimensions.get("window").height;

class WatchPage extends React.Component {
  static navigationOptions = {
    header: null
  }
  
  state = {
    search: '',
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  updateSearch = search => {
    this.setState({ search });
  };

  render() {
    const { search } = this.state;

    const mapMarker = {
      position: 'absolute',
      height: 55,
      width: 55,
    };

    return (
        <SafeAreaView style={{ position: 'relative', zIndex: 99 }}>
          <View style={[styles.mcGrid, page.top]}>

            <View style={[styles.mcRow, styles.topNavigation]}>
              <TouchableOpacity 
                style={[styles.mcCol1, styles.backButton]}
                onPress={() => this.props.navigation.goBack()}>
                <Image
                  style={{width: 35, height: 35}}
                  source={require('../assets/icon-chevron-left.png')}
                />
              </TouchableOpacity>
              <View style={[styles.mcCol4, styles.topNavTitle]}>
                <Text style={styles.pageTitle}></Text>
              </View>
              <View style={[styles.mcCol3, styles.leftIcons]}>
                
              </View>
            </View>

          </View>
          <ScrollView style={[styles.mcGrid, page.pageWrapper]}>
            <BoxAccent
              yAxis={-25}
              xAxis={325}
              width={200}
              height={200}
            ></BoxAccent>
            <BoxAccent
              yAxis={-200}
              xAxis={-100}
              width={300}
              height={300}
            ></BoxAccent>

            <View style={[styles.mcRow, page.center, page.header]}>
              <View style={styles.mcCol6}>
                <Text style={[page.headerTitle, {fontWeight: '700'}]}>Watch Videos</Text>
              </View>
            </View>

            <View style={[styles.mcRow, page.center]}>
              <View style={page.bgImageWp}>
                <Video source={require('../assets/dairy-vid.mp4')}  
                   ref={(ref) => {
                     this.player = ref
                   }}
                   resizeMode="cover"                            
                   onBuffer={this.onBuffer}               
                   onError={this.videoError}              
                   style={{height: 280, width: '100%'}} />
                {/* <ImageBackground 
                  source={require('../assets/icon-watch.png')}
                  style={{height:280, width:'100%', borderRadius: 16, top: -30}}>
                </ImageBackground> */}
              </View>
            </View>

            <View style={[styles.row, page.center, {marginVertical: 15}]}>
              <Text style={page.caption}>
                Watch this nice advertise to get points !
              </Text>
            </View>

            <View style={[styles.row, {marginTop: 25, marginHorizontal: 35, justifyContent: 'center', alignItems: 'center'}]}>
              <TouchableOpacity 
                style={
                  [page.ModalButtonWp, 
                  { width: '100%',
                    shadowColor: primary,
                    shadowOffset: { width: 0, height: 4 },
                    shadowOpacity: 0.3,
                    shadowRadius: 4,
                 }]}
                onPress={this.toggleModal}>
                <Text style={[page.ModalButtonText, {fontSize: 17}]}>Play Video - Get 30 Points</Text>
              </TouchableOpacity>
            </View>

          </ScrollView>

          {/* Modal Area */}  
          <Modal isVisible={this.state.isModalVisible}>
            <View style={page.modalWp}>
              <View style={page.modalInner}>
                <Image
                    style={page.modalImgAccent}
                    source={require('../assets/success-img.png')}
                  />
                {/* info : button closah */}
                <TouchableOpacity 
                  style={page.closeButton}
                  onPress={this.toggleModal}>
                    <Image
                      style={{width: 10, height: 10}}
                      source={require('../assets/icon-times-white.png')}
                    />
                </TouchableOpacity>
                {/* info : nama barangnye dimareh */}
                <Text style={page.modalTextName}>Yay! You Get</Text>
                <View style={page.ModalrowButton}>
                  {/* info : button info */}
                  <TouchableOpacity 
                    style={page.ModalButtonWp}
                    onPress={this.toggleModal}>
                    <Text style={page.ModalButtonText}>15 Points</Text>
                  </TouchableOpacity>

                  <Text style={page.TextSmall, {color: grey, textAlign: 'center'}}>Check your accounf for more info</Text>
                </View>
              </View>
            </View>
          </Modal>
          {/* Modal Area - - - END */}
        </SafeAreaView>
    );
  }
};

export default WatchPage;

const page = StyleSheet.create({

  // NEW VERY NEW NEW
  // M O D A L
  modalWp: {
    backgroundColor: 'rgba(0,0,0,0)',
    height: 350,
    width: '100%',
    paddingTop: 45,
  },
  modalInner: {
    backgroundColor: '#fff',
    height: '100%',
    width: '100%',
    borderRadius: 16,
    paddingTop: 45,
    paddingHorizontal: 25,
    paddingBottom: 25
  },
  modalImgAccent: {
    position: 'absolute',
    alignSelf: 'center',
    top: -160,
    height: 250,
    width: 250,
  },
  modalTextName: {
    fontSize: 32,
    fontFamily: 'CeraPro-Bold',
    color: black,
    fontWeight: '700',
    marginBottom: 25,
    textAlign: 'center',
    marginTop: 25,
  },
  ModalrowButton: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  ModalButtonWp: {
    width: '80%',
    height: 85,
    // padding: 15,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: primary,
    borderRadius: 16,
    overflow: 'hidden',
    marginBottom: 25,
  },
  ModalButton: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    left: 0,
    top: 0,
    zIndex: 0,
    justifyContent: 'center',
    alignItems: 'center',

  },
  ModalButtonText: {
    fontSize: 32,
    fontFamily: 'CeraPro-Bold',
    color: '#ffffff',
    fontWeight: '700',
    position: 'relative',
    zIndex: 1,
    textAlign: 'center',
  },
  closeButton: {
    height: 25,
    width: 25,
    backgroundColor: 'rgba(211,62,62,1)',
    borderRadius: 25,
    position: 'absolute',
    right: 7,
    top: 7,
    alignItems: 'center',
    justifyContent: 'center',
  },
  // M O D A L - - - E N D
  // END

  bgImageWp: {
    width: '100%',
    height: 200,
    borderRadius: 16,
    overflow: 'hidden',
    position: 'relative',
    marginVertical: 25,
  },
  caption: {
    fontSize: 16,
    fontWeight: '400',
    fontFamily: 'CeraPro-Regular',
    color: '#798191',
  },

  top: {
    zIndex: 3,
  },

  pageWrapper: {
    marginTop: -65,
    paddingTop: 0,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 250,
    paddingHorizontal: 0,
    marginBottom: 50,
  },
  center: {
    paddingHorizontal: 30,
    marginHorizontal: 0,
  },
  header: {
    alignItems: 'center',
    marginTop: 125,
    paddingBottom: 0,
  },
  headerTitle: {
    fontSize: 27,
    fontFamily: 'CeraPro-Bold',
    color: black,
  },
  headerSubtitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  subtitle: {
    fontSize: 16,
    fontFamily: 'CeraPro-Regular',
    color: '#ffffff',
    marginLeft: 8,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: 0, height: 1},
    textShadowRadius: 1,
    letterSpacing: .8,
  },
  searchButtonWp: {
    justifyContent: 'flex-end',
  },
  searchButton: {
    flexDirection: 'row',
    height: 65,
    alignItems: 'center',
    backgroundColor: primary,
    paddingHorizontal: 15,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  text: {
    color: '#ffffff',
    fontSize: 16,
    marginLeft: 8,
  },
  // - - - -
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 12,
    backgroundColor: lightGrey,
    paddingHorizontal: 15,
    marginVertical: 15,
  },
  searchInput: {
    flex: 1,
    marginLeft: 15,
    paddingVertical: Platform.OS === 'ios' ? 15 : 15, 
  },
  // - - - -
  optionButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  // - - - -
  historyWrapper: {
    flexDirection: 'column',
    marginVertical: 25,
    position: 'relative',
    zIndex: 9,
  },
  dateDivider: {
    marginVertical: 15,
  },
  textDivider:{
    fontSize: 16,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
    textTransform: 'uppercase',
    letterSpacing: 2,
    marginBottom: 15,
  },
  historyItem: {
    width: '100%',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 8,
    backgroundColor: lightGrey,
    flexDirection: 'row',
    marginBottom: 15,
  },
  historyTitle: {
    fontSize: 16,
    color: black,
    fontFamily: 'CeraPro-Bold',
    marginBottom: 2,
  },
  historySubtitle: {
    textTransform: 'capitalize',
    fontSize: 13,
    color: darkGrey,
  },
  historyPrice: {
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    flex: 6,
    paddingHorizontal: 0,
    marginHorizontal: 0,
  },
  historyPriceText: {
    textAlign: 'right',
    fontSize: 16,
    color: primary2,
    fontFamily: 'CeraPro-Bold',
  }

});