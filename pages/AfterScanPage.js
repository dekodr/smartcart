import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , Modal
  , TouchableOpacity 
  , TextInput
  , SafeAreaView } from 'react-native';
  import { createAppContainer } from 'react-navigation';
  import { createStackNavigator } from 'react-navigation-stack';
  import { createBottomTabNavigator } from 'react-navigation-tabs';

import { 
  styles
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

import BoxAccent from '../components/BoxAccent2';
import Button from '../components/Button';
import SwipeablePanel from 'rn-swipeable-panel';
// import PayQrModal from '../components/PayQrModal';

let ScreenHeight = Dimensions.get("window").height;

class PayQrSuccess extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
        swipeablePanelActive: false
    };
  };

  // componentDidMount = () => {
  //   this.openPanel();
  // };

  openPanel = () => {
    this.setState({ swipeablePanelActive: true });
  };

  closePanel = () => {
    this.setState({ swipeablePanelActive: false });
  };

  state = {
    modalVisible: false,
  };

  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  };

  render() {

    return (
      <View>
         {/* Background Color */}
        <View style={page.BgWrapper}>
          <BoxAccent
            yAxis={50}
            xAxis={-175}
            width={300}
            height={300}
          ></BoxAccent>
          <BoxAccent
            yAxis={-85}
            xAxis={75}
            width={200}
            height={150}
          ></BoxAccent>
          <BoxAccent
            bottom={-95}
            xAxis={215}
            width={200}
            height={200}
          ></BoxAccent>
        </View>
        {/* Background Color - - - END */}

        {/* SafeAreaView */}
        <SafeAreaView style={{ height: ScreenHeight, position: 'relative', zIndex: 2 }}>

          <View style={[styles.mcGrid, page.top]}>
            {/* Top Navigation */}
              <View style={[styles.mcRow, styles.topNavigation]}>
                <TouchableOpacity 
                  style={[styles.mcCol1, styles.backButton]}
                  onPress={() => {this.props.navigation.navigate('Home'); }}>
                  <View style={page.circleButton}>
                    <Image
                      style={{width: 15, height: 15}}
                      source={require('../assets/icon-times.png')}
                    />
                  </View>
                </TouchableOpacity>
                <View style={[styles.mcCol4, styles.topNavTitle]}>
                  <Text style={styles.pageTitle}></Text>
                </View>
                <View style={[styles.mcCol3, styles.leftIcons]}>
                </View>
              </View>
            {/* Top Navigation - - -  END */}
          </View>
            
          <View style={page.centerArea}>
            <Text style={page.centerText}>

            </Text>

          {/* center card area  */}
            <View style={page.centerCard}>
              <Image
                style={page.centerImage}
                source={require('../assets/store.png')}
              />
              
              <Text style={page.modalHeaderTitle}>
                Smartcart Store
              </Text>
              <Text style={page.modalHeaderSubtitle}>
                Soedarpo Informatika
              </Text>
            </View>
          {/* center card area - - - END */}

          {/* closoe button  */}
            <TouchableOpacity 
              style={page.roundedButton}
              onPress={() => {this.props.navigation.navigate('Home'); }}>
              <Text style={page.roundedButtonText}>Close</Text>
            </TouchableOpacity>
          {/* closoe button - - - END */}
          </View>

        </SafeAreaView>
        {/* SafeAreaView - - - END*/}
      </View>
    );
  }
};

export default PayQrSuccess;

const page = StyleSheet.create({

  top: {
    zIndex: 3,
  },

  BgWrapper: {
    height: ScreenHeight,
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1,
    backgroundColor: primary2,
  },
  circleButton: {
    height: 40,
    width: 40,
    backgroundColor: '#fff',
    borderRadius: 35,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 6,
    elevation: 9,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
  },
  centerArea: {
    width: '100%',
    paddingTop: 50,
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
    zIndex: 0,
  },
  centerText: {
    fontSize: 27,
    fontFamily: 'CeraPro-Bold',
    color: '#fff',
    marginBottom: 165,
  },
  centerCard: {
    position: 'relative',
    zIndex: 0,
    width: 288,
    height: 161,
    borderRadius: 12,
    backgroundColor: '#fff',
    paddingVertical: 10,
    paddingHorizontal: 15,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 18,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 12 },
    shadowOpacity: 0.2,
    shadowRadius: 16,
  },
  centerImage: {
    position: 'absolute',
    top: -120,
    height: 150,
    width: 150,
  },
  cardTotal: {
    fontSize: 20,
    fontFamily: 'CeraPro-Bold',
    color: grey,
    marginTop: 25,
    marginBottom: 15,
  },
  cardStore: {
    fontSize: 20,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
  },
  roundedButton: {
    marginTop: 35,
    width: 170,
    alignItems: 'center',
    justifyContent: 'center',
    height: 49,
    borderRadius: 50,
    backgroundColor: 'rgba(255,255,255,.30)',
  },
  roundedButtonText: {
    fontSize: 23,
    fontFamily: 'CeraPro-Regular',
    color: '#fff',
    marginTop: 5,
  },
  bottomPanel: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    width: '100%',
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
    backgroundColor: '#fff',
    paddingVertical: 25,
    paddingBottom : 40,
    marginTop: -15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bottomPanelText: {
    fontSize: 16,
    color: primary2,
    fontFamily: 'CeraPro-Regular'
  },

  // - - - Panel Styling
  panelHeader: {
    width: '100%',
    alignItems: 'center',
    marginTop: 15,
  },
  panelHeaderText: {
    fontSize: 16,
    color: primary2,
    fontFamily: 'CeraPro-Regular'
  },
  panelBottom: {
    width: '100%',
    alignItems: 'center',
    marginTop: 35,
  },
  panelBottomText: {
    fontSize: 16,
    color: primary2,
    fontFamily: 'CeraPro-Regular'
  },
  textBold: {
    fontFamily: 'CeraPro-Bold',
  },

  // - - - Modal Styling
  modalHeader: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalHeaderImageWp: {
    height: 113,
    width: 113,
    backgroundColor: 'rgba(255,255,255, .7)',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalHeaderImageBg: {
    backgroundColor: 'rgba(255,255,255,1)',
    height: 85,
    width: 85,
    borderRadius: 100,
    // overflow: 'hidden',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 3,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
  },
  modalHeaderImage: {
    height: 63,
    width: 63,
  },
  modalHeaderTitle: {
    fontSize: 26,
    color: black,
    fontFamily: 'CeraPro-Bold',
  },
  modalHeaderSubtitle: {
    fontSize: 14,
    color: grey,
    fontFamily: 'CeraPro-Regular',
  },
  modalContent: {
    paddingHorizontal: 25,
    borderRadius: 8,
    marginTop: 25,
    backgroundColor: lightGrey,
    padding: 8,
    marginHorizontal: 25,
  },
  modalTotalPayment: {
    position: 'relative',
    zIndex: 9,
    width: '100%',
    backgroundColor: lightGrey,
    paddingHorizontal: 0,
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  totalText: {
    color: black,
    fontFamily: 'CeraPro-Bold',
    flex: 1,
    fontSize: 16,
  },
  totalAmount: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-end'
  },
  sup: {
    // fontSize: 14,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
    marginRight: 3,
  },
  number: {
    // fontSize: 24,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
  },
  // - - - Modal Styling End

  // - - - 
  swipeableWp: {
    position: 'relative',
    height: ScreenHeight,
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 3,
  },
  SwipeUpDown: {
    marginTop: '10%',
    paddingHorizontal: 0,
    backgroundColor: 'rgba(0,0,0,.2)',
    overflow: 'hidden',
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
  },
  miniSwipe: {
    width: '100%',
    backgroundColor: '#fff',
    height: 100,
    alignItems: 'center',
    marginTop: -25,
    paddingTop: 25,
  },
  miniSwipeText: {
    fontSize: 16,
    color: primary2,
    fontFamily: 'CeraPro-Regular'
  },


  pageWrapper: {
    marginTop: -65,
    paddingTop: 125,
    position: 'relative',
    zIndex: 0,
    paddingBottom: 250,
    paddingHorizontal: 0,
    marginBottom: 50,
  },
  center: {
    paddingHorizontal: 30,
    marginHorizontal: 0,
  }

});