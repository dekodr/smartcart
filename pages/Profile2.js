import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , Modal
  , SafeAreaView
  , StatusBar
  , Footer
  , TouchableOpacity } from 'react-native';
  import { createAppContainer } from 'react-navigation';
  import { createStackNavigator } from 'react-navigation-stack';
  import { createBottomTabNavigator } from 'react-navigation-tabs';
// import LinearGradient from 'react-native-linear-gradient';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { normalize } from '../assets/Normalize';
import FlipCard from 'react-native-flip-card'

import Button from '../components/Button';
import UserCard from '../components/UserCard';
// import TextDivider from '../components/TextDivider';
import PromoBox2 from '../components/PromoBox2';
import PromoBox1 from '../components/PromoBox1';
import PromoBox3 from '../components/PromoBox3';
import BoxAccent from '../components/BoxAccent';
import BoxAccent2 from '../components/BoxAccent2';
import UserBalance from '../components/UserBalance';
import Carousel from 'react-native-snap-carousel';

import MemberCard from '../components/MemberCard';
import DefaultList from '../components/DefaultList';

import { 
  black
  , styles
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

let ScreenHeight = Dimensions.get("window").height;

class Home2 extends React.Component {
  static navigationOptions = {
    header: null
  }

  // modal visibility
  state = {
    modalVisible: false,
  };

  _renderItem () {
    return (
      <TouchableOpacity>
        <View style={[page.MagazineSlide, page.slideFirst]}>
          <View style={page.MagazineSlideCover}>
            <Image
              style={page.MagazineSlideCoverImg}
              source={require('../assets/magazine-cover-2.png')}
            />
          </View>
          <View style={page.MagazineSlideText}>
            <Text style={page.MagazineSlideTextTitle}>Mall Magazine Vol.10</Text>
            <Text style={page.MagazineSlideTextSmall}>12 Feb 2019</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  // set modal visible function
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  };

  constructor(props){
    super(props);

    // data buat slider dibawah profile
    this.state = {
      entries: [
        { 'id': '1', 
          'name': 'Your loyalty Points !',
          'bgColor': '#BC7BB7',
          'amount': '1.828',
          'mL': 35,
          'image': require('../assets/cartoon-1.png')
        },
        { 'id': '2', 
          'name': 'Average montly savings',
          'bgColor': '#8293DD',
          'amount': '418.620',
          'mL': 25,
          'image': require('../assets/cartoon-5.png')
        },
        { 'id': '3', 
          'name': 'Your daily promo usage',
          'bgColor': '#76D0D0',
          'amount': '2',
          'mL': 25,
          'image': require('../assets/cartoon-3.png')
        }
      ],
    }
    // data buat slider dibawah profile - - - END
  }
  

  _onPressCarousel = () => {
    // here handle carousel press
  }

  _renderItem ({item, index}) {
    return (
      <TouchableOpacity>
        <View style={[page.prfSlider, {marginLeft: item.mL, backgroundColor: item.bgColor}]}>
          <Image
            style={page.prfImgAccent}
            source={item.image}
          />
          <View style={page.prfSliderText}>
            <Text style={page.prfSliderTextUpper}>{item.name}</Text>
            <Text style={page.prfSliderTextUnder}>{item.amount}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar  
          backgroundColor = "rgba(0,0,0,0)"  
          barStyle = "dark-content"   
          hidden = {false}    
          translucent = {true}  
        />
        
        <ScrollView style={styles.mcGrid}>
          <BoxAccent
            yAxis={110}
            xAxis={-175}
            width={300}
            height={300}
          ></BoxAccent>
          <BoxAccent
            yAxis={-125}
            xAxis={25}
            width={200}
            height={200}
          ></BoxAccent>

          <SafeAreaView style={{display: 'none', marginTop: Platform.OS === 'android' ? 5 : 0}}>
            {/* Top Navigation */}
              <View style={[styles.mcRow, styles.topNavigation, {marginTop: Platform.OS === 'android' ? 50 : 0}]}>
                <TouchableOpacity 
                  style={[styles.mcCol3, styles.backButton]}
                  onPress={() => {this.props.navigation.navigate('Home2'); }}>
                  <View style={styles.logo}>
                    <Image
                      style={{width: 23, height: 18}}
                      source={require('../assets/menu-icon.png')}
                    />
                  </View>
                </TouchableOpacity>
                <View style={[styles.mcCol1, styles.topNavTitle]}>
                  <Image
                    style={{width: 115, height: 25}}
                    source={require('../assets/smartcart-logo.png')}
                  />
                </View>
                <View style={[styles.mcCol3, styles.leftIcons]}>
                  <Image
                      style={{width: 18, height: 18}}
                      source={require('../assets/icon-bell-notification-green.png')}
                    />
                </View>
              </View>
            {/* Top Navigation - - -  END */}
          </SafeAreaView>

          {/* Ini header */}
            <View style={page.PrfHeader}>
              <View style={page.PrfHeaderLeft}>
                <Text style={page.PrfHeaderName}>Glantino Putra</Text>
                <Text style={page.PrfHeaderMembership}>SILVER MEMBER</Text>
              </View>
              <View style={page.PrfHeaderRight}>
                <View style={page.PrfHeaderProfile}>
                  <Image
                    style={page.profilePicture}
                    source={require('../assets/profile-pic.png')}
                  />
                  <View style={page.verifiedIcon}>
                    <Image
                      style={page.vrfIcon}
                      source={require('../assets/icon-check.png')}
                    />
                  </View>
                </View>
              </View>
            </View>
          {/* Ini header - - - END */}
          {/* coba coba carousel */}
            <View style={[styles.sliderDefault, {}]}>
              <View styles={[page.carouselWp]}>
                <Carousel
                  layout={'default'}
                  layoutCardOffset={'18'}
                  ref={(c) => { this._carousel = c; }}
                  data={this.state.entries}
                  renderItem={this._renderItem}
                  sliderWidth={wp('100%')}
                  itemWidth={200}
                  // loop={true}
                  // loopClonesPerSide={6}
                  activeSlideAlignment={'start'}
                  inactiveSlideShift={2}
                  inactiveSlideOpacity={1}
                  inactiveSlideScale={.9}
                />
              </View>
            </View> 
          {/* coba coba carousel - - - END */}
          {/* ini slider dibawah header, u see it */}
            <View style={[styles.sliderDefault, {display: 'none'}]}>
              <ScrollView 
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                >
                <TouchableOpacity>
                  <View style={[page.prfSlider, page.slideFirst]}>
                    <Image
                      style={page.prfImgAccent}
                      source={require('../assets/cartoon-1.png')}
                    />
                    <View style={page.prfSliderText}>
                      <Text style={page.prfSliderTextUpper}>Your loyalty Points !</Text>
                      <Text style={page.prfSliderTextUnder}>1.828</Text>
                    </View>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={[page.prfSlider, {backgroundColor: '#8293DD',}]}>
                    <Image
                      style={page.prfImgAccent}
                      source={require('../assets/cartoon-5.png')}
                    />
                    <View style={page.prfSliderText}>
                      <Text style={page.prfSliderTextUpper}>Average montly savings</Text>
                      <Text style={page.prfSliderTextUnder}>418.620</Text>
                    </View>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={[page.prfSlider, {backgroundColor: '#76D0D0',}]}>
                    <Image
                      style={page.prfImgAccent}
                      source={require('../assets/cartoon-3.png')}
                    />
                    <View style={page.prfSliderText}>
                      <Text style={page.prfSliderTextUpper}>Your daily promo usage</Text>
                      <Text style={page.prfSliderTextUnder}>2</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </ScrollView>
            </View> 
          {/* ini slider dibawah header, u see it - - - END */}

          {/* Things got cursed, forgotten within days, i'll be the one who remember. dear old things, time for replace */}
          {/* Account setting Section */}
            <View style={[page.accountSetting, {marginTop: 25,}]}>
              {/* Account setting Title */}
              <View style={page.accountSettingTitle}>
                <Text style={page.accountSettingTitleBold}>Account Setting</Text>
              </View>
              {/* Account setting Content */}
                <View style={page.listWp}>
                  <DefaultList 
                    name="My Payment Method"
                  />
                  <DefaultList 
                    name="Personal Information"
                  />
                  <DefaultList 
                    name="My Rewards"
                  />
                  <DefaultList 
                    name="Benefits"
                  />
                </View>
                {/* Voucher ceritanya */}
                  <TouchableOpacity 
                    style={page.voucher}
                    onPress={() => this.props.navigation.navigate('MyPoints')}>
                    <View style={page.voucherText}>
                      <Text style={page.voucherTextUpper}>Get more discount !</Text>
                      <Text style={page.voucherTextUnder}>Reedem <Text style={page.TextBold}>Rewards</Text></Text>
                    </View>
                    <Image
                      style={page.voucherImgAccent}
                      source={require('../assets/icon-voucher.png')}
                    />
                  </TouchableOpacity>
                {/*  - - - END */}
              {/* Account setting Content- - - END */}
            </View>
          {/* Account setting Section - - -  END */}

        </ScrollView>
      </View>
    );
  }
};

export default Home2;

const page = StyleSheet.create({

  // BRAND BRAND NEW SHIT
  TextBig: {
    fontSize: 32,
  },
  TextSmall: {
    fontSize: 12,
  },
  TextFzMedium: {
    fontSize: 22,
  },
  TextWhite: {
    color: '#fff',
  },
  TextBlack: {
    color: black,
  },
  TextBold: {
    fontWeight: '700',
    fontFamily: 'CeraPro-Bold',
  },
  TextFwMedium: {
    fontWeight: '400',
    fontFamily: 'CeraPro-Regular',
  },
  TextLight: {
    fontWeight: '300',
    fontFamily: 'CeraPro-Light',
  },
  TextCapitalize: {
    textTransform: 'capitalize',
  },

  // H E A D E R  B A B E
  PrfHeader: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: 100,
    marginBottom: 50,
    paddingHorizontal: 35,
  },
  PrfHeaderLeft: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  PrfHeaderName: {
    fontSize: 25,
    fontFamily: 'CeraPro-Bold',
    color: black,
    fontWeight: '700',
    marginBottom: 8,
  },
  PrfHeaderMembership: {
    fontSize: 12,
    fontFamily: 'CeraPro-Regular',
    color: black,
    fontWeight: '400',
    letterSpacing: 2,
  },
  PrfHeaderRight: {
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  PrfHeaderProfile: {
    backgroundColor: 'rgba(255,255,255,.8)',
    height: 85,
    width: 85,
    borderRadius: 75,
    alignItems: 'center',
    justifyContent: 'center',  
  },
  profilePicture: {
    height: 75,
    width: 75,
    borderRadius: 55,
    alignItems: 'center',
  },
  verifiedIcon: {
    position: 'absolute',
    right: -12,
    alignItems: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    width: 35,
    height: 35,
    borderRadius: 35,
    backgroundColor: '#4FA7F2',
    borderWidth: 5,
    borderColor: 'rgba(255,255,255,.4)',
  },
  vrfIcon: {
    height: 10,
    width: 15,
  },
  // H E A D E R  B A B E - - - END
  // S L I D E R  D I  B A W A H  H E A D E R
  slideFirst: {
    marginLeft: 35,
  },
  prfSlider: {
    width: 165,
    height: 200,
    marginRight: 20,
    borderRadius: 16,
    overflow: 'hidden',
    backgroundColor: '#BC7BB7',
    position: 'relative',
    padding: 25,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.2,
    shadowRadius: 6,
    elevation: 10,
    marginBottom: 15,
  },
  prfImgAccent: {
    position: 'absolute',
    width: 200,
    height: 220,
    right: 0,
    bottom: 0,
    // alignItems: 'flex-end',
  },
  prfSliderText: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    width: '95%',
  },
  prfSliderTextUpper: {
    fontSize: 14,
    fontFamily: 'CeraPro-Regular',
    color: '#fff',
    // fontWeight: '400',
    lineHeight: 18,
  },
  prfSliderTextUnder: {
    fontSize: 24,
    fontFamily: 'CeraPro-Bold',
    color: '#fff',
    // fontWeight: '700',
  },
  // S L I D E R  D I  B A W A H  H E A D E R - - - END
  // T E X T  D I V I D E R 
  TextDivider: {
    marginTop: 25,
    height: 30,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
    width: '100%',
    paddingHorizontal: 35,
  },
  TextLeft: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  TextRight: {
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
  },
  seeAll: {
    color: '#6E72D6',
    fontSize: 15,
  },
  // T E X T  D I V I D E R - - - END
  // H I S T O R Y  I T E M
  HistoryWrapper: {
    paddingHorizontal: 35,
    marginVertical: 35,
  },
  TrItem: {
    backgroundColor: lightGrey,
    paddingVertical: 20,
    borderRadius: 16,
    position: 'relative',
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: 25,
  },
  // T O D A Y  R E C E I P T  U P P E R
  TrUpper: {
    position: 'relative',
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    height: 45,
    paddingHorizontal: 20,
  },
  TrIcon: {
    position: 'relative',
    height: 45,
    width: 45,
    borderRadius: 50,
    overflow: 'hidden',
    marginRight: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  TrIconImg: {
    position: 'absolute',
    height: 45,
    width: 45,
  },
  TrTitle: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  storeName: {
    fontSize: 18,
    fontFamily: 'CeraPro-Bold',
    color: black,
    fontWeight: '700',
  },
  storeLocation: {
    fontSize: 12,
    fontFamily: 'CeraPro-Regular',
    color: grey,
    fontWeight: '400',
  },
  TrPrice: {
    marginLeft: 'auto',
  },
  storeTotalPrice: {
    fontSize: 20,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
    fontWeight: '700',
  },
  // T O D A Y  R E C E I P T  U P P E R - - - E N D
  // H I S T O R Y  I T E M - - - END
  // A C C O U N T  S E T T I N G
  accountSetting: {
    marginTop: 0,
    position: 'relative',
    // backgroundColor: '#798191',
    backgroundColor: primary2,
    width: wp('100%'),
    // height: 350,
    borderTopLeftRadius: 35,
    borderTopRightRadius: 35,
    paddingHorizontal: 23,
    paddingVertical: 35,
  },
  accountSettingTitle: {
    flexDirection: 'row',
  },
  accountSettingTitleBold: {
    fontSize: 22,
    color: '#fff',
    fontFamily: 'CeraPro-Bold',
    marginLeft: 5,
    fontWeight: '700',
  },
  listWp: {
    marginVertical: 25,
  },
  // A C C O U N T  S E T T I N G - - - END
  // V O U C H E R  I N  A C C O U N T  S E T T I N G
  voucher: {
    width: '100%',
    marginVertical: 0,
    paddingHorizontal: 25,
    paddingVertical: 20,
    borderRadius: 16,
    backgroundColor: '#fff',
    overflow: 'hidden',
  },
  voucherText: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  voucherTextUpper: {
    fontSize: 13,
    color: black,
    fontFamily: 'CeraPro-Regular',
    fontWeight: '400',
  },
  voucherTextUnder: {
    fontSize: 21,
    color: black,
    fontFamily: 'CeraPro-Regular',
    fontWeight: '400',
  },
  voucherImgAccent: {
    position: 'absolute',
    height: 150,
    width: 150,
    bottom: -25,
    right: -25,
  },
  // V O U C H E R  I N  A C C O U N T  S E T T I N G - -  END
  // END

  spanRecipt: {
    position: 'absolute',
    right: 0,
    top: 0,
    borderRadius: 50,
    height: 25,
    width: 25,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#D33E3E',
    borderWidth: 2,
    borderColor: '#FFFFFF',
  },
  spanReciptText: {
    color: '#ffffff',
    fontSize: 10,
    fontWeight: '400',
    fontFamily: 'CeraPro-Bold',
    alignItems: 'center',
    justifyContent: 'center',
  },

  pointCard: {
    backgroundColor: '#fff',
    borderRadius: 16,
    width: '45%',
    height: 175, 
    overflow: 'hidden',
  },
  pointVertical: {
    position: 'absolute',
    bottom: 15,
    left: 15,
  },
  pointTitle: {
    fontSize: 27,
    fontWeight: '700',
    fontFamily: 'CeraPro-Bold',
    color: '#4977BB',
  },
  pointSubtitle: {
    fontSize: 14,
    fontWeight: '400',
    fontFamily: 'CeraPro-Regular',
    color: '#798191',
  },

  mini: {
    fontSize: normalize(12),
  },
  small: {
    fontSize: normalize(15),
  },
  medium: {
    fontSize: normalize(17),
  },
  large: {
    fontSize: normalize(20),
  },
  xlarge: {
    fontSize: normalize(25),
  },

  cardWp: {
    marginBottom: 190,
  },
  cardBg: {
    width: '100%',
    position: 'relative',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.2,
    shadowRadius: 6,  
    alignItems: 'center',
    justifyContent: 'center',
    height: 190,
    overflow: 'visible',
    margin: 5,
    borderRadius: 16,
    elevation: 8,
  },
  qrIcon: {
    height: 100,
    width: 100,
  },


  container: {
    backgroundColor: '#fff',
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  mcGrid: {
    margin: 0,
    padding: 0,
    zIndex: 0,
    position: "relative",
  },
  mcRow: {
    marginVertical: 0,
    marginHorizontal: 18,
    flexDirection: 'row',
  },
  fullWidth: {
    marginHorizontal: 0,
  },
  firstRow: {
    marginTop: hp('5%'),
  },
  mt15: {
    marginVertical: 15,
  },
  mt25: {
    marginVertical: 25,
  },
  mcCol1: {
    flex: 1,
  },
  mcCol2: {
    flex: 2,
  },
  mcCol3: {
    flex: 3,
  },
  mcCol4: {
    flex: 4,
  },
  mcCol5: {
    flex: 5,
  },
  mcCol6: {
    flex: 6,
  },
  block: {
    backgroundColor: '#dddddd',
    borderRadius: 8,
    height: 50,
    margin: 5,
  },

  gradientBg: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: hp('30%'),
  },

  radiusUpper: {
    backgroundColor: '#F5FCFF',
    position: 'absolute',
    left: 0,
    top: hp('26%'),
    width: '100%',
    height: 35,
    borderTopRightRadius: 16,
    borderTopLeftRadius: 16,
  },

  sliderBox: {
    position: 'relative',
    zIndex: 9,
    elevation: 5,
  },

  buttonIcon: {
    height: 85,
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F6F6F6',
  },
  buttonText: {
    marginTop: 8,
    color: '#2A3348',
    fontFamily: 'CeraPro-Regular',
  },

  // userBalance - - - H E R E 

  // topNavigation - - - H E R E
  topNavigation: {
    height: 50,
    marginVertical: 6,
    position: 'relative',
    zIndex: 9,
    marginHorizontal: 25,
  },
  backButton: {
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  leftIcons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  topNavTitle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  pageTitle: {
    fontSize: 22,
    color: '#2A3348',
    fontWeight: '700',
    paddingBottom: 8,
  },

  mapWrapper: {
    height: ScreenHeight,
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1
  },

  // pageWrapper - - - H E R E
  pageWrapper: {
    marginTop: -65,
    paddingTop: 65,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 25,
    paddingHorizontal: 35,
  },
  pageHeader: {
    backgroundColor: 'rgba(255,255,255,.6)',
    padding: 15,
    position: 'relative',
    height: 85,
    marginTop: 15,
    borderTopLeftRadius: 14,
    borderTopRightRadius: 14,
  },
  headerTitle: {
    fontSize: 29,
    color: '#2A3348',
    fontWeight: '700',
    flex: 1,
  },
  headerSubtitle: {
    fontSize: 13,
    color: '#2A3348',
  },
  headerImage: {
    position: 'absolute',
    right: 35,
    top: -15,
    width: 65,
    height: 65,
    backgroundColor: 'rgba(255,255,255,.8)',
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pageContent: {
    padding: 15,
    backgroundColor: '#ffffff',
    borderBottomLeftRadius: 14,
    borderBottomRightRadius: 14,
    marginBottom: 100,
  },
  // mcFooter: {
  //   width: '100%',
  //   height: 75,
  // },
  mb75: {
    marginBottom: 75,
  },

  // Header 
  header: {
    flexDirection: 'column',
    paddingHorizontal: 15,
    paddingTop: hp('5%'),
    paddingBottom: 35,
  },
  headerText: {
    fontSize: 25,
    color: '#2A3348',
    fontFamily: 'CeraPro-Regular',
  },
  headerName: {
    fontSize: 25,
    color: '#2A3348',
    fontFamily: 'CeraPro-Bold',
  },
  // Slider Menu 
  sliderMenuWp: {
    marginTop: 35,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 23,
  },
  sliderMenu: {
    height: 120, 
    width: '28%', 
    marginTop: 5,
    marginBottom: 15,
    backgroundColor: '#8293DD',
    position: 'relative',
    overflow: 'hidden',
    borderRadius: 16,
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 12 },
    shadowOpacity: 0.3,
    shadowRadius: 15,  
    elevation: 6,
  },
  sliderText: {
    position: 'absolute',
    left: 15,
    top: 15,
    flexDirection: 'row',
  },
  TextVertical: {
    flexDirection: 'column'
  },
  spanBold: {
    fontSize: 17,
    color: '#fff',
    fontFamily: 'CeraPro-Bold',
    marginRight: 5,
  },
  spanLight: {
    fontFamily: 'CeraPro-Regular',
    fontSize: 17,
    color: '#fff',
  },
  sliderIcon: {
    height: 125,
    width: 95,
    position: 'absolute',
    right: 0,
    bottom: 0,
    zIndex: 0,
  },
  titleLight: {
    fontFamily: 'CeraPro-Regular',
    fontSize: 22,
    color: '#fff',
  },
  overviewContent: {
    position: 'relative',
    marginTop: 35,
    marginBottom: 35,  
  },
  overviewCardRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: hp('4%'),
  },
  overviewCard: {
    width: '45%',
    padding: 20,
    paddingBottom: 15,
    backgroundColor: '#fff',
    borderRadius: 16,
    shadowColor: '#879BBC',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.2,
    shadowRadius: 6,  
    elevation: 6,
  },
  overviewIcon: {
    width: 50,
    height: 50,
    marginBottom: 20,
  },
  overviewText: {
    fontFamily: 'CeraPro-Regular',
    fontSize: 17,
    color: '#798191',
  },
  overviewAmount: {
    fontFamily: 'CeraPro-Bold',
    fontSize: 27,
    color: '#2A3348',
  },
  // award wrapper
  megaAward: {
    marginTop: -35,
    position: 'relative',
    backgroundColor: '#fff',
    width: wp('100%'),
    // height: 350,
    borderTopLeftRadius: 35,
    borderTopRightRadius: 35,
    paddingVertical: 35,
  },
  megaAwardTitle: {
    flexDirection: 'row',
    paddingHorizontal: 23,
  },
  megaAwardtitleBold: {
    fontSize: 22,
    color: '#2A3348',
    fontFamily: 'CeraPro-Bold',
    marginLeft: 5,
  },
  megaAwardtitleLight: {
    fontFamily: 'CeraPro-Regular',
    fontSize: 22,
    color: '#2A3348',
  },
  // Award Slider

  sliderAwards: {
    flexDirection: 'row',
    height: 250,
    marginTop: 25,
    width: '100%',
  },
  slideAward: {
    width: wp('75%'),
    height: 236,
    marginLeft: 23,
    // backgroundColor: '#ddd',
    borderRadius: 16,
    // shadowColor: '#879BBC',
    // shadowOffset: { width: 0, height: 4 },
    // shadowOpacity: 0.2,
    // shadowRadius: 6,  
    // elevation: 6,
  },
  AwardImageWp: {
    width: '99%',
    height: 120,
    borderRadius: 16,
    overflow: 'hidden',
    position: 'absolute',
    zIndex: 9,
    left: 0,
    top: 0,
    elevation: 1,
    marginLeft: 4,
  },
  AwardImage: {
    width: '100%',
    height: '100%',
  },
  awardContent: {
    backgroundColor: Platform.OS === 'android' ? '#F6F6F6' : '#fff',
    marginLeft: 4,
    borderRadius: 16,
    paddingHorizontal: 15,
    paddingVertical: 25,
    paddingBottom: 10,
    borderRadius: 16,
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.2,
    shadowRadius: 6,
    marginTop: 110,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    elevation: 0,
    position: 'relative',
    zIndex: 0,
  },
  awardContentText: {
    fontSize: 16,
    color: '#2A3348',
    fontFamily: 'CeraPro-Bold',
    marginBottom: 10,
    lineHeight: Platform.OS === 'android' ? 17 : 15,
  },
  pointAward: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pointIcon: {
    height: 18,
    width: 18,
    marginRight: 8,
  },
  pointText: {
    fontSize: 18,
    color: '#DD5565',
    fontFamily: 'CeraPro-Bold',
  },
  awardButton: {
    position: 'absolute',
    right: -1,
    bottom: 0,
    backgroundColor: '#50D6B6',
    borderTopLeftRadius: 50,
    borderBottomLeftRadius: 50,
    borderBottomRightRadius: Platform.OS === 'android' ? 50 : 25,
    paddingHorizontal: 25,
    paddingVertical: 10,
    zIndex: 9,
    shadowColor: '#50D6B6',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.2,
    shadowRadius: 6,  
    // elevation: 7,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  awardButtonText: {
    color: '#fff',
    fontSize: 18,
    fontFamily: 'CeraPro-Bold',
  },
  // variation 2
  AwardVideoWp: {
    height: 216,
    zIndex: 0,
    elevation: 0,
    opacity: 0.8,
  },
  AwardVideoContent: {
    backgroundColor: 'rgba(0,0,0,0)',
    position: 'absolute',
    zIndex: 9,
    elevation: 0,
  },
  awardVideoContentText: {
    color: '#fff',
    opacity: 1,
  }
});