import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , Modal
  , TouchableOpacity
  , SafeAreaView } from 'react-native';

import BoxAccent from '../components/BoxAccent';
import Button from '../components/Button';
import PromoBox2 from '../components/PromoBox2';
import PromoBox3 from '../components/PromoBox3';
import MerchantSlider from '../components/MerchantSlider';
import TextDivider from '../components/TextDivider';

import { 
  styles
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey
  , fonts } from '../assets/style';

class StorePage extends React.Component {
  static navigationOptions = {
    header: null
  }
  render() {
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fff'}}>
        <View style={styles.mcGrid}>
          <View style={[styles.mcRow, styles.topNavigation]}>
            <TouchableOpacity 
              style={[styles.mcCol1, styles.backButton]}
              onPress={() => this.props.navigation.goBack()}>
              <Image
                style={{width: 35, height: 35}}
                source={require('../assets/icon-chevron-left.png')}
              />
            </TouchableOpacity>
            <View style={[styles.mcCol4, styles.topNavTitle]}>
              <Text style={styles.pageTitle}></Text>
            </View>
            <View style={[styles.mcCol3, styles.leftIcons]}>
              
            </View>
          </View>
          <ScrollView style={[styles.mcGrid, page.pageWrapper]}>
            <BoxAccent
              yAxis={0}
              xAxis={300}
              width={200}
              height={200}
            ></BoxAccent>
            <View style={page.imageHeaderWp}>
              <Image
                style={page.imageHeader}
                source={require('../assets/mall-senayan-city.jpeg')}
              />
            </View>

            <View style={[styles.mcRow, page.firstRow, page.center, page.header]}>
              <Text style={page.headerTitle}>
                Senayan City
              </Text>
              <View style={page.headerInfo}>
                <View style={page.infoItem}>
                  <Image
                    style={{width: 12, height: 16}}
                    source={require('../assets/icon-location-mark.png')}
                  />
                  <Text style={page.subtitle}><Text style={styles.bold}>67</Text> merchant</Text>
                </View>
                <View style={page.infoItem}>
                  <Image
                    style={{width: 12, height: 16}}
                    source={require('../assets/icon-location-mark.png')}
                  />
                  <Text style={page.subtitle}><Text style={styles.bold}>67</Text> merchant</Text>
                </View>
              </View>
            </View>

            <View style={[styles.sliderDefault]}>
              <ScrollView 
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              >
                <TouchableOpacity 
                  onPress={
                    () => this.props.navigation.navigate('PromoDetail', { Component: { options: { bottomTabs: { visible: false, drawBehind: true, animate: true } } } })
                  }>
                  <PromoBox2 
                    title="Buy 1 get 1 FREE!"
                    span="Hot Deals!"
                    image={require('../assets/promo-2-1.png')}
                  />
                </TouchableOpacity>
                <TouchableOpacity 
                  onPress={() => this.props.navigation.navigate('PromoDetail')}>
                  <PromoBox2 
                    title="Buy 2 get 1 FREE!"
                    span="Hot Deals!"
                    image={require('../assets/promo-2-2.png')}
                  />
                </TouchableOpacity>
                <TouchableOpacity 
                  onPress={() => this.props.navigation.navigate('PromoDetail')}>
                  <PromoBox2 
                    title="Buy 3 get 1 FREE!"
                    span="Hot Deals!"
                    image={require('../assets/promo-2-1.png')}
                    lastSlide
                  />
                </TouchableOpacity>
              </ScrollView>
            </View>

            <TextDivider 
              upper=""
              under="Our Merchant"
            />

            <View style={[styles.sliderDefault]}>
              <ScrollView 
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              >
                <MerchantSlider
                  image={require('../assets/merchant-chatime.jpg')} 
                  text='Chatime'
                />
                <MerchantSlider
                  image={require('../assets/merchant-nike.jpg')} 
                  text='Nike'
                />
                <MerchantSlider
                  image={require('../assets/merchant-chatime.jpg')} 
                  text='Chatime'
                />
                <MerchantSlider
                  image={require('../assets/merchant-chatime.jpg')} 
                  text='Chatime'
                />
                <MerchantSlider
                  image={require('../assets/merchant-chatime.jpg')} 
                  text='Chatime'
                />
                <MerchantSlider
                  image={require('../assets/merchant-chatime.jpg')} 
                  text='Chatime'
                />
                <MerchantSlider
                  image={require('../assets/merchant-chatime.jpg')} 
                  text='Chatime'
                />
                <MerchantSlider
                  image={require('../assets/merchant-chatime.jpg')} 
                  text='Chatime'
                />
                <MerchantSlider
                  image={require('../assets/merchant-chatime.jpg')} 
                  text='Chatime'
                  lastSlide
                />
              </ScrollView>
            </View>

            <TextDivider 
              upper=""
              under="Prefered Promos"
            />

            <View style={[styles.sliderBox, page.lastRow]}>
              <ScrollView 
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              >
                <PromoBox3 
                  title="Unilever Mega sale WEEK!"
                  subtitle="Grand Indonesia"
                  span="0.1 KM"
                />
                <PromoBox3 
                  title="Unilever Mega sale WEEK!"
                  subtitle="Grand Indonesia"
                  span="0.1 KM"
                />
                <PromoBox3 
                  title="Unilever Mega sale WEEK!"
                  subtitle="Grand Indonesia"
                  span="0.1 KM"
                />
                <PromoBox3 
                  title="Unilever Mega sale WEEK!"
                  subtitle="Grand Indonesia"
                  span="0.1 KM"
                />
                <PromoBox3 
                  title="Unilever Mega sale WEEK!"
                  subtitle="Grand Indonesia"
                  span="0.1 KM"
                />
                <PromoBox3 
                  title="Unilever Mega sale WEEK!"
                  subtitle="Grand Indonesia"
                  span="0.1 KM"
                  lastSlide
                />
              </ScrollView>
          </View>

          </ScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

export default StorePage;

const page = StyleSheet.create({

  firstRow: {
    marginTop: 125,
  },
  lastRow: {
    marginBottom: 75,
  },
  center: {
    paddingHorizontal: 35,
  },

  // pageWrapper
  pageWrapper: {
    marginTop: -65,
    position: 'relative',
    zIndex: 1,
  },
  imageHeaderWp: {
    width: 300,
    height: 300,
    borderRadius: 52,
    position: 'absolute',
    transform: [{ rotate: '45deg'}],
    overflow: 'hidden',
    top: -175,
    right: -75,
    backgroundColor: '#CEFFF9',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  imageHeader: {
    transform: [{ rotate: '-45deg' }],
    position: 'absolute',
    width: 350,
    height: 350,
    bottom: -150,
    right: -125,
  },
  header: {
    flexDirection: 'column',
    marginBottom: 55,
  },
  headerTitle: {
    fontSize: 28,
    fontFamily: 'CeraPro-Bold',
    color: black,
  },
  headerInfo: {
    flexDirection: 'row',
  },
  infoItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginRight: 8,
  },
  subtitle: {
    marginLeft: 5,
    fontSize: 15,
    color: '#798191',
  },


  
});