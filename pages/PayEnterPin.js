import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , Modal
  , TouchableOpacity 
  , TextInput
  , SafeAreaView } from 'react-native';
import { 
  createBottomTabNavigator
  , createAppContainer
  , createStackNavigator } from 'react-navigation';

import { 
  styles
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

import PromoBoxNearby from '../components/PromoBoxNearby';
import BoxAccent from '../components/BoxAccent';

import PinView from 'react-native-pin-view'

let ScreenHeight = Dimensions.get("window").height;

class PayEnterPin extends React.Component {
  static navigationOptions = {
    header: null
  }
  
  constructor(props) {
    super(props);
    this.onFailure = this.onFailure.bind(this);
    this.onSuccess = this.onSuccess.bind(this);
  }
  onFailure() {
    alert('FAILURE')
  }
  onSuccess() {
    alert('SUCCESS')
  }

  render() {

    return (
        <SafeAreaView style={{ position: 'relative', zIndex: 99 }}>
          <View style={[styles.mcGrid, page.top]}>
            {/* Top Navigation */}
            <View style={[styles.mcRow, styles.topNavigation]}>
              <TouchableOpacity 
                style={[styles.mcCol1, styles.backButton]}
                onPress={() => this.props.navigation.goBack()}>
                <Image
                  style={{width: 35, height: 35}}
                  source={require('../assets/icon-chevron-left.png')}
                />
              </TouchableOpacity>
              <View style={[styles.mcCol4, styles.topNavTitle]}>
                <Text style={styles.pageTitle}></Text>
              </View>
              <View style={[styles.mcCol3, styles.leftIcons]}>
                
              </View>
            </View>
            {/* Top Navigation - - - END */}
          </View>
          <ScrollView style={[styles.mcGrid, page.pageWrapper]}>
            
            <PinView
              onSuccess={ () => this.props.navigation.navigate('History') }
              onFailure={ this.onFailure }
              password={ [1, 5, 7, 6, 5, 9] }
              onComplete={ () => this.props.navigation.navigate('PayQrSuccess', {transaction_total:this.props.navigation.state.params.transaction_total}) }
              pinLength={6}
            />

          </ScrollView>
        </SafeAreaView>
    );
  }
};

export default PayEnterPin;

const page = StyleSheet.create({

  top: {
    zIndex: 3,
  },

  pageWrapper: {
    marginTop: -65,
    paddingTop: 175,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 250,
    paddingHorizontal: 0,
    marginBottom: 50,
  },
  center: {
    paddingHorizontal: 30,
    marginHorizontal: 0,
  },
  header: {
    height: 75,
    alignItems: 'center',
    marginTop: 85,
    paddingBottom: 0,
  },
  headerTitle: {
    fontSize: 27,
    fontFamily: 'CeraPro-Bold',
    color: black,
  },
  headerSubtitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  subtitle: {
    fontSize: 16,
    fontFamily: 'CeraPro-Regular',
    color: '#ffffff',
    marginLeft: 8,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: 0, height: 1},
    textShadowRadius: 1,
    letterSpacing: .8,
  },
  searchButtonWp: {
    justifyContent: 'flex-end',
  },
  searchButton: {
    flexDirection: 'row',
    height: 65,
    alignItems: 'center',
    backgroundColor: primary,
    paddingHorizontal: 15,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  text: {
    color: '#ffffff',
    fontSize: 16,
    marginLeft: 8,
  },
  // - - - -
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 12,
    backgroundColor: lightGrey,
    paddingHorizontal: 15,
    marginVertical: 15,
  },
  searchInput: {
    flex: 1,
    marginLeft: 15,
    paddingVertical: Platform.OS === 'ios' ? 15 : 15, 
  },
  // - - - -
  optionButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  // - - - -
  historyWrapper: {
    flexDirection: 'column',
    marginVertical: 25,
    position: 'relative',
    zIndex: 9,
  },
  dateDivider: {
    marginVertical: 15,
  },
  textDivider:{
    fontSize: 16,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
    textTransform: 'uppercase',
    letterSpacing: 2,
    marginBottom: 15,
  },
  historyItem: {
    width: '100%',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 8,
    backgroundColor: lightGrey,
    flexDirection: 'row',
    marginBottom: 15,
  },
  historyTitle: {
    fontSize: 16,
    color: black,
    fontFamily: 'CeraPro-Bold',
    marginBottom: 2,
  },
  historySubtitle: {
    textTransform: 'capitalize',
    fontSize: 13,
    color: darkGrey,
  },
  historyPrice: {
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    flex: 6,
    paddingHorizontal: 0,
    marginHorizontal: 0,
  },
  historyPriceText: {
    textAlign: 'right',
    fontSize: 16,
    color: primary2,
    fontFamily: 'CeraPro-Bold',
  }

});