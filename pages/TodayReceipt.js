import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , TouchableOpacity 
  , TextInput
  , SafeAreaView } from 'react-native';

  import { createAppContainer } from 'react-navigation';
  import { createStackNavigator } from 'react-navigation-stack';
  import { createBottomTabNavigator } from 'react-navigation-tabs';
import Modal from "react-native-modal";
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

import { 
  styles
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

import PromoBoxNearby from '../components/PromoBoxNearby';
import BoxAccent from '../components/BoxAccent';
import Button from '../components/Button';
import SkeletonPlaceholder from "react-native-skeleton-placeholder";
import NumberFormat from 'react-number-format';
import moment from 'moment';


let ScreenHeight = Dimensions.get("window").height;




class TodayReceipt extends React.Component {
  static navigationOptions = {
    header: null
  }
  
  state = {
    search: '',
    historyList: [],
    loading: true,
  };

  updateSearch = search => {
    this.setState({ search });
  };



  
  // Load Data to retrieve in page
  async componentDidMount(){
    try {
      let response = await fetch(
        'http://103.3.62.188/osmosis/openDataToday_',
      );
      let responseJson = await response.json();
      
      console.log(responseJson)
        
      this.setState({historyList: responseJson, loading: false});
    }catch (error) {
      console.error("errorrr");
      console.error(error);

    }
    console.log("component mount!");
  }

  // formatDate(date) {
  //   var monthNames = [
  //     "January",
  //     "February",
  //     "March",
  //     "April",
  //     "May",
  //     "June",
  //     "July",
  //     "August",
  //     "September",
  //     "October",
  //     "November",
  //     "December"
  //   ];
  
  //   var day         = date.getDate();
  //   var monthIndex  = date.getMonth();
  //   var year        = date.getFullYear();
  
  //   return day + ' ' + monthNames[monthIndex] + ' ' + year;
  // }
  
  // formatDate(date){
  //   var now       =  moment(new Date());
  //   var now       = moment("2019-08-19");
  //   var duration  = moment.duration(now.diff(date))
  //   var hours     = duration.asHours();
    
  //   console.log("jam_")
  //   console.log(date+" ------ "+hours)
  //   return hours+" hours ago";
  // }

  renderHistoryList() {
    let data = this.state.historyList;
    
    console.log(data)

    return (
            <View style={[styles.mcRow, page.center, page.historyWrapper]}>
              { data.map((item, key)=>(
                <TouchableOpacity style={page.TrItem} key={key} onPress={() => this.props.navigation.navigate('PayReceipt2', { receipt_id:item.id, source: 'today' })}> 
                    

                    <View style={page.TrUpper}>
                      <View style={page.TrIcon}>
                        <Image
                          style={page.TrIconImg}
                          source={require('../assets/brand-piko-mart.png')}
                        />
                      </View>
                      <View style={page.TrTitle}>
                        <Text style={page.storeName}>{item.store_name}</Text>
                        <Text style={page.storeLocation}>Bekasi Store</Text>
                      </View>
                    </View>
                    {/* info : accent doang ini */}
                    <View style={page.TrMiddle}>
                      <View style={page.circleWhiteLeft}></View>
                      <Text numberOfLines={1} style={[page.dashedLine, {textAlign: 'left'}]}>- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -</Text>
                      <View style={page.circleWhiteRight}></View>
                    </View>
                    {/* info : bagian bawah, ada date, time and amount */}
                    <View style={page.TrUnder}>
                      {/* <View style={page.TrUnderInfo}>
                        <Text style={page.infoAbove}>Date</Text>
                        <Text style={page.infoBelow}>{this.formatDate(item.transaction_date)}</Text>
                      </View> */}
                      <View style={page.TrUnderInfo}>
                        <Text style={page.infoAbove}>Time</Text>
                        <Text style={page.infoBelow}>{moment(item.transaction_date).format("hh:mm")}</Text>
                      </View>
                      <View style={page.TrUnderInfo}>
                        <Text style={page.infoAbove}>Amount</Text>
                        <Text style={page.infoBelow}><NumberFormat value={item.transaction_total} displayType={'text'} thousandSeparator={true} prefix={' '} renderText={value => <Text>{value}</Text>}/></Text>
                      </View>
                    </View>
                </TouchableOpacity>
              ))} 
          </View>
          );
  }


  render() {
    const { search } = this.state;

    const mapMarker = {
      position: 'absolute',
      height: 55,
      width: 55,
    };
    

    if(this.state.loading == true){
      return (
        <SkeletonPlaceholder>
          <View style={{ width: "100%", height: 140 }} />
          <View
            style={{
              width: 100,
              height: 100,
              borderRadius: 100,
              borderWidth: 5,
              borderColor: "white",
              alignSelf: "center",
              position: "relative",
              top: -50
            }}
          />
          <View style={{ width: 120, height: 20, alignSelf: "center" }} />
          <View
            style={{
              width: 240,
              height: 20,
              alignSelf: "center",
              marginTop: 12
            }}
          />
        </SkeletonPlaceholder>
      );
    }else{
      return (
        <SafeAreaView style={{ position: 'relative', zIndex: 99 }}>
          <View style={[styles.mcGrid, page.top]}>

            <View style={[styles.mcRow, styles.topNavigation]}>
              <TouchableOpacity 
                style={[styles.mcCol1, styles.backButton]}
                onPress={() => this.props.navigation.goBack()}>
                <Image
                  style={{width: 35, height: 35}}
                  source={require('../assets/icon-chevron-left.png')}
                />
              </TouchableOpacity>
              <View style={[styles.mcCol4, styles.topNavTitle]}>
                <Text style={styles.pageTitle}></Text>
              </View>
              <View style={[styles.mcCol3, styles.leftIcons]}>
                
              </View>
            </View>

          </View>
          <ScrollView style={[styles.mcGrid, page.pageWrapper]}>
            <BoxAccent
              yAxis={-25}
              xAxis={325}
              width={200}
              height={200}
            ></BoxAccent>
            <BoxAccent
              yAxis={-200}
              xAxis={-100}
              width={300}
              height={300}
            ></BoxAccent>

            <View style={[styles.mcRow, page.center, page.header]}>
              <View style={styles.mcCol6}>
                <Text style={page.headerTitle}>Todays receipt</Text>
              </View>
            </View>

            {/* TODAYS RECEIPT GENERATE HERE */}
            {this.renderHistoryList()}

            {/* info : button button? */}
            <View style={[styles.mcRow, page.buttonWp]}>
              <TouchableOpacity 
                onPress={() => this.props.navigation.navigate('History')}
                style={page.button}>
                <Text style={page.buttonText}>View Shopping History</Text>
              </TouchableOpacity>
            </View>
          {/* info - - - END */}

          </ScrollView>

        </SafeAreaView>
      );
    }
  }
};

export default TodayReceipt;

const page = StyleSheet.create({

  // N E W  C A R D  T I C K E T  S T Y L E
  TrItem: {
    backgroundColor: lightGrey,
    paddingVertical: 15,
    borderRadius: 16,
    position: 'relative',
    flexDirection: 'column',
    alignItems: 'center',
    marginBottom: 25,
  },
  // T O D A Y  R E C E I P T  U P P E R
  TrUpper: {
    position: 'relative',
    width: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexDirection: 'row',
    height: 45,
    paddingHorizontal: 20,
  },
  TrIcon: {
    position: 'relative',
    height: 45,
    width: 45,
    borderRadius: 50,
    overflow: 'hidden',
    marginRight: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  TrIconImg: {
    position: 'absolute',
    height: 45,
    width: 45,
  },
  TrTitle: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  storeName: {
    fontSize: 18,
    fontFamily: 'CeraPro-Bold',
    color: black,
    fontWeight: '700',
  },
  storeLocation: {
    fontSize: 12,
    fontFamily: 'CeraPro-Bold',
    color: grey,
    fontWeight: '400',
  },
  // T O D A Y  R E C E I P T  U P P E R - - - E N D
  // T O D A Y  R E C E I P T  U N D E R
  TrUnder: {
    position: 'relative',
    marginTop: 35,
    width: '100%',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    flexDirection: 'row',
    height: 45,
    paddingHorizontal: 20,
  },
  TrUnderInfo: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  infoAbove: {
    fontSize: 12,
    fontFamily: 'CeraPro-Bold',
    color: grey,
    fontWeight: '400',
  },
  infoBelow: {
    fontSize: 18,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
    fontWeight: '700',
  },
  // T O D A Y  R E C E I P T  U N D E R - - - E N D
  // T O D A Y  R E C E I P T  M I D D L E  A C C E N T
  TrMiddle: {
    // position: 'absolute',
    // left: 0,
    // top: '50%',
    // transform: [{translateY: 50}],
    width: '100%',
    height: 30,
    overflow: 'hidden',
    top: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  circleWhiteLeft: {
    width: 30,
    height: 30,
    borderRadius: 15,
    position: 'absolute',
    left: -15,
    backgroundColor: '#fff',
    zIndex: 99,
  },
  circleWhiteRight: {
    width: 30,
    height: 30,
    borderRadius: 15,
    position: 'absolute',
    right: -15,
    backgroundColor: '#fff',
    zIndex: 99,
  },
  dashedLine: {
    fontSize: 12,
    fontFamily: 'CeraPro-Bold',
    color: grey,
    fontWeight: '300',
    paddingHorizontal: 20,
    width: '100%',
    overflow: 'hidden',
    width: '120%',
  },
  // T O D A Y  R E C E I P T  M I D D L E  A C C E N T - - - E N D
  // I N I  B U T T O N  D A N  W P - N Y A
  buttonWp: {
    alignItems: 'center',
    justifyContent: 'center',
    marginBottom: 100,
  },
  button: {
    width: '80%',
    padding: 20,
    backgroundColor: primary,
    borderRadius: 16,
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 20,
    fontFamily: 'CeraPro-Bold',
    color: '#fff',
    fontWeight: '700',
  },
  // I N I  B U T T O N  D A N  W P - N Y A - - - E N D

  top: {
    zIndex: 3,
  },

  pageWrapper: {
    marginTop: -65,
    paddingTop: 0,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 250,
    paddingHorizontal: 0,
    marginBottom: 50,
  },
  center: {
    paddingHorizontal: 30,
    marginHorizontal: 0,
  },
  header: {
    height: 75,
    alignItems: 'center',
    marginTop: 85,
    paddingBottom: 0,
  },
  headerTitle: {
    fontSize: 27,
    fontFamily: 'CeraPro-Bold',
    color: black,
    fontWeight: '700',
  },
  headerSubtitle: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  subtitle: {
    fontSize: 16,
    fontFamily: 'CeraPro-Regular',
    color: '#ffffff',
    marginLeft: 8,
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: {width: 0, height: 1},
    textShadowRadius: 1,
    letterSpacing: .8,
  },
  searchButtonWp: {
    justifyContent: 'flex-end',
  },
  searchButton: {
    flexDirection: 'row',
    height: 65,
    alignItems: 'center',
    backgroundColor: primary,
    paddingHorizontal: 15,
    borderRadius: 8,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    shadowRadius: 4,
    elevation: 10,
  },
  text: {
    color: '#ffffff',
    fontSize: 16,
    marginLeft: 8,
  },
  // - - - -
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 12,
    backgroundColor: lightGrey,
    paddingHorizontal: 15,
    marginVertical: 15,
  },
  searchInput: {
    flex: 1,
    marginLeft: 15,
    paddingVertical: Platform.OS === 'ios' ? 15 : 15, 
  },
  // - - - -
  optionButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  // - - - -
  historyWrapper: {
    flexDirection: 'column',
    marginVertical: 25,
    position: 'relative',
    zIndex: 9,
  },
  dateDivider: {
    marginVertical: 15,
  },
  textDivider:{
    fontSize: 16,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
    textTransform: 'uppercase',
    letterSpacing: 2,
    marginBottom: 15,
  },
  ReceiptIcon: {
    height: 50,
    width: 50,
    borderRadius: 50,
    overflow: 'hidden',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 15,
  },
  ReceiptItem: {
    width: '100%',
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderRadius: 8,
    backgroundColor: lightGrey,
    flexDirection: 'row',
    marginBottom: 15,
    alignItems: 'center',
  },
  ReceiptTitle: {
    fontSize: 16,
    color: black,
    fontFamily: 'CeraPro-Bold',
    marginBottom: 2,
    width: wp('30%'),
  },
  ReceiptSubtitle: {
    textTransform: 'capitalize',
    fontSize: 13,
    color: darkGrey,
  },
  ReceiptPrice: {
    alignItems: 'flex-end',
    justifyContent: 'flex-start',
    flex: 6,
    paddingHorizontal: 0,
    marginHorizontal: 0,
  },
  ReceiptPriceText: {
    textAlign: 'right',
    fontSize: 16,
    color: primary2,
    fontFamily: 'CeraPro-Bold',
  }

});