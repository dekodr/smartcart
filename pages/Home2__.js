import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , Modal
  , SafeAreaView
  , StatusBar
  , Footer
  , TouchableOpacity } from 'react-native';
  import { createAppContainer } from 'react-navigation';
  import { createStackNavigator } from 'react-navigation-stack';
  import { createBottomTabNavigator } from 'react-navigation-tabs';
// import LinearGradient from 'react-native-linear-gradient';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { normalize } from '../assets/Normalize';
import FlipCard from 'react-native-flip-card'

import Button from '../components/Button';
import UserCard from '../components/UserCard';
import TextDivider from '../components/TextDivider';
import PromoBox2 from '../components/PromoBox2';
import PromoBox1 from '../components/PromoBox1';
import PromoBox3 from '../components/PromoBox3';
import BoxAccent from '../components/BoxAccent';
import BoxAccent2 from '../components/BoxAccent2';
import UserBalance from '../components/UserBalance';
import Carousel from 'react-native-snap-carousel';

import MemberCard from '../components/MemberCard';

import { 
  black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

let ScreenHeight = Dimensions.get("window").height;

class Home2 extends React.Component {
  static navigationOptions = {
    header: null
  }

  // modal visibility
  state = {
    modalVisible: false,
  };

  // set modal visible function
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  };

  constructor(props){
    super(props);

    this.state = {
      entries: [1,2,3,4],
    }
  }

  _onPressCarousel = () => {
    // here handle carousel press
  }

  _renderItem () {
    return (
      <TouchableOpacity 
        style={{ borderRadius: 16 }}
        onPress={
          () => this.props.navigation.navigate('PromoDetail', { Component: { options: { bottomTabs: { visible: false, drawBehind: true, animate: true } } } })
        }>
        <PromoBox2
          image={require('../assets/promo-2-1.png')} 
          title="Buy 1 get 1 FREE!"
          span="Hot Deals!"
          noText
        />
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <StatusBar  
          backgroundColor = "rgba(0,0,0,0)"  
          barStyle = "dark-content"   
          hidden = {false}    
          translucent = {true}  
        />
        
        <ScrollView style={styles.mcGrid}>
          <BoxAccent
            yAxis={110}
            xAxis={-175}
            width={300}
            height={300}
          ></BoxAccent>
          <BoxAccent
            yAxis={-125}
            xAxis={25}
            width={200}
            height={200}
          ></BoxAccent>

          <SafeAreaView style={{marginTop: Platform.OS === 'android' ? 5 : 0}}>
            {/* Top Navigation */}
              <View style={[styles.mcRow, styles.topNavigation, {marginTop: Platform.OS === 'android' ? 50 : 0}]}>
                <TouchableOpacity 
                  style={[styles.mcCol3, styles.backButton]}
                  onPress={() => {this.props.navigation.navigate('Home2'); }}>
                  <View style={styles.logo}>
                    <Image
                      style={{width: 23, height: 18}}
                      source={require('../assets/menu-icon.png')}
                    />
                  </View>
                </TouchableOpacity>
                <View style={[styles.mcCol1, styles.topNavTitle]}>
                  <Image
                    style={{width: 115, height: 25}}
                    source={require('../assets/smartcart-logo.png')}
                  />
                </View>
                <View style={[styles.mcCol3, styles.leftIcons]}>
                  <Image
                      style={{width: 18, height: 18}}
                      source={require('../assets/icon-bell-notification-green.png')}
                    />
                </View>
              </View>
            {/* Top Navigation - - -  END */}
          </SafeAreaView>

          <View style={[styles.mcRow, styles.header, {display: 'none'}]}>
            <Text style={styles.headerText}>Welcome,</Text>
            <Text style={styles.headerName}>Glantino Putra</Text>
          </View>

        {/* Member Card */}
          <View style={[styles.mcRow, {marginTop: 15}]}>

            <FlipCard
              flipHorizontal={true}
              flipVertical={false}
              friction={6}
              perspective={1000}
              flip={false}
            >
              {/* Face Side */}
              <View style={styles.face}>
                <MemberCard 
                  amount="1.820"
                  image={require('../assets/silver-member-bg.png')}
                />
              </View>
              {/* Back Side */}
              <View style={styles.back}>
                <ImageBackground 
                  source={require('../assets/silver-member-bg.png')}
                  style={styles.cardBg}
                  imageStyle={{ borderRadius: 16 }}>
                    <Image
                      style={styles.qrIcon}
                      source={require('../assets/qr-dekodr.png')}
                    />
                </ImageBackground>
              </View>
            </FlipCard>
         
            {/*<View style={styles.mcCol1}>
              <MemberCard 
                amount="1.820"
                image={require('../assets/silver-member-bg.png')}
              />
            </View>*/}

          </View>
        {/* Member Card - - -  END */}

        {/* Button under Card */}
          <View style={[styles.sliderBox, styles.sliderMenuWp]}>
            {/* RECENT RECEIPT */}
            <TouchableOpacity 
              style={styles.sliderMenu}
              onPress={() => this.props.navigation.navigate('TodayReceipt2')}
              >
              <View style={[styles.sliderText, styles.TextVertical]}>
                <Text style={[styles.spanBold, {}]}>Recent</Text>
                <Text style={styles.spanLight}>Receipt</Text>
              </View>
              <Image
                style={styles.sliderIcon}
                source={require('../assets/cartoon-5.png')}
              />
            </TouchableOpacity>
            {/* end of RECENT RECEIPT */}

            <TouchableOpacity 
              style={[styles.sliderMenu, {backgroundColor: '#76D0D0' }]}
              onPress={() => this.props.navigation.navigate('MyPoints')}
              >
              <View style={[styles.sliderText, styles.TextVertical]}>
                <Text style={[styles.spanBold, {}]}>My</Text>
                <Text style={styles.spanLight}>Points</Text>
              </View>
              <Image
                style={styles.sliderIcon}
                source={require('../assets/cartoon-1.png')}
              />
            </TouchableOpacity>
            <TouchableOpacity 
              style={[styles.sliderMenu, {backgroundColor: '#99D076', marginRight: 23 }]}
              onPress={() => this.props.navigation.navigate('ShoppingHistory')}
              >
              <View style={[styles.sliderText, styles.TextVertical]}>
                <Text style={[styles.spanBold, {}]}>Shoping</Text>
                <Text style={styles.spanLight}>History</Text>
              </View>
              <Image
                style={styles.sliderIcon}
                source={require('../assets/cartoon-3.png')}
              />
            </TouchableOpacity>
          </View>
        {/* Button under Card - - -  END */}

        {/* Overview Section */}
          <View style={styles.overview}>

          {/* Overview Title */}
            <View style={styles.overviewTitle}>
              <Text style={styles.titleLight}>Earn</Text>
              <Text style={styles.titleBold}>Points</Text>
            </View>

          {/* Overview Content */}
            <View style={styles.overviewContent}>
              <View style={[styles.overviewCardRow]}>
                <TouchableOpacity 
                  style={styles.pointCard}
                  onPress={() => this.props.navigation.navigate('Share2')}>
                  <View style={[styles.imgWp, {position: 'absolute', left: 15, top: 25, flexDirection: 'row'}]}>
                    <Image
                      style={{height:25, width:25, marginRight: 15}}
                      source={require('../assets/icon-instagram-purple.png')}
                    />
                    <Image
                      style={{height:25, width:10, marginRight: 15}}
                      source={require('../assets/icon-facebook-purple.png')}
                    />
                    <Image
                      style={{height:25, width:25, marginRight: 15}}
                      source={require('../assets/icon-twitter-purple.png')}
                    />
                  </View>
                  <View style={[styles.pointVertical]}>
                    <Text style={[styles.pointTitle]}>Share</Text>
                    <Text style={[styles.pointSubtitle]}>get 20 Points!</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity 
                  style={styles.pointCard}
                  onPress={() => this.props.navigation.navigate('Watch')}>
                  <ImageBackground 
                    source={require('../assets/icon-watch.png')}
                    style={{height:85, width:'120%', position: 'absolute', left: '-15%', top: 0}}>
                  </ImageBackground>
                  <View style={[styles.pointVertical]}>
                    <Text style={[styles.pointTitle, {color: '#DD5565'}]}>Watch</Text>
                    <Text style={[styles.pointSubtitle]}>get 20 Points!</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <View style={[styles.overviewCardRow, {display: 'none'}]}>
                {/* Overview Card */}
                  <View style={styles.overviewCard}>
                    <Image
                      style={styles.overviewIcon}
                      source={require('../assets/icon-credit-card.png')}
                    />
                    <View style={[styles.TextVertical]}>
                      <Text style={[styles.overviewText, styles.small]}>Loyalty</Text>
                      <Text style={[styles.overviewText, styles.small]}>Points</Text>
                    </View>
                    <Text style={styles.overviewAmount}>8.196</Text>
                  </View>
                {/* Overview Card - - - END */}
                {/* Overview Card */}
                  <View style={styles.overviewCard}>
                    <Image
                      style={styles.overviewIcon}
                      source={require('../assets/icon-piggy-bank.png')}
                    />
                    <View style={[styles.TextVertical]}>
                      <Text style={[styles.overviewText, styles.small]}>Average</Text>
                      <Text style={[styles.overviewText, styles.small]}>monthly saving</Text>
                    </View>
                    <Text style={styles.overviewAmount}>8.196</Text>
                  </View>
                {/* Overview Card - - - END */}
              </View>

              <View style={[styles.overviewCardRow, {display: 'none'}]}>
                {/* Overview Card */}
                  <View style={styles.overviewCard}>
                    <Image
                      style={styles.overviewIcon}
                      source={require('../assets/icon-price-tag.png')}
                    />
                    <View style={[styles.TextVertical]}>
                      <Text style={[styles.overviewText, styles.small]}>Daily</Text>
                      <Text style={[styles.overviewText, styles.small]}>promo usage</Text>
                    </View>
                    <Text style={styles.overviewAmount}>8.196</Text>
                  </View>
                {/* Overview Card - - - END */}
                {/* Overview Card */}
                  <View style={styles.overviewCard}>
                    <Image
                      style={styles.overviewIcon}
                      source={require('../assets/icon-price-tag.png')}
                    />
                    <View style={[styles.TextVertical]}>
                      <Text style={[styles.overviewText, styles.small]}>Daily</Text>
                      <Text style={[styles.overviewText, styles.small]}>promo usage</Text>
                    </View>
                    <Text style={styles.overviewAmount}>8.196</Text>
                  </View>
                {/* Overview Card - - - END */}
              </View>
            </View>
          {/* Overview Content- - - END */}
          </View>
        {/* Overview Section - - -  END */}

          <View style={styles.megaAward}>
          {/* Mega Award Title */}
            <View style={styles.megaAwardTitle}>
              <Text style={styles.megaAwardtitleLight}>Mega</Text>
              <Text style={styles.megaAwardtitleBold}>Reward</Text>
            </View>

          {/* Mega Award Slider */}
            <View style={[styles.sliderBox, styles.sliderAwards]}>
              <ScrollView 
                horizontal={true}
                showsHorizontalScrollIndicator={false}
              >

                {/* Mega Award Card 1 */}
                  <View style={styles.slideAward}>
                    <View style={styles.AwardImageWp}>
                      <Image
                        style={styles.AwardImage}
                        source={require('../assets/award-image-1.png')}
                      />
                    </View>
                    <View style={styles.awardContent}>
                      <Text style={styles.awardContentText}>
                        Ding Ding, Its lunch time! Which one do your prefer?
                      </Text>
                      <View style={styles.pointAward}>
                        <Image
                          style={styles.pointIcon}
                          source={require('../assets/icon-target.png')}
                        />
                        <Text style={styles.pointText}>200</Text>
                      </View>
                    </View>
                    <View style={styles.awardButton}>
                      <Text style={styles.awardButtonText}>vote</Text>
                    </View>
                  </View>
                {/* Mega Award Card 1 - - - END */}

                {/* Mega Award Card 2 [use marginRight: 23; for last item in slider] */}
                  <View style={[styles.slideAward, {marginRight: 23}]}>
                    <View style={[styles.AwardImageWp, styles.AwardVideoWp]}>
                      <Image
                        style={styles.AwardImage}
                        source={require('../assets/award-image-2.png')}
                      />
                    </View>
                    <View style={[styles.awardContent, styles.AwardVideoContent]}>
                      <Text style={[styles.awardContentText, styles.awardVideoContentText]}>
                        Psst! have you seen this video before ?
                      </Text>
                      <View style={styles.pointAward}>
                        <Image
                          style={styles.pointIcon}
                          source={require('../assets/icon-target.png')}
                        />
                        <Text style={styles.pointText}>200</Text>
                      </View>
                    </View>
                    <View style={styles.awardButton}>
                      <Text style={styles.awardButtonText}>watch</Text>
                    </View>
                  </View>
                {/* Mega Award Card 2 - - - END */}
              </ScrollView>
            </View>
          </View>

        </ScrollView>
      </View>
    );
  }
};

export default Home2;

const styles = StyleSheet.create({

  spanRecipt: {
    position: 'absolute',
    right: 0,
    top: 0,
    borderRadius: 50,
    height: 25,
    width: 25,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#D33E3E',
    borderWidth: 2,
    borderColor: '#FFFFFF',
  },
  spanReciptText: {
    color: '#ffffff',
    fontSize: 10,
    fontWeight: '400',
    fontFamily: 'CeraPro-Bold',
    alignItems: 'center',
    justifyContent: 'center',
  },

  pointCard: {
    backgroundColor: '#fff',
    borderRadius: 16,
    width: '45%',
    height: 175, 
    overflow: 'hidden',
  },
  pointVertical: {
    position: 'absolute',
    bottom: 15,
    left: 15,
  },
  pointTitle: {
    fontSize: 27,
    // fontWeight: '700',
    fontFamily: 'CeraPro-Bold',
    color: '#4977BB',
  },
  pointSubtitle: {
    fontSize: 14,
    fontWeight: '400',
    fontFamily: 'CeraPro-Regular',
    color: '#798191',
  },

  mini: {
    fontSize: normalize(12),
  },
  small: {
    fontSize: normalize(15),
  },
  medium: {
    fontSize: normalize(17),
  },
  large: {
    fontSize: normalize(20),
  },
  xlarge: {
    fontSize: normalize(25),
  },

  cardWp: {
    marginBottom: 190,
  },
  cardBg: {
    width: '100%',
    position: 'relative',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.2,
    shadowRadius: 6,  
    alignItems: 'center',
    justifyContent: 'center',
    height: 190,
    overflow: 'visible',
    margin: 5,
    borderRadius: 16,
    elevation: 8,
  },
  qrIcon: {
    height: 100,
    width: 100,
  },


  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  mcGrid: {
    margin: 0,
    padding: 0,
    zIndex: 0,
    position: "relative",
  },
  mcRow: {
    marginVertical: 0,
    marginHorizontal: 18,
    flexDirection: 'row',
  },
  fullWidth: {
    marginHorizontal: 0,
  },
  firstRow: {
    marginTop: hp('5%'),
  },
  mt15: {
    marginVertical: 15,
  },
  mt25: {
    marginVertical: 25,
  },
  mcCol1: {
    flex: 1,
  },
  mcCol2: {
    flex: 2,
  },
  mcCol3: {
    flex: 3,
  },
  mcCol4: {
    flex: 4,
  },
  mcCol5: {
    flex: 5,
  },
  mcCol6: {
    flex: 6,
  },
  block: {
    backgroundColor: '#dddddd',
    borderRadius: 8,
    height: 50,
    margin: 5,
  },

  gradientBg: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: hp('30%'),
  },

  radiusUpper: {
    backgroundColor: '#F5FCFF',
    position: 'absolute',
    left: 0,
    top: hp('26%'),
    width: '100%',
    height: 35,
    borderTopRightRadius: 16,
    borderTopLeftRadius: 16,
  },

  sliderBox: {
    position: 'relative',
    zIndex: 9,
    elevation: 5,
  },

  buttonIcon: {
    height: 85,
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F6F6F6',
  },
  buttonText: {
    marginTop: 8,
    color: '#2A3348',
    fontFamily: 'CeraPro-Regular',
  },

  // userBalance - - - H E R E 

  // topNavigation - - - H E R E
  topNavigation: {
    height: 50,
    marginVertical: 6,
    position: 'relative',
    zIndex: 9,
    marginHorizontal: 25,
  },
  backButton: {
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  leftIcons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  topNavTitle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  pageTitle: {
    fontSize: 22,
    color: '#2A3348',
    fontWeight: '700',
    paddingBottom: 8,
  },

  mapWrapper: {
    height: ScreenHeight,
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1
  },

  // pageWrapper - - - H E R E
  pageWrapper: {
    marginTop: -65,
    paddingTop: 65,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 25,
    paddingHorizontal: 35,
  },
  pageHeader: {
    backgroundColor: 'rgba(255,255,255,.6)',
    padding: 15,
    position: 'relative',
    height: 85,
    marginTop: 15,
    borderTopLeftRadius: 14,
    borderTopRightRadius: 14,
  },
  headerTitle: {
    fontSize: 29,
    color: '#2A3348',
    fontWeight: '700',
    flex: 1,
  },
  headerSubtitle: {
    fontSize: 13,
    color: '#2A3348',
  },
  headerImage: {
    position: 'absolute',
    right: 35,
    top: -15,
    width: 65,
    height: 65,
    backgroundColor: 'rgba(255,255,255,.8)',
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pageContent: {
    padding: 15,
    backgroundColor: '#ffffff',
    borderBottomLeftRadius: 14,
    borderBottomRightRadius: 14,
    marginBottom: 100,
  },
  // mcFooter: {
  //   width: '100%',
  //   height: 75,
  // },
  mb75: {
    marginBottom: 75,
  },

  // Header 
  header: {
    flexDirection: 'column',
    paddingHorizontal: 15,
    paddingTop: hp('5%'),
    paddingBottom: 35,
  },
  headerText: {
    fontSize: 25,
    color: '#2A3348',
    fontFamily: 'CeraPro-Regular',
  },
  headerName: {
    fontSize: 25,
    color: '#2A3348',
    fontFamily: 'CeraPro-Bold',
  },
  // Slider Menu 
  sliderMenuWp: {
    marginTop: 35,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: 23,
  },
  sliderMenu: {
    height: 120, 
    width: '28%', 
    marginTop: 5,
    marginBottom: 15,
    backgroundColor: '#8293DD',
    position: 'relative',
    overflow: 'hidden',
    borderRadius: 16,
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 12 },
    shadowOpacity: 0.3,
    shadowRadius: 15,  
    elevation: 6,
  },
  sliderText: {
    position: 'absolute',
    left: 15,
    top: 15,
    flexDirection: 'row',
  },
  TextVertical: {
    flexDirection: 'column'
  },
  spanBold: {
    fontSize: 17,
    color: '#fff',
    fontFamily: 'CeraPro-Bold',
    marginRight: 5,
  },
  spanLight: {
    fontFamily: 'CeraPro-Regular',
    fontSize: 17,
    color: '#fff',
  },
  sliderIcon: {
    height: 125,
    width: 95,
    position: 'absolute',
    right: 0,
    bottom: 0,
    zIndex: 0,
  },
  // account overview 
  overview: {
    marginTop: 35,
    position: 'relative',
    backgroundColor: '#61C4B1',
    width: wp('100%'),
    // height: 350,
    borderTopLeftRadius: 35,
    borderTopRightRadius: 35,
    paddingHorizontal: 23,
    paddingVertical: 35,
  },
  overviewTitle: {
    flexDirection: 'row',
  },
  titleBold: {
    fontSize: 22,
    color: '#fff',
    fontFamily: 'CeraPro-Bold',
    marginLeft: 5,
  },
  titleLight: {
    fontFamily: 'CeraPro-Regular',
    fontSize: 22,
    color: '#fff',
  },
  overviewContent: {
    position: 'relative',
    marginTop: 35,
    marginBottom: 35,  
  },
  overviewCardRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: hp('4%'),
  },
  overviewCard: {
    width: '45%',
    padding: 20,
    paddingBottom: 15,
    backgroundColor: '#fff',
    borderRadius: 16,
    shadowColor: '#879BBC',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.2,
    shadowRadius: 6,  
    elevation: 6,
  },
  overviewIcon: {
    width: 50,
    height: 50,
    marginBottom: 20,
  },
  overviewText: {
    fontFamily: 'CeraPro-Regular',
    fontSize: 17,
    color: '#798191',
  },
  overviewAmount: {
    fontFamily: 'CeraPro-Bold',
    fontSize: 27,
    color: '#2A3348',
  },
  // award wrapper
  megaAward: {
    marginTop: -35,
    position: 'relative',
    backgroundColor: '#fff',
    width: wp('100%'),
    // height: 350,
    borderTopLeftRadius: 35,
    borderTopRightRadius: 35,
    paddingVertical: 35,
  },
  megaAwardTitle: {
    flexDirection: 'row',
    paddingHorizontal: 23,
  },
  megaAwardtitleBold: {
    fontSize: 22,
    color: '#2A3348',
    fontFamily: 'CeraPro-Bold',
    marginLeft: 5,
  },
  megaAwardtitleLight: {
    fontFamily: 'CeraPro-Regular',
    fontSize: 22,
    color: '#2A3348',
  },
  // Award Slider

  sliderAwards: {
    flexDirection: 'row',
    height: 250,
    marginTop: 25,
    width: '100%',
  },
  slideAward: {
    width: wp('75%'),
    height: 236,
    marginLeft: 23,
    // backgroundColor: '#ddd',
    borderRadius: 16,
    // shadowColor: '#879BBC',
    // shadowOffset: { width: 0, height: 4 },
    // shadowOpacity: 0.2,
    // shadowRadius: 6,  
    // elevation: 6,
  },
  AwardImageWp: {
    width: '99%',
    height: 120,
    borderRadius: 16,
    overflow: 'hidden',
    position: 'absolute',
    zIndex: 9,
    left: 0,
    top: 0,
    elevation: 1,
    marginLeft: 4,
  },
  AwardImage: {
    width: '100%',
    height: '100%',
  },
  awardContent: {
    backgroundColor: Platform.OS === 'android' ? '#F6F6F6' : '#fff',
    marginLeft: 4,
    borderRadius: 16,
    paddingHorizontal: 15,
    paddingVertical: 25,
    paddingBottom: 10,
    borderRadius: 16,
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.2,
    shadowRadius: 6,
    marginTop: 110,
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    elevation: 0,
    position: 'relative',
    zIndex: 0,
  },
  awardContentText: {
    fontSize: 16,
    color: '#2A3348',
    fontFamily: 'CeraPro-Bold',
    marginBottom: 10,
    lineHeight: Platform.OS === 'android' ? 17 : 15,
  },
  pointAward: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  pointIcon: {
    height: 18,
    width: 18,
    marginRight: 8,
  },
  pointText: {
    fontSize: 18,
    color: '#DD5565',
    fontFamily: 'CeraPro-Bold',
  },
  awardButton: {
    position: 'absolute',
    right: -1,
    bottom: 0,
    backgroundColor: '#50D6B6',
    borderTopLeftRadius: 50,
    borderBottomLeftRadius: 50,
    borderBottomRightRadius: Platform.OS === 'android' ? 50 : 25,
    paddingHorizontal: 25,
    paddingVertical: 10,
    zIndex: 9,
    shadowColor: '#50D6B6',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.2,
    shadowRadius: 6,  
    // elevation: 7,
    overflow: Platform.OS === 'android' ? 'hidden' : 'visible',
  },
  awardButtonText: {
    color: '#fff',
    fontSize: 18,
    fontFamily: 'CeraPro-Bold',
  },
  // variation 2
  AwardVideoWp: {
    height: 216,
    zIndex: 0,
    elevation: 0,
    opacity: 0.8,
  },
  AwardVideoContent: {
    backgroundColor: 'rgba(0,0,0,0)',
    position: 'absolute',
    zIndex: 9,
    elevation: 0,
  },
  awardVideoContentText: {
    color: '#fff',
    opacity: 1,
  }
});