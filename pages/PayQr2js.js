import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , Modal
  , TouchableOpacity 
  , TextInput
  , SafeAreaView } from 'react-native';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import { 
  styles
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

import BoxAccent from '../components/BoxAccent';
import Button from '../components/Button';
// import PayQrModal from '../components/PayQrModal';

let ScreenHeight = Dimensions.get("window").height;

class PayQr extends React.Component {
  static navigationOptions = {
    header: null
  };

  // modal visibility
  state = {
    modalVisible: false,
  };

  // set modal visible function
  setModalVisible(visible) {
    this.setState({modalVisible: visible});
  };

  render() {

    return (
      <View>
        {/* QR WRapper area */}
        <View style={page.QRWrapper}> 
          <Image
            style={{width: '100%', height: '100%'}}
            source={require('../assets/qr-background.png')}
          />
        </View>
        {/* QR WRapper area - - - END */}

        {/* SafeAreaView  */}
        <SafeAreaView style={{ position: 'relative', zIndex: 99 }}>

          <View style={[styles.mcGrid, page.top]}>
            {/* Top Navigation */}
            <View style={[styles.mcRow, styles.topNavigation]}>
              <TouchableOpacity 
                style={[styles.mcCol1, styles.backButton]}
                onPress={() => {this.props.navigation.navigate('Home'); }}>
                <View style={page.circleButton}>
                  <Image
                    style={{width: 30, height: 30, marginTop: 5}}
                    source={require('../assets/icon-chevron-left.png')}
                  />
                </View>
              </TouchableOpacity>
              <View style={[styles.mcCol4, styles.topNavTitle]}>
                <Text style={styles.pageTitle}></Text>
              </View>
              <View style={[styles.mcCol3, styles.leftIcons]}>
                {/* <View style={page.circleButton}>
                  <Image
                    style={{width: 16, height: 22}}
                    source={require('../assets/icon-lightbulb.png')}
                  />
                </View> */}
              </View>
            </View>
            {/* Top Navigation - - - END */}
          </View>
          
          {/* Square Accent Image */}  
          <View style={page.squareArea}>
            <TouchableOpacity
            onPress={() => {this.props.navigation.navigate('AfterScanPage'); }}>
              <Image
                style={{width: 200, height: 200, marginTop: -100}}
                source={require('../assets/square-area.png')}
              />
            </TouchableOpacity>
            <Text style={page.textTitle}>Scan qr code and pay!</Text>
            <Text style={page.textSubtitle}>As easy point your camera to qr code!</Text>

            <View style={page.circleButtonBottom}>
              <Image
                style={{width: 16, height: 22}}
                source={require('../assets/icon-lightbulb.png')}
              />
            </View>
          </View>
          {/* Square Accent Image - - - END */}

        </SafeAreaView>
        {/* SafeAreaView - - - END*/}

        {/* Modal Area */}  
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          <View style={page.modalOverlay}>
            {/* Close navigation for modal */}  
            <SafeAreaView>
              <View style={[styles.mcRow, styles.topNavigation]}>
                <TouchableOpacity 
                  style={[styles.mcCol1, styles.backButton]}
                  onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);
                }}>
                  <View style={page.circleButton}>
                    <Image
                      style={{ width: 30, height: 30, marginLeft: 5, transform: [{ rotate: '-90deg' }] }}
                      source={require('../assets/icon-chevron-left.png')}
                    />
                  </View>
                </TouchableOpacity>
                <View style={[styles.mcCol4, styles.topNavTitle]}>
                  <Text style={styles.pageTitle}></Text>
                </View>
                <View style={[styles.mcCol3, styles.leftIcons]}>
                </View>
              </View>
            </SafeAreaView>
            {/* Close navigation for modal - - - END */}
            <View style={page.modalBody}>
              <BoxAccent
                yAxis={50}
                xAxis={-125}
                width={200}
                height={200}
              ></BoxAccent>
              <BoxAccent
                yAxis={145}
                xAxis={225}
                width={350}
                height={200}
              ></BoxAccent>
              <View style={page.modalHeader}>
                <View style={page.modalHeaderImageWp}>
                  <View style={page.modalHeaderImageBg}>
                    <Image
                      style={page.modalHeaderImage}
                      source={require('../assets/brand-starbucks.png')}
                    />
                  </View>
                </View>
                {/* Name of store */}  
                <Text style={page.modalHeaderTitle}>
                  Starbucks
                </Text>
              {/* Location of store */} 
                <Text style={page.modalHeaderSubtitle}>
                  Ground Floor, Grand Indonesia
                </Text>
              </View>
              <View style={page.modalContent}>
                {/* Total Payment of store */} 
                <View style={page.modalTotalPayment}>
                  <Text style={[page.totalText, styles.medium]}>
                    Total Payment
                  </Text>
                  <View style={page.totalAmount}>
                    <Text style={[page.sup, styles.small]}>
                      Rp
                    </Text>
                    <Text style={[page.number, styles.xlarge]}>
                      12.280.720
                    </Text>
                  </View>
                </View>
              {/* Cashback of store */} 
                <View style={page.modalCashback}>
                  <Text style={[page.cashbackText, styles.medium]}>
                    Potensial Cashback
                  </Text>
                  <View style={page.cashbackAmount}>
                    <Text style={[page.cashbackText, styles.medium]}>
                      RP 20.000 !
                    </Text>
                  </View>
                </View>
              </View>
            {/* Next button actually */} 
              <View style={page.modalButton}>
                <Button 
                  text="Proceed to pay"
                  size="large"
                  theme="primary2"
                  onPress={() => {this.props.navigation.navigate('PayEnterPin'); this.setModalVisible(!this.state.modalVisible)}} 
                />
              </View>
            </View>
          </View>
        </Modal>
        {/* Modal Area - - - END */}
      </View>
    );
  }
};

export default PayQr;

const page = StyleSheet.create({

  top: {
    zIndex: 3,
  },

  QRWrapper: {
    height: ScreenHeight,
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1,
  },
  circleButton: {
    height: 40,
    width: 40,
    backgroundColor: '#fff',
    borderRadius: 60,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 6,
    elevation: 9,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
  },
  circleButtonBottom: {
    marginTop: 50,
    top: 50,
    height: 70,
    width: 70,
    backgroundColor: '#fff',
    borderRadius: 60,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 6,
    elevation: 9,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.2,
    shadowRadius: 4,
  },
  squareArea: {
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textTitle: {
    marginTop: 75,
    fontSize: 22,
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'CeraPro-Bold',
  },
  textSubtitle: {
    fontSize: 12,
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'CeraPro-Regular',
  },
  modalOverlay: {
    width: '100%', 
    height: '100%', 
    backgroundColor: 'rgba(0,0,0,.3)', 
    position: 'relative'
  },
  modalBody: {
    width: '100%', 
    height: 400, 
    backgroundColor: '#fff', 
    position: 'absolute', 
    bottom: 0,
    borderTopRightRadius: 25,
    borderTopLeftRadius: 25,
  },
  modalHeader: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: -55,
  },
  modalHeaderImageWp: {
    height: 113,
    width: 113,
    backgroundColor: 'rgba(255,255,255, .7)',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalHeaderImageBg: {
    backgroundColor: 'rgba(255,255,255,1)',
    height: 85,
    width: 85,
    borderRadius: 100,
    // overflow: 'hidden',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    elevation: 3,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.3,
    shadowRadius: 2,
  },
  modalHeaderImage: {
    height: 63,
    width: 63,
  },
  modalHeaderTitle: {
    fontSize: 26,
    color: black,
    fontFamily: 'CeraPro-Bold',
  },
  modalHeaderSubtitle: {
    fontSize: 14,
    color: grey,
    fontFamily: 'CeraPro-Regular',
  },
  modalContent: {
    paddingHorizontal: 25,
  },
  modalTotalPayment: {
    position: 'relative',
    zIndex: 9,
    marginTop: 35,
    width: '100%',
    borderRadius: 8,
    backgroundColor: lightGrey,
    paddingHorizontal: 20,
    paddingVertical: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  totalText: {
    color: black,
    fontFamily: 'CeraPro-Bold',
    flex: 1,
  },
  totalAmount: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-end'
  },
  sup: {
    // fontSize: 14,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
    marginRight: 3,
  },
  number: {
    // fontSize: 24,
    fontFamily: 'CeraPro-Bold',
    color: primary2,
  },
  modalCashback: {
    position: 'relative',
    zIndex: 8,
    width: '100%',
    borderRadius: 8,
    backgroundColor: '#E0EAFF',
    marginTop: -15,
    paddingTop: 30,
    paddingHorizontal: 20,
    paddingVertical: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
  cashbackText: {
    color: '#6E95E5',
    fontFamily: 'CeraPro-Bold',
    flex: 1,
  },
  modalButton: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    paddingBottom: 25,
  },


  pageWrapper: {
    marginTop: -65,
    paddingTop: 125,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 250,
    paddingHorizontal: 0,
    marginBottom: 50,
  },
  center: {
    paddingHorizontal: 30,
    marginHorizontal: 0,
  }

});