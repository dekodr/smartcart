import React from 'react';
import PropTypes from 'prop-types';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , Modal
  , SafeAreaView
  , TouchableOpacity } from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
// import LinearGradient from 'react-native-linear-gradient';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { normalize } from '../assets/Normalize';

import Button from '../components/Button';
import UserCard from '../components/UserCard';
import TextDivider from '../components/TextDivider';
import PromoBox2 from '../components/PromoBox2';
import PromoBox1 from '../components/PromoBox1';
import PromoBox3 from '../components/PromoBox3';
import BoxAccent from '../components/BoxAccent';
import BoxAccent2 from '../components/BoxAccent2';
import UserBalance from '../components/UserBalance';
import Carousel from 'react-native-snap-carousel';

import { 
  black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

let ScreenHeight = Dimensions.get("window").height;

class Home extends React.Component {
  static navigationOptions = {
    header: null
  }

  constructor(props){
    super(props);

    this.state = {
      entries: [1,2,3,4],
    }
  }

  _onPressCarousel = () => {
    // here handle carousel press
  }

  _renderItem () {
    return (
      <TouchableOpacity 
        style={{ borderRadius: 16 }}
        onPress={
          () => this.props.navigation.navigate('PromoDetail', { Component: { options: { bottomTabs: { visible: false, drawBehind: true, animate: true } } } })
        }>
        <PromoBox2
          image={require('../assets/promo-2-1.png')} 
          title="Buy 1 get 1 FREE!"
          span="Hot Deals!"
          noText
        />
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        
        <ScrollView style={styles.mcGrid}>
          <ImageBackground style={styles.gradientBg} source={require('../assets/linear-bg.png')}>
            <BoxAccent2
              yAxis={110}
              xAxis={-175}
              width={300}
              height={300}
            ></BoxAccent2>
            <BoxAccent2
              yAxis={-125}
              xAxis={25}
              width={200}
              height={200}
            ></BoxAccent2>
            <View style={styles.radiusUpper}>
            </View>
          </ImageBackground>

          <SafeAreaView>
            {/* Top Navigation */}
              <View style={[styles.mcRow, styles.topNavigation]}>
                <TouchableOpacity 
                  style={[styles.mcCol6, styles.backButton]}
                  onPress={() => {this.props.navigation.navigate('Home'); }}>
                  <View style={styles.logo}>
                    <Image
                      style={{width: 115, height: 25}}
                      source={require('../assets/logo-white.png')}
                    />
                  </View>
                </TouchableOpacity>
                <View style={[styles.mcCol1, styles.topNavTitle]}>
                  <Text style={styles.pageTitle}></Text>
                </View>
                <View style={[styles.mcCol3, styles.leftIcons]}>
                  <Image
                      style={{width: 20, height: 20}}
                      source={require('../assets/icon-bell-notification.png')}
                    />
                </View>
              </View>
            {/* Top Navigation - - -  END */}
          </SafeAreaView>

          <View style={[styles.sliderDefault]}>
            <Carousel
              ref={(c) => { this._carousel = c; }}
              data={this.state.entries}
              renderItem={this._renderItem}
              sliderWidth={wp('100%')}
              itemWidth={wp('90%')}
              loop={true}
              loopClonesPerSide={2}
              inactiveSlideScale={0.95}
              inactiveSlideOpacity={1}
              enableMomentum={true}
              activeSlideAlignment={'start'}
              activeAnimationType={'spring'}
              activeAnimationOptions={{
                  friction: 4,
                  tension: 40
              }}
              slideStyle={{ overflow: 'visible' }}
              autoplay={true}
              autoplayDelay={500}
              autoplayInterval={3000}
            />
            {/*<ScrollView 
                          horizontal={true}
                          showsHorizontalScrollIndicator={false}
                          >
                          <TouchableOpacity 
                            style={{ borderRadius: 16 }}
                            onPress={
                              () => this.props.navigation.navigate('PromoDetail', { Component: { options: { bottomTabs: { visible: false, drawBehind: true, animate: true } } } })
                            }>
                            <PromoBox2
                              image={require('../assets/promo-2-1.png')} 
                              title="Buy 1 get 1 FREE!"
                              span="Hot Deals!"
                              noText
                            />
                          </TouchableOpacity>
                          <TouchableOpacity 
                            onPress={() => this.props.navigation.navigate('PromoDetail')}>
                            <PromoBox2
                              image={require('../assets/promo-2-2.png')} 
                              title="Buy 2 get 1 FREE!"
                              span="Hot Deals!"
                              noText
                            />
                          </TouchableOpacity>
                          <TouchableOpacity 
                            onPress={() => this.props.navigation.navigate('PromoDetail')}>
                            <PromoBox2
                              image={require('../assets/promo-2-1.png')} 
                              title="Buy 3 get 1 FREE!"
                              span="Hot Deals!"
                              noText
                              lastSlide
                            />
                          </TouchableOpacity>
                        </ScrollView>*/}
          </View>

          {/* <View style={[styles.mcRow, styles.firstRow]}>
         
            <View style={styles.mcCol1}>
              <UserCard 
                name="m de chalidad"
                amount="202.867"
                expire="12/23"
              />
            </View>

          </View> */}

          <View style={[styles.mcRow]}>
            <UserBalance 
              balance='280.220'
              cashback='18.000'
            />
          </View>

          <View style={[styles.mcRow, styles.mt25]}>
            <View style={styles.mcCol4}>
              <TouchableOpacity 
                onPress={() => this.props.navigation.navigate('PayQr')}>
                <View 
                  style={[styles.block, styles.buttonIcon]}>
                  <Image
                    style={{width: 39, height: 35, overflow: 'visible'}}
                    source={require('../assets/icon-pay.png')}
                  />
                  <Text style={[styles.buttonText, styles.mini]}>Pay</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.mcCol4}>
              <View style={[styles.block, styles.buttonIcon]}>
                <Image
                  style={{width: 32, height: 39, overflow: 'visible'}}
                  source={require('../assets/icon-food-menu.png')}
                />
                <Text style={[styles.buttonText, styles.mini]}>Menu</Text>
              </View>
            </View>
            <View style={styles.mcCol4}>
              <View style={[styles.block, styles.buttonIcon]}>
                <Image
                  style={{width: 32, height: 42, overflow: 'visible'}}
                  source={require('../assets/icon-reward.png')}
                />
                <Text style={[styles.buttonText, styles.mini]}>Reward</Text>
              </View>
            </View>
            <View style={styles.mcCol4}>
              <TouchableOpacity 
                onPress={() => this.props.navigation.navigate('History')}>
                <View style={[styles.block, styles.buttonIcon]}>
                  <Image
                    style={{width: 34, height: 39, overflow: 'visible'}}
                    source={require('../assets/icon-history.png')}
                  />
                  <Text style={[styles.buttonText, styles.mini]}>History</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>

          <TextDivider 
            upper="only for you"
            under="today special!"
          />

          <View style={[styles.sliderDefault]}>
            <ScrollView 
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              <TouchableOpacity 
                style={{ borderRadius: 16 }}
                onPress={
                  () => this.props.navigation.navigate('PromoDetail', { Component: { options: { bottomTabs: { visible: false, drawBehind: true, animate: true } } } })
                }>
                <PromoBox2
                  image={require('../assets/promo-2-1.png')} 
                  title="Buy 1 get 1 FREE!"
                  span="Hot Deals!"
                />
              </TouchableOpacity>
              <TouchableOpacity 
                onPress={() => this.props.navigation.navigate('PromoDetail')}>
                <PromoBox2
                  image={require('../assets/promo-2-2.png')} 
                  title="Buy 2 get 1 FREE!"
                  span="Hot Deals!"
                />
              </TouchableOpacity>
              <TouchableOpacity 
                onPress={() => this.props.navigation.navigate('PromoDetail')}>
                <PromoBox2
                  image={require('../assets/promo-2-1.png')} 
                  title="Buy 3 get 1 FREE!"
                  span="Hot Deals!"
                  lastSlide
                />
              </TouchableOpacity>
            </ScrollView>
          </View>

          <TextDivider 
            upper="top stores"
            under="near you!"
          />

          <View style={[styles.sliderBox]}>
            <ScrollView 
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              <PromoBox1 
                text="Store title #1"
                noDistance
              />
              <PromoBox1 
                text="Store title #2"
                noDistance
              />
              <PromoBox1 
                text="Store title #3"
                noDistance
              />
              <PromoBox1 
                text="Store title #4"
                noDistance
              />
              <PromoBox1 
                text="Store title #5"
                noDistance
              />
              <PromoBox1 
                text="Store title #6"
                noDistance
                lastSlide
              />
            </ScrollView>
          </View>

          <TextDivider 
            upper=""
            under="discover promo"
          />

          <View style={[styles.sliderBox]}>
            <ScrollView 
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              <PromoBox3 
                title="Unilever Mega sale WEEK!"
                subtitle="Grand Indonesia"
                span="0.1 KM"
              />
              <PromoBox3 
                title="Unilever Mega sale WEEK!"
                subtitle="Grand Indonesia"
                span="0.1 KM"
              />
              <PromoBox3 
                title="Unilever Mega sale WEEK!"
                subtitle="Grand Indonesia"
                span="0.1 KM"
              />
              <PromoBox3 
                title="Unilever Mega sale WEEK!"
                subtitle="Grand Indonesia"
                span="0.1 KM"
              />
              <PromoBox3 
                title="Unilever Mega sale WEEK!"
                subtitle="Grand Indonesia"
                span="0.1 KM"
              />
              <PromoBox3 
                title="Unilever Mega sale WEEK!"
                subtitle="Grand Indonesia"
                span="0.1 KM"
                lastSlide
              />
            </ScrollView>
          </View>
          <View style={[styles.sliderBox, styles.mb75]}>
            <ScrollView 
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              <PromoBox3 
                title="Unilever Mega sale WEEK!"
                subtitle="Grand Indonesia"
                span="0.1 KM"
              />
              <PromoBox3 
                title="Unilever Mega sale WEEK!"
                subtitle="Grand Indonesia"
                span="0.1 KM"
              />
              <PromoBox3 
                title="Unilever Mega sale WEEK!"
                subtitle="Grand Indonesia"
                span="0.1 KM"
              />
              <PromoBox3 
                title="Unilever Mega sale WEEK!"
                subtitle="Grand Indonesia"
                span="0.1 KM"
              />
              <PromoBox3 
                title="Unilever Mega sale WEEK!"
                subtitle="Grand Indonesia"
                span="0.1 KM"
              />
              <PromoBox3 
                title="Unilever Mega sale WEEK!"
                subtitle="Grand Indonesia"
                span="0.1 KM"
                lastSlide
              />
            </ScrollView>
          </View>

          <View style={[styles.mcFooter]}>
          </View>

         <BoxAccent
          bottom={-145}
          xAxis={75}
          width={200}
          height={200}
        ></BoxAccent>
        <BoxAccent
            bottom={0}
            xAxis={-150}
            width={250}
            height={350}
        ></BoxAccent>

        </ScrollView>
      </View>
    );
  }
};

export default Home;

const styles = StyleSheet.create({

  mini: {
    fontSize: normalize(12),
  },
  small: {
    fontSize: normalize(15),
  },
  medium: {
    fontSize: normalize(17),
  },
  large: {
    fontSize: normalize(20),
  },
  xlarge: {
    fontSize: normalize(25),
  },

  container: {
    backgroundColor: '#F5FCFF',
    flex: 1,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  mcGrid: {
    margin: 0,
    padding: 0,
    zIndex: 0,
    position: "relative",
  },
  mcRow: {
    marginVertical: 0,
    marginHorizontal: 18,
    flexDirection: 'row',
  },
  fullWidth: {
    marginHorizontal: 0,
  },
  firstRow: {
    marginTop: 95,
  },
  mt15: {
    marginVertical: 15,
  },
  mt25: {
    marginVertical: 25,
  },
  mcCol1: {
    flex: 1,
  },
  mcCol2: {
    flex: 2,
  },
  mcCol3: {
    flex: 3,
  },
  mcCol4: {
    flex: 4,
  },
  mcCol5: {
    flex: 5,
  },
  mcCol6: {
    flex: 6,
  },
  block: {
    backgroundColor: '#dddddd',
    borderRadius: 8,
    height: 50,
    margin: 5,
  },

  gradientBg: {
    position: 'absolute',
    left: 0,
    top: 0,
    width: '100%',
    height: hp('30%'),
  },

  radiusUpper: {
    backgroundColor: '#F5FCFF',
    position: 'absolute',
    left: 0,
    top: hp('26%'),
    width: '100%',
    height: 35,
    borderTopRightRadius: 16,
    borderTopLeftRadius: 16,
  },

  sliderBox: {
    position: 'relative',
    zIndex: 9,
    elevation: 5,
  },

  buttonIcon: {
    height: 85,
    marginHorizontal: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#F6F6F6',
  },
  buttonText: {
    marginTop: 8,
    color: '#2A3348',
    fontFamily: 'CeraPro-Regular',
  },

  // userBalance - - - H E R E 

  // topNavigation - - - H E R E
  topNavigation: {
    height: 50,
    marginVertical: 6,
    position: 'relative',
    zIndex: 9,
    marginHorizontal: 25,
  },
  backButton: {
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  leftIcons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  topNavTitle: {
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  pageTitle: {
    fontSize: 22,
    color: '#2A3348',
    fontWeight: '700',
    paddingBottom: 8,
  },

  mapWrapper: {
    height: ScreenHeight,
    width: '100%',
    position: 'absolute',
    top: 0,
    left: 0,
    zIndex: 1
  },

  // pageWrapper - - - H E R E
  pageWrapper: {
    marginTop: -65,
    paddingTop: 65,
    position: 'relative',
    zIndex: 2,
    paddingBottom: 25,
    paddingHorizontal: 35,
  },
  pageHeader: {
    backgroundColor: 'rgba(255,255,255,.6)',
    padding: 15,
    position: 'relative',
    height: 85,
    marginTop: 15,
    borderTopLeftRadius: 14,
    borderTopRightRadius: 14,
  },
  headerTitle: {
    fontSize: 29,
    color: '#2A3348',
    fontWeight: '700',
    flex: 1,
  },
  headerSubtitle: {
    fontSize: 13,
    color: '#2A3348',
  },
  headerImage: {
    position: 'absolute',
    right: 35,
    top: -15,
    width: 65,
    height: 65,
    backgroundColor: 'rgba(255,255,255,.8)',
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pageContent: {
    padding: 15,
    backgroundColor: '#ffffff',
    borderBottomLeftRadius: 14,
    borderBottomRightRadius: 14,
    marginBottom: 100,
  },
  // mcFooter: {
  //   width: '100%',
  //   height: 75,
  // },
  mb75: {
    marginBottom: 75,
  }

});