import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, Image, StyleSheet } from 'react-native';
import { 
  styles
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

class UserBalance extends React.Component {
  static propTypes = {
    balance: PropTypes.string.isRequired,
    cashback: PropTypes.string.isRequired,
  };

  render() {
    const { balance, cashback } = this.props;
    const boxAccent = {
        position: 'absolute',
        borderRadius: 52,
        backgroundColor: '#fff',
        opacity: .15,
        transform: [{ rotate: '45deg'}]
    };
    return (
      <View style={components.userBalance}>
        <View style={[styles.mcCol3, components.balanceCol]}>
          <Text style={components.userBalanceUpper}>Balance</Text>
          <Text style={[components.userBalanceUnder, styles.medium]}>Rp {balance}</Text>
        </View>
        <View style={[styles.mcCol3, components.balanceCol]}>
          <Text style={components.userBalanceUpper}>Cashback</Text>
          <Text style={[components.userBalanceUnder, styles.medium]}>Rp {cashback}</Text>
        </View>
        <View style={[styles.mcCol1, components.qrCol]}>
          <Image
            style={{width: 28, height: 28}}
            source={require('../assets/icon-qrcode.png')}
          />
          <Text style={components.userQrColText}>QR ID</Text>
        </View>
      </View>
    )
  }
}

export default UserBalance;

const components = StyleSheet.create({
  userBalance: {
    width: '100%',
    height: 70,
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 5,
    paddingHorizontal: 15,
    borderRadius: 8,
    backgroundColor: '#fff',
    elevation: 4,
    shadowColor: '#367B6A',
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.24,
    shadowRadius: 4,
    marginTop: '3%',
  },
  userBalanceUpper: {
    fontSize: 14,
    color: primary2,
    fontFamily: 'CeraPro-Bold',
  },
  userBalanceUnder: {
    fontSize: 18,
    color: black,
    fontFamily: 'CeraPro-Bold',
  },
  balanceCol: {
    borderRightWidth: 1,
    borderColor: '#DBDBDB',
    marginRight: 15,
  },
  qrCol: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  userQrColText: {
    fontSize: 12,
    color: grey,
    fontFamily: 'CeraPro-Bold',
    marginTop: 5,
  },
});