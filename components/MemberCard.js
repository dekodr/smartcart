import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, ImageBackground, StyleSheet, Image } from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

class MemberCard extends React.Component {
  static propTypes = {
    amount: PropTypes.string.isRequired,
  };

	render() {
    const { amount, name, expire, image } = this.props;
		return (
			<View style={[styles.card]}>
        <ImageBackground 
          source={image}
          style={styles.cardBg}
          imageStyle={{ borderRadius: 16 }}>
          <View style={{ alignItems: 'center', justifyContent: 'flex-start', flexDirection: 'row', position: 'absolute', top: '15%', left: 25 }}>
            <Image
              style={{width: 95, height: 20}}
              source={require('../assets/logo-white.png')}
            />
          </View>
          <Text style={styles.cardName}>SILVER MEMBER</Text>
          <View style={styles.qrwp}>
            <Image
              style={{width: 35, height: 35}}
              source={require('../assets/qrcode-white.png')}
            />
            <Text style={styles.qrText}>SHOW QR</Text>
          </View>
          {/* <Text style={styles.cardAmount}>{amount}</Text> */}
        </ImageBackground>
      </View>
		)
	}
}

export default MemberCard;

const styles = StyleSheet.create({
  card: {
    height: 190,
    overflow: 'visible',
    margin: 5,
    borderRadius: 16,
    elevation: 8,
  },
  cardBg: {
    width: '100%',
    height: '100%',
    position: 'relative',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.2,
    shadowRadius: 6,  
    elevation: 6,
  },
  cardSup: {
    fontSize: 13,
    marginRight: 5,
    marginBottom: 7,
    color: '#2A3348',
    fontFamily: 'CeraPro-Bold',
  },
  cardName: {
    fontSize: 16,
    color: '#fff',
    textTransform: 'uppercase',
    position: 'absolute',
    bottom: 45,
    left: 25,
    fontFamily: 'CeraPro-Regular',
    letterSpacing: 3,
  },
  cardAmount: {
    fontSize: 36,
    color: '#fff',
    // letterSpacing: 1,  
    textTransform: 'capitalize',
    position: 'absolute',
    bottom: 25,
    right: 25,
    fontFamily: 'CeraPro-Bold',
  },
  qrwp: {
    position: 'absolute',
    bottom: 25,
    right: 0,
    backgroundColor: 'rgba(255,255,255,.3)',
    paddingVertical: 10,
    paddingLeft: 15,
    paddingRight: 15,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
  },
  qrText: {
    paddingTop: 5,
    fontSize: 7,
    color: '#fff',
    fontFamily: 'CeraPro-Regular',
    textTransform: 'uppercase',
  }
});