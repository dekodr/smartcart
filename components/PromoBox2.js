import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, Image, StyleSheet, ImageBackground, Modal, TouchableHighlight, ScrollView} from 'react-native';

import PromoBox1 from './PromoBox1';
import Button from './Button';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  mcGrid: {
    margin: 0,
    padding: 0,
    zIndex: 0,
    position: "relative",
  },
  mcRow: {
    marginVertical: 0,
    marginHorizontal: 18,
    flexDirection: 'row',
  },
  fullWidth: {
    marginHorizontal: 0,
  },
  firstRow: {
    marginTop: 95,
  },
  mt15: {
    marginVertical: 15,
  },
  mt25: {
    marginVertical: 25,
  },
  mcCol1: {
    flex: 1,
  },
  mcCol2: {
    flex: 2,
  },
  mcCol3: {
    flex: 3,
  },
  mcCol4: {
    flex: 4,
  },
  mcCol5: {
    flex: 5,
  },
  mcCol6: {
    flex: 6,
  },
  block: {
    backgroundColor: '#dddddd',
    borderRadius: 8,
    height: 50,
    margin: 5,
  },

  sliderDefault: {
    paddingHorizontal: 0,
    marginHorizontal: 0,
    height: 200,
  },
  sliderCard: {
    height: 75,
  },
  sliderParent: {
    position: 'relative',
    height: wp('50%'), 
    width: wp('84%'), 
  },
  promoDefault: {
    height: wp('50%'), 
    width: wp('84%'), 
    marginLeft: 23,
    overflow: 'hidden',
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: 'center',
  },
  promoTitle: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    paddingVertical: 15,
    paddingHorizontal: 25,
    backgroundColor: 'rgba(28,31,33,0.55)',
    borderBottomLeftRadius: 16,
    borderBottomRightRadius: 16,
  },
  title: {
    fontSize: 17,
    color: '#fff',
    fontWeight: '700',
    fontFamily: 'CeraPro-Bold',
  },
  spanArea: {
    width: '100%', 
    height: 165, 
    position: 'absolute', 
    overflow: 'visible', 
    borderRadius: 16,
    zIndex: 9,
  },
  promoSpan: {
    position: 'absolute',
    right: 15,
    top: -8,
    paddingVertical: 3,
    paddingHorizontal: 12,
    backgroundColor: '#F4376A',
    borderRadius: 25,
    zIndex: 99,
  },
  spanText: {
    fontSize: 10,
    color: '#ffffff',
    textTransform: 'uppercase',
    letterSpacing: 2,
    fontFamily: 'CeraPro-Bold',
  },
  lastSlide: {
    marginRight: 23,
  },

  // topNavigation
  topNavigation: {
    height: 50,
    marginVertical: 6,
    position: 'relative',
    zIndex: 9,
  },
  backButton: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  leftIcons: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  topNavTitle: {
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  pageTitle: {
    fontSize: 22,
    color: '#2A3348',
    fontWeight: '700',
    paddingBottom: 8,
  },

  // modalWrapper
  modalWrapper: {
    marginTop: -65,
    position: 'relative',
    zIndex: 1,
    paddingBottom: 25,
  },
  modalImage: {
    position: 'relative',
    zIndex: 1,
  },
  modalContent: {
    flex: 1,
    marginTop: -45,
    borderTopLeftRadius: 36,
    borderTopRightRadius: 36,
    backgroundColor: '#ffffff',
    position: 'relative',
    zIndex: 9,
    paddingBottom: 75,
  },
  modalContentLogo: {
    height: 80,
    width: 80,
    borderRadius: 100,
    backgroundColor: 'rgba(255,255,255,.75)',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    right: 35,
    top: -35,
    zIndex: 9,
  },
  modalContentMain: {
    marginTop: 75,
  },
  modalTitle: {
    fontSize: 28,
    color: '#2A3348',
    fontWeight: '700',
    paddingHorizontal: 35,
  },
  modalParagraph: {
    fontSize: 14,
    color: '#798191',
    marginVertical: 15,
    lineHeight: 25,
    paddingHorizontal: 35,
  },
  timeSection: {
    alignItems: 'center',
    flexDirection: 'row',
    padding: 14,
    backgroundColor: '#F6F6F6',
    borderRadius: 12,
    marginVertical: 15,
    marginHorizontal: 35,
    width: 250,
  },
  timeSectionText: {
    fontSize: 14,
    fontWeight: '700',
    color: '#2A3348',
    marginLeft: 13,
  },
  modalTextDivider: {
    fontSize: 22,
    fontWeight: '700',
    color: '#2A3348',
    marginHorizontal: 35,
    marginTop: 15,
  },
  list: {
    flex: 1,
    marginVertical: 15,
    marginHorizontal: 35,
  },
  listItem: {
    alignItems: 'flex-start',
    flexDirection: 'row',
  },
  listStyle: {
    width: 6,
    height: 6,
    borderRadius: 25,
    backgroundColor: '#798191',
    marginTop: 10,
  },
  listText: {
    fontSize: 14,
    color: '#798191',
    marginLeft: 8,
    lineHeight: 26,
  },
  modalButton: {
    marginHorizontal: 5,
    paddingTop: 75,
  },
  noText: {
    opacity: 0,
  }
});

const getStyles = ({
  lastSlide,
  noText
}) => {
  const promoStyles = [styles.promoDefault];
  const promoTitle = [styles.promoTitle];

  if (lastSlide) {
    promoStyles.push(styles.lastSlide);
  }

  if (noText) {
    promoTitle.push(styles.noText);
  }

  return { promoStyles, promoTitle }
}

class PromoBox2 extends React.Component {
  static propTypes = {
    title: PropTypes.string.isRequired,
    span: PropTypes.string.isRequired,
    lastSlide: PropTypes.bool,
    noText: PropTypes.bool,
  };

  static defaultProps = {
    lastSlide: false,
    noText: false,
  };

	render() {
    const { title, span, theme, lastSlide, image, noText } = this.props;
    const { promoStyles, promoTitle } = getStyles({ lastSlide, noText });
		return (
      <View
        style={promoStyles}
        activeOpacity={0}
        underlayColor="rgba(0,0,0,0)"
        onPress={() => {
          this.setModalVisible(true);
        }}>
        <View style={styles.spanArea}>
          <View style={styles.promoSpan}>
            <Text style={styles.spanText}>{span}</Text>
          </View>
        </View>
        <ImageBackground 
          source={image}
          style={{width: '100%', height: 165, position: 'relative', overflow: 'hidden', borderRadius: 16, zIndex: 1}}
          imageStyle={{ borderRadius: 16, overflow: 'visible' }}>
          <View style={promoTitle}>
            <Text style={styles.title}>{title}</Text>
          </View>
        </ImageBackground>
      </View>
		)
	}
}

export default PromoBox2;

