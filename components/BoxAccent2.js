import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, Image, StyleSheet } from 'react-native';

class BoxAccent2 extends React.Component {
  static propTypes = {
    yAxis: PropTypes.number,
    xAxis: PropTypes.number.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
    bottom: PropTypes.number,
  };

	render() {
    const { yAxis, xAxis, width, height, bottom } = this.props;
    const boxAccent = {
        position: 'absolute',
        borderRadius: 52,
        backgroundColor: '#CEFFF9',
        opacity: .18,
        transform: [{ rotate: '45deg'}]
    };
		return (
			<View style={Object.assign({}, boxAccent, {top: yAxis, right: xAxis, width: width, height: height, bottom: bottom}) }>
      </View>
		)
	}
}

export default BoxAccent2;

const styles = StyleSheet.create({
  boxAccent: {
    position: 'absolute',
    width: 250,
    height: 350,
    borderRadius: 52,
    backgroundColor: '#CEFFF9',
    transform: [{ rotate: '45deg'}],
    zIndex: 1,
  },
});