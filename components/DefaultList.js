import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, ImageBackground, StyleSheet, Image } from 'react-native';
import { 
  styles
  , black
  , primary
  , primary2
  , darkGrey
  , grey
  , lightGrey } from '../assets/style';

class DefaultList extends React.Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
  };

	render() {
    const { name } = this.props;
		return (
			<View style={comp.listItem}>
        <Text style={comp.listItemText}>
          {name}
        </Text>
        <View style={comp.listIconWp}>
          <Image
            style={comp.chevronRight}
            source={require('../assets/icon-chevron-left.png')}
          />
        </View>
      </View>
		)
	}
}

export default DefaultList;

const comp = StyleSheet.create({
  listItem: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 18,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#EAEAEA',
  },
  listItemText: {
    fontFamily: 'CeraPro-Regular',
    fontSize: 18,
    color: black,
  },
  listIconWp: {
    height: 20,
    width: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  chevronRight: {
    width: 20, 
    height: 20, 
    transform: [{ rotate: '-180deg' }]
  },
});