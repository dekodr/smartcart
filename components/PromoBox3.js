import React from 'react';
import PropTypes from 'prop-types';
import { normalize } from '../assets/Normalize';
import { 
  View
  , Text
  , Image
  , StyleSheet
  , ImageBackground
  , Platform } from 'react-native';

const styles = StyleSheet.create({
  mini: {
    fontSize: normalize(12),
  },
  small: {
    fontSize: normalize(15),
  },
  medium: {
    fontSize: normalize(17),
  },
  large: {
    fontSize: normalize(20),
  },
  xlarge: {
    fontSize: normalize(25),
  },
  sliderBox: {
    paddingHorizontal: 0,
    marginHorizontal: 0,
    height: 330,
    position: 'relative',
    zIndex: 9,
    elevation: 5,
  },
  promoBox: {
    height: 300, 
    width: 195, 
    marginLeft: 23,
    marginBottom: 15,
    backgroundColor: 'rgba(0,0,0,0)',
    borderRadius: 16,
    position: 'relative',
    overflow: 'hidden',
  },
  promoImageWp: {
    width: '100%',
    position: 'relative',
    zIndex: 3,
    height: 200,
    borderRadius: 16,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 12 },
    shadowOpacity: 0.3,
    shadowRadius: 12,
    elevation: 15,
    marginVertical: 15,
  },
  promoImage: {
    width: '100%',
    height: '100%',
    borderRadius: 16,
  },
  promoContent: {
    position: 'absolute',
    bottom: -5,
    left: 0,
    paddingHorizontal: 18,
    paddingVertical: 15,
    paddingTop: 35,
    zIndex: 2,
    backgroundColor: '#f6f6f6',
    borderBottomLeftRadius: 16,
    borderBottomRightRadius: 16,
  },
  promoTitle: {
    // fontSize: 17,
    color: '#2A3348',
    // fontWeight: '700',
    marginTop: 10,
    fontFamily: 'CeraPro-Bold'
  },
  promoSubtitle: {
    // fontSize: 10,
    color: '#36A49F',
    fontFamily: 'CeraPro-Regular'
  },
  span: {
    position: 'absolute',
    right: 3,
    top: -12,
    paddingVertical: 3,
    paddingHorizontal: 8,
    backgroundColor: '#50D6B6',
    borderRadius: 25,
    borderTopRightRadius: Platform.OS === 'ios' ? 10 : 25,
    borderBottomRightRadius: 4,
    zIndex: 9,
    alignItems: 'center',
    justifyContent: 'center',
  },
  spanText: {
    fontSize: 13,
    color: '#ffffff',
    fontFamily: 'CeraPro-Bold'
  },
  lastSlide: {
    marginRight: 23,
  },
});

const getStyles = ({
  lastSlide,
}) => {
  const promoStyles = [styles.promoBox];

  if (lastSlide) {
    promoStyles.push(styles.lastSlide);
  }

  return { promoStyles }
}

class PromoBox3 extends React.Component {
  static propTypes = {
    lastSlide: PropTypes.bool,
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string.isRequired,
    span: PropTypes.string.isRequired,
  };

  static defaultProps = {
    lastSlide: false,
  };

	render() {
    const { title, subtitle, span, lastSlide, image } = this.props;
    const { promoStyles } = getStyles({ lastSlide });
		return (
			<View style={promoStyles}>
        <View style={styles.promoImageWp}>
          <View style={styles.span}>
            <Text style={styles.spanText}>{span}</Text>
          </View>
          <Image
            style={styles.promoImage}
            source={image}
          />
        </View>
        <View style={styles.promoContent}>
          <Text style={[styles.promoTitle, styles.medium]}>{title}</Text>
          <Text style={[styles.promoSubtitle, styles.small]}>{subtitle}</Text>
        </View>
      </View>
		)
	}
}

export default PromoBox3;

