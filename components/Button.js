import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Text, StyleSheet } from 'react-native';

const PRIMARY_COLOR = '#2BEAA0';
const PRIMARY2_COLOR = '#50D6B6'
const SECONDARY_COLOR = '#a8a8a8';
const styles = StyleSheet.create({
	// Container Styles
	containerDefault: {
		alignItems: 'center',
		paddingVertical: 10,
		borderWidth: 1,
		borderRadius: 10,
		marginHorizontal: 20,
		marginVertical: 10,
	},
	containerSecondary: {
		backgroundColor: SECONDARY_COLOR,
		borderColor: SECONDARY_COLOR,
	},
	containerPrimary: {
		backgroundColor: PRIMARY_COLOR,
		borderColor: PRIMARY_COLOR,
	},
	containerPrimary2: {
		backgroundColor: PRIMARY2_COLOR,
		borderColor: PRIMARY2_COLOR,
	},
	containerPrimaryOutline: {
		backgroundColor: 'transparent',
	},
	containerSecondaryOutline: {
		backgroundColor: 'transparent',
	},
	containerLarge: {
		paddingVertical: 20,
	},
	containerSmall: {
		paddingVertical: 5,
	},
	containerDisabled: {
		opacity: 0.65,
	},
	bottomFixed: {
		position: 'absolute',
		bottom: 15,
		left: 15,
	},

	// Text Styles
	textDefault: {
		fontSize: 14,
		fontFamily: 'CeraPro-Bold',
		color: '#fff',
	},
	textPrimary: {

	},
	textPrimaryOutline: {
		color: PRIMARY_COLOR,
	},
	textSecondary: {

	},
	textSecondaryOutline: {
		color: SECONDARY_COLOR,
	},
	textLarge: {
		fontSize: 20,
	},
	textSmall: {
		fontSize: 14,
	},
	textDisabled: {

	},
});

const getStyles = ({
	size,
	theme,
	outline,
	disabled,
}) => {
	const containerStyles = [styles.containerDefault];
	const textStyles = [styles.textDefault];

	if (theme === 'secondary') {
		containerStyles.push(styles.containerSecondary);
		textStyles.push(styles.textSecondary);

		if (outline) {
			containerStyles.push(styles.containerSecondaryOutline);
			textStyles.push(styles.textSecondaryOutline);
		}
	} if (theme === 'primary2') {
		containerStyles.push(styles.containerPrimary2);
		textStyles.push(styles.textPrimary);

		if (outline) {
			containerStyles.push(styles.containerSecondaryOutline);
			textStyles.push(styles.textSecondaryOutline);
		}
	} else {
		containerStyles.push(styles.containerPrimary);
		textStyles.push(styles.textPrimary);

		if (outline) {
			containerStyles.push(styles.containerPrimaryOutline);
			textStyles.push(styles.textPrimaryOutline);
		}
	}

	if (size === 'large') {
		containerStyles.push(styles.containerLarge);
		textStyles.push(styles.textLarge);
	} else if (size === 'small') {
		containerStyles.push(styles.containerSmall);
		textStyles.push(styles.textSmall);
	}

	if (disabled) {
		containerStyles.push(styles.containerDisabled);
		textStyles.push(styles.textDisabled);
	}


	return { containerStyles, textStyles }
}

class Button extends React.Component {
	static propTypes = {
		text: PropTypes.string.isRequired,
		// onPress: PropTypes.func.isRequired,
		outline: PropTypes.bool,
		size: PropTypes.oneOf(['small', 'default', 'large']),
		theme: PropTypes.oneOf(['primary', 'primary2','secondary']),
		disabled: PropTypes.bool,
	};

	static defaultProps = {
		size: 'default',
		theme: 'primary',
		outline: false,
		disabled: false,
	}

	render() {
		const { text, onPress, disabled, ...rest } = this.props;
		const { textStyles, containerStyles } = getStyles({ disabled, ...rest });
		return (
			<TouchableOpacity onPress={onPress} disabled={disabled} style={containerStyles}>
				<Text style={textStyles}>{text}</Text>
			</TouchableOpacity>
		)
	}
}

export default Button;