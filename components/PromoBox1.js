import React from 'react';
import PropTypes from 'prop-types';
import { normalize } from '../assets/Normalize';
import { 
  View
  , Text
  , Image
  , StyleSheet
  , ImageBackground
  , Platform } from 'react-native';

const styles = StyleSheet.create({
  mini: {
    fontSize: normalize(12),
  },
  small: {
    fontSize: normalize(15),
  },
  medium: {
    fontSize: normalize(17),
  },
  large: {
    fontSize: normalize(20),
  },
  xlarge: {
    fontSize: normalize(25),
  },
  sliderBox: {
    paddingHorizontal: 0,
    marginHorizontal: 0,
    height: 210,
    position: 'relative',
    zIndex: 9,
    elevation: 5,
  },
  promoBox: {
    height: 195, 
    width: 125, 
    marginLeft: 23,
    marginTop: 5,
    marginBottom: 15,
    backgroundColor: 'rgba(0,0,0,0)',
    position: 'relative',
    overflow: 'hidden',
    borderRadius: 16,
  },
  promoImageWp: {
    width: '100%',
    height: '70%',
    overflow: 'hidden',
    borderRadius: 16,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 25 },
    shadowOpacity: 0.8,
    shadowRadius: 35,
    elevation: 15,
    marginTop: 15,
    position: 'relative',
    zIndex: 9,
  },
  promoImage: {
    width: '100%',
    height: '100%',
  },
  promoText: {
    fontSize: 14,
    color: '#2A3348',
    paddingTop: 35,
    paddingBottom: 15,
    paddingHorizontal: 10,
    position: 'absolute',
    zIndex: 1,
    bottom: 0,
    left: 0,
    flex: 1,
    width: '100%',
    backgroundColor: '#F6F6F6',
    borderBottomLeftRadius: 16,
    borderBottomRightRadius: 16,
    fontFamily: 'CeraPro-Bold',
  },
  span: {
    position: 'absolute',
    right: 0,
    top: 6,
    paddingVertical: 3,
    paddingHorizontal: 8,
    backgroundColor: '#50D6B6',
    borderRadius: 25,
    borderTopRightRadius: Platform.OS === 'ios' ? 10 : 25,
    borderBottomRightRadius: 2,
    zIndex: 10,
    elevation: 16,
    opacity: 1,
  },
  spanText: {
    fontSize: 13,
    color: '#ffffff',
    fontFamily: 'CeraPro-Bold',
  },
  noDistance: {
    opacity: 0,
  },
  lastSlide: {
    marginRight: 23,
  },
});

const getStyles = ({
  lastSlide,
  noDistance,
}) => {
  const promoStyles = [styles.promoBox];
  const spanStyles = [styles.span];

  if (lastSlide) {
    promoStyles.push(styles.lastSlide);
  }

  if (noDistance) {
    spanStyles.push(styles.noDistance);
  }

  return { promoStyles, spanStyles }
}

class PromoBox1 extends React.Component {
  static propTypes = {
    lastSlide: PropTypes.bool,
    noDistance: PropTypes.bool,
    text: PropTypes.string.isRequired,
  };

  static defaultProps = {
    lastSlide: false,
    noDistance: false,
  };

	render() {
    const { text, lastSlide, noDistance, image, span } = this.props;
    const { promoStyles, spanStyles } = getStyles({ lastSlide, noDistance });
		return (
			<View style={promoStyles}>
        <View style={spanStyles}>
          <Text style={[styles.spanText]}>{span}</Text>
        </View>
        <View style={styles.promoImageWp}>
          <Image
            style={styles.promoImage}
            source={image}
          />
        </View>
        <Text style={[styles.promoText, styles.small]}>{text}</Text>
      </View>
		)
	}
}

export default PromoBox1;

