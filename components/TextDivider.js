import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, Image, StyleSheet } from 'react-native';
import { normalize } from '../assets/Normalize';

class TextDivider extends React.Component {
  static propTypes = {
    upper: PropTypes.string.isRequired,
    under: PropTypes.string.isRequired,
  };

	render() {
    const { upper, under } = this.props;
		return (
			<View style={[styles.mcRow, styles.textDivider]}>
        <View style={styles.mcCol6}>
          <Text style={[styles.upper, styles.small]}>{upper}</Text>
          <Text style={[styles.under, styles.large]}>{under}</Text>
        </View>
        <View style={styles.mcCol1}>
          <Image
            style={{width: 25, height: 25}}
            source={require('../assets/icon-more.png')}
          />
        </View>
      </View>
		)
	}
}

export default TextDivider;

const styles = StyleSheet.create({
  mini: {
    fontSize: normalize(12),
  },
  small: {
    fontSize: normalize(15),
  },
  medium: {
    fontSize: normalize(17),
  },
  large: {
    fontSize: normalize(20),
  },
  xlarge: {
    fontSize: normalize(25),
  },
  mcRow: {
    marginVertical: 0,
    marginHorizontal: 28,
    flexDirection: 'row',
  },
  mcCol6: {
    flex: 6,
  },
  mcCol1: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  block: {
    backgroundColor: '#dddddd',
    borderRadius: 8,
    height: 50,
    margin: 5,
  },
  textDivider: {
    marginVertical: 10,
  },
  upper: {
    fontSize: 17,
    marginBottom: 0,
    color: '#2A3348',
    fontFamily: 'CeraPro-Regular',
  },
  under: {
    fontSize: 24,
    color: '#2A3348',
    textTransform: 'capitalize',
    fontFamily: 'CeraPro-Bold',
  }
});