import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, ImageBackground, StyleSheet } from 'react-native';

class UserCard extends React.Component {
  static propTypes = {
    amount: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    expire: PropTypes.string.isRequired,
  };

	render() {
    const { amount, name, expire, image } = this.props;
		return (
			<View style={[styles.card]}>
        <ImageBackground 
          source={image}
          style={styles.cardBg}
          imageStyle={{ borderRadius: 16 }}>
          <View style={{ alignItems: 'center', justifyContent: 'flex-start', flexDirection: 'row', position: 'absolute', top: '45%', left: 25 }}>
            
            <Text style={styles.cardAmount}>{amount}</Text>
            <Text style={styles.cardSup}>Pts</Text>

          </View>
          <Text style={styles.cardName}>{name}</Text>
          <Text style={styles.cardExp}>Exp {expire}</Text>
        </ImageBackground>
      </View>
		)
	}
}

export default UserCard;

const styles = StyleSheet.create({
  card: {
    height: 180,
    overflow: 'visible',
    margin: 5,
    borderRadius: 16,
    elevation: 8,
  },
  cardBg: {
    width: '100%',
    height: '100%',
    position: 'relative',
    shadowColor: '#14E0AF',
    shadowOffset: { width: 0, height: 12 },
    shadowOpacity: 0.2,
    shadowRadius: 15,  
    elevation: 6,
  },
  cardAmount: {
    fontSize: 27,
    fontFamily: 'CeraPro-Bold',
    color: '#fff',
  },
  cardSup: {
    fontSize: 13,
    marginRight: 5,
    marginLeft: 5,
    marginBottom: 7,
    color: '#fff',
    fontFamily: 'CeraPro-Bold',
  },
  cardName: {
    fontSize: 14,
    color: '#fff',
    letterSpacing: 2,
    textTransform: 'uppercase',
    position: 'absolute',
    bottom: 25,
    left: 25,
    fontFamily: 'CeraPro-Regular',
  },
  cardExp: {
    fontSize: 14,
    color: '#fff',
    letterSpacing: 1,  
    textTransform: 'capitalize',
    position: 'absolute',
    bottom: 25,
    right: 25,
    fontFamily: 'CeraPro-Bold',
  }
});