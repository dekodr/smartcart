import React from 'react';
import PropTypes from 'prop-types';
import { 
  View
  , Text
  , Image
  , StyleSheet
  , ImageBackground
  , Platform } from 'react-native';

const styles = StyleSheet.create({
  merchantSlider: {
    width: 85,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 23,
  },
  merchantImage: {
    overflow: 'hidden',
    width: 80,
    height: 80,
    borderRadius: Platform.OS === 'ios' ? 40 : 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  merchantName: {
    fontSize: 12,
    fontFamily: 'CeraPro-Bold',
    color: '#2A3348',
    marginTop: 8,
  },
  lastSlide: {
    marginRight: 23,
  },
});

const getStyles = ({
  lastSlide,
}) => {
  const merchantSlide = [styles.merchantSlider];

  if (lastSlide) {
    merchantSlide.push(styles.lastSlide);
  }

  return { merchantSlide }
}

class MerchantSlider extends React.Component {
  static propTypes = {
    lastSlide: PropTypes.bool,
    text: PropTypes.string.isRequired,
  };

  static defaultProps = {
    lastSlide: false,
    noDistance: false,
  };

	render() {
    const { text, lastSlide, image } = this.props;
    const { merchantSlide } = getStyles({ lastSlide });
		return (
			<View style={merchantSlide}>
        <Image
          style={styles.merchantImage}
          source={image}
        />
        <Text style={styles.merchantName}>
          {text}
        </Text>
      </View>
		)
	}
}

export default MerchantSlider;

