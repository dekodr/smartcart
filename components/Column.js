import React from 'react';
import PropTypes from 'prop-types';
import { View, Text, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
	mcGrid: {
		margin: 0,
		padding: 0,
		zIndex: 1,
		position: 'relative',
	}
});

const getStyles = ({
	mcGrid,
	mcRow,
	mcCol,
	size,
}) => {
	const constStyle = [styles.constStyles];

	if (mcGrid) {
		constStyle.push(styles.mcGrid)
	} 

	return { constStyle }
}

class Column extends React.Component {
	static propTypes = {
		mcGrid: PropTypes.bool,
		mcRow: PropTypes.bool,
		mcCol: PropTypes.bool,
		size: PropTypes.oneOf([12, 6]),
		content: PropTypes.string.isRequired,
	}

	static defaultProps = {
		mcGrid: false,
		mcRow: false,
		mcCol: false,
	}

	render() {
		const { content, mcCol, ...rest } = this.props;
		const { constStyles } = getStyles({ mcCol, ...rest });
		return (
			<View style={constStyles}>
				<Text>{content}</Text>
			</View>
		)
	}
}

export default Column;


