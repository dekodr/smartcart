import React from 'react';
import { 
	StyleSheet
	, Text
	, View 
} from 'react-native';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import NearbyScreen from '../pages/Nearby';
import NearbyMapScreen from '../pages/NearbyMap';

const AppStackNavigator = createStackNavigator({
	Nearby: {
		screen: NearbyScreen,
	},
	NearbyMap: {
		screen: NearbyMapScreen,
	},
}, 
{
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});

const App = createAppContainer(AppStackNavigator);

export default App;