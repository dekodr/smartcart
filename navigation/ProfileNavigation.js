import React from 'react';
import { 
	StyleSheet
	, Text
	, View 
} from 'react-native';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import PayPopupScreen from '../pages/PayPopup';
import PayReceiptScreen from '../pages/PayReceipt';

const AppStackNavigator = createStackNavigator({
	PayPopup: {
		screen: PayPopupScreen,
	},
	PayReceipt: {
		screen: PayReceiptScreen,
	},
}, 
{
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});

const App = createAppContainer(AppStackNavigator);

export default App;