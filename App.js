/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {
  Platform
  , Dimensions
  , StyleSheet
  , Text
  , View
  , ScrollView 
  , Image
  , ImageBackground
  , TouchableHighlight} from 'react-native';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import styles from './assets/style';

// - - -  S T A C K  M E T H O D - - -
import Navigation from './navigation/Navigation';
import NearbyNavigation from './navigation/NearbyNavigation';
// import PayNavigation from './navigation/PayNavigation';
import PromoNavigation from './navigation/PromoNavigation';
import ProfileNavigation from './navigation/ProfileNavigation';

// - - -  H O M E  S C R E E N - - -
import HomeScreen from './pages/Home';
import Home2Screen from './pages/Home2';
import Home3Screen from './pages/Home3';
import PromoDetailScreen from './pages/PromoDetail';
import PromoDetailMapScreen from './pages/PromoDetailMap';
import HistoryScreen from './pages/History';
import WatchScreen from './pages/WatchPage';
import ShareScreen from './pages/SharePage';
import TodayReceiptScreen from './pages/TodayReceipt';
import TodayReceipt2Screen from './pages/TodayReceipt2';
import TodayReceiptDetailScreen from './pages/TodayReceiptDetail';
// import TodayReceiptDetail2Screen from './pages/TodayReceiptDetail2';
import MyPointsScreen from './pages/MyPoints';
import Share2Screen from './pages/SharePage2';
import ShoppingHistoryScreen from './pages/ShoppingHistory';

import MagazineScreen from './pages/Magazine';

// - - -  N E A R B Y  S C R E E N - - -
import NearbyScreen from './pages/Nearby';
import NearbyMapScreen from './pages/NearbyMap';
import StorePageScreen from './pages/StorePage';

// - - -  P A Y  S C R E E N - - -
import PayPopupScreen from './pages/PayPopup';
import PayReceiptScreen from './pages/PayReceipt';
import PayReceipt2Screen from './pages/PayReceipt2';
import PayQrScreen from './pages/PayQr';
import PayQrCustomerScreen from './pages/PayQrCustomer';
import PayQr2Screen from './pages/PayQr2';
import AfterScanPageScreen from './pages/AfterScanPage';
import PayEnterPinScreen from './pages/PayEnterPin';
import PayQrSuccessScreen from './pages/PayQrSuccess';
import PayOptionScreen from './pages/PayOption';

// - - -  P R O F I L E  S C R E E N - - -
import ProfileScreen from './pages/Profile';
import Profile2Screen from './pages/Profile2';
console.disableYellowBox = true;
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

// - - - G E T  S C R E E N  H E I G H T
let ScreenHeight = Dimensions.get("window").height;

// - - -  H O M E  S T A C K - - -
const HomeStack = createStackNavigator({ 
  Home: {
    screen: Home2Screen,
  },
  PromoDetail: {
    screen: PromoDetailScreen,
  },
  PromoDetailMap: {
    screen: PromoDetailMapScreen,
  },
  Watch: {
    screen: WatchScreen,
  }, 
  TodayReceipt: {
    screen: TodayReceiptScreen,
  },
  TodayReceipt2: {
    screen: TodayReceipt2Screen,
  },
  TodayReceiptDetail: {
    screen: TodayReceiptDetailScreen,
  },
  MyPoints: {
    screen: MyPointsScreen,
  },
  Share: {
    screen: ShareScreen,
  },
  Share2: {
    screen: Share2Screen,
  },
  
  /*-------------------------------------*/

  PayQr: {
    screen: PayQrScreen,
  },
  PayQr2: {
    screen: PayQr2Screen,
  },

});

HomeStack.navigationOptions = ({ navigation }) => {
  if(navigation.state.index!=0){
      return {
        tabBarVisible: false,
      };
  }
  return {
    tabBarVisible: false,
  }, {
    tabBarLabel: 'Home',
  };
};
// - - -  H O M E  S T A C K  E N D - - -


// - - -  M A G A Z I N E  S T A C K - - -
const MagazineStack = createStackNavigator({
  Magazine: {
    screen: MagazineScreen,
  },
});

MagazineStack.navigationOptions = ({ navigation }) => {
  if(navigation.state.index!=0){
      return {
        tabBarVisible: false,
      };
  }
  return {
    tabBarVisible: true,
  }, {
    tabBarLabel: 'Magazine',
  };
};

// - - -  N E A R B Y  S T A C K - - -
const Nearby = createStackNavigator({
  Nearby: {
    screen: NearbyScreen,
  },
  NearbyMap: {
    screen: NearbyMapScreen,
  },
  StorePage: {
    screen: StorePageScreen,
  },
  History: {
    screen: HistoryScreen,
  },
  PayEnterPin: {
    screen: PayEnterPinScreen,
  },
  PayQrSuccess: {
    screen: PayQrSuccessScreen,
  },
  PayPopup: {
    screen: PayPopupScreen,
  },
  PayReceipt: {
    screen: PayReceiptScreen,
  },
  PayReceipt2: {
    screen: PayReceipt2Screen,
  },
  AfterScanPage: {
    screen: AfterScanPageScreen,
  },
});

Nearby.navigationOptions = ({ navigation }) => {
  if(navigation.state.index!==1){
      return {
        tabBarVisible: false,
      };
  }
  return {
    tabBarVisible: false,
  }, {
    tabBarLabel: 'Nearby',
  };
};
// - - -  N E A R B Y  S T A C K  E N D - - -


// - - -  P A Y  S T A C K - - -
const PayStack = createStackNavigator({
  // PayQrCustomer: {
  //   screen: PayQrCustomerScreen,
  // },
  // PayQr2: {
  //   screen: PayQr2Screen,
  // },
  PayQr: {
    screen: PayQrScreen,
  },
  
});
PayStack.navigationOptions = ({ navigation }) => {
  if(navigation.state.index==1){
      return {
        tabBarVisible: false,
        tabBarLabel: ' ',
      };
  }
  return {
    tabBarVisible: false,
    tabBarLabel: ' ',
  };
};

const PayQr = createStackNavigator({
  PayOption: {
    screen: PayOptionScreen
  },
  PayPopup: {
    screen: PayQrScreen,
  },
  PayQr: {
    screen: PayQrScreen,
  },
  // PayReceipt: {
  //   screen: PayReceiptScreen,
  // },
});

PayQr.navigationOptions = ({ navigation }) => {
  if(navigation.state.index==1){
      return {
        tabBarVisible: false,
        tabBarLabel: ' ',
      };
  }
  return {
    tabBarVisible: false,
    tabBarLabel: ' ',
  };
};
// - - -  P A Y  S T A C K  E N D - - -


// - - -  P R O F I L E  S T A C K - - -
const ProfileStack = createStackNavigator({
  Profile: {
    screen: Profile2Screen,
  },
});

ProfileStack.navigationOptions = ({ navigation }) => {
  if(navigation.state.index!=0){
      return {
        tabBarVisible: false,
      };
  }
  return {
    tabBarVisible: true,
  }, {
    tabBarLabel: 'Profile',
  };
};
// - - -  P R O F I L E  S T A C K  E N D - - -


// - - - - - - - - - - - - - - - - - - - -
// S C R E E N  M E T H O D , P R O B A B L Y  W A N N A  C H A N G E
class Promocreen extends React.Component {
  render() {
    return (
      <PromoNavigation />
    );
  }
}
// - - - - - - - - - - - - - - - - - - - -

// - - - C R E A T E  B O T T O M  N A V I G A T O R - - -
const TabNavigator = createBottomTabNavigator({
  HomeStack,
  Nearby,
  PayQr,
  MagazineStack,
  ProfileStack,
},
{
  // - - - I C O N S  O P T I O N - - -
  defaultNavigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ focused, horizontal, tintColor }) => {
      const { routeName } = navigation.state;
      if (routeName === 'HomeStack') {
        return (
          focused
          ? <Image
            source={ require('./assets/icon-menu-home-active.png') }
            style={{ width: 15, height: 15, }} />
          : <Image
            source={ require('./assets/icon-menu-home.png') }
            style={{ width: 15, height: 15, }} />
        );
      } if (routeName === 'Nearby') {
        return (
          focused
          ? <Image
            source={ require('./assets/icon-menu-nearby-active.png') }
            style={{ width: 13, height: 17, }} />
          : <Image
            source={ require('./assets/icon-menu-nearby.png') }
            style={{ width: 13, height: 17, }} />
        );
      } if (routeName === 'Promo') {
        return (
          focused
          ? <Image
            source={ require('./assets/icon-menu-promo-active.png') }
            style={{ width: 14, height: 14, }} />
          : <Image
            source={ require('./assets/icon-menu-promo.png') }
            style={{ width: 14, height: 14, }} />
        );
      } if (routeName === 'MagazineStack') {
        return (
          focused
          ? <Image
            source={ require('./assets/icon-menu-magazine-active.png') }
            style={{ width: 16, height: 16, }} />
          : <Image
            source={ require('./assets/icon-menu-magazine.png') }
            style={{ width: 16, height: 16, }} />
        );
      } if (routeName === 'ProfileStack') {
        return (
          focused
          ? <Image
            source={ require('./assets/icon-menu-user-active.png') }
            style={{ width: 19, height: 19, }} />
          : <Image
            source={ require('./assets/icon-menu-user.png') }
            style={{ width: 19, height: 19, }} />
        );
      } else {
        return (
          <View style={{
            height: 95,
            width: 95,
            borderRadius: 100,
            backgroundColor: 'rgba(80,214,182, .36)',
            alignItems: 'center',
            justifyContent: 'center',
            bottom: 0,
          }}>
            <View style={{ 
              height: 75, 
              width: 75, 
              borderRadius: 100, 
              backgroundColor: '#50D6B6', 
              // borderWidth: 8, 
              // borderColor: 'rgba(80,214,182, .36)',
              position: 'absolute',
              alignItems: 'center',
              justifyContent: 'center',
              zIndex: 99,
            }}>
              <Text style={{
                fontSize: 20,
                fontWeight: '700',
                color: '#ffffff',
              }}>PAY</Text>
            </View>
          </View>
        );
      }
    },
  }),
  tabBarOptions: {
    activeTintColor: '#6E72D6',
    inactiveTintColor: '#263238',
    style: {
      backgroundColor: '#ffffff',
      // borderTopLeftRadius: 36,
      // borderTopRightRadius: 36,
      paddingBottom: 8,
      paddingTop:5,
      height: 55,
      shadowColor: '#000',
      shadowOffset: { width: 0, height: -8 },
      shadowOpacity: 0.2,
      shadowRadius: 8,
      elevation: 35,
      borderTopWidth: 0,
    },
  },
}
);

export default createAppContainer(TabNavigator);


